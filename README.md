# Infinicell
It is what it is

## Technologies
### Backend
#### Current
- Java
- Spring Boot
- GraphQL (Query, Mutation, Subscription)
- JPA
- Built-in JWT Auth
#### Future
- Centralized Auth

### Client
#### Current
- Angular
- Bulma CSS
### Future
- Pixi - JS Engine
- Unity - C# Engine
- DragonBones - 2D animation software
  - Plugins for Pixi and Unity