package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.RandomizedItemDrop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RandomizedItemDropRepository extends JpaRepository<RandomizedItemDrop, String> {
  List<RandomizedItemDrop> findAllByDropTableId(String dropTableId);
  List<RandomizedItemDrop> findAllByItemId(String itemId);
}
