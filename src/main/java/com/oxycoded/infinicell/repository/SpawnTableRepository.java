package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.SpawnTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SpawnTableRepository extends JpaRepository<SpawnTable, String> {
  Optional<SpawnTable> findBySpawnTableEnabledEntityId(String dropTableEnabledEntityId);
  Optional<SpawnTable> findByConsistentCreatureSpawnsId(String consistentCreatureSpawnsId);
  Optional<SpawnTable> findByRandomizedCreatureSpawnsId(String randomizedCreatureSpawnsId);
}
