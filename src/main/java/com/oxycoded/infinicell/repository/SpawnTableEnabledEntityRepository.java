package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.SpawnTableEnabledEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SpawnTableEnabledEntityRepository extends JpaRepository<SpawnTableEnabledEntity, String> {
  Optional<SpawnTableEnabledEntity> findBySpawnTableId(String spawnTableId);
}
