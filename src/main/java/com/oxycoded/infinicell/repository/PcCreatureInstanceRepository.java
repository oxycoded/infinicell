package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.PcCreatureInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PcCreatureInstanceRepository extends JpaRepository<PcCreatureInstance, String> {
  List<PcCreatureInstance> findAllByAccountId(String accountId);
  Optional<PcCreatureInstance> findBySelectedByAccountId(String accountId);
  Optional<PcCreatureInstance> findByPcCreatureInstanceCellsId(String creatureInstanceCellId);
  Optional<PcCreatureInstance> findByPcCreatureInstanceCellsCellIdAndPcCreatureInstanceCellsDiscovered(String cellId, boolean discovered);
}
