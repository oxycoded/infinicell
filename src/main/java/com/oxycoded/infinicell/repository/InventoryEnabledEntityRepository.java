package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.InventoryEnabledEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InventoryEnabledEntityRepository extends JpaRepository<InventoryEnabledEntity, String> {
  Optional<InventoryEnabledEntity> findByInventoryId(String inventoryId);
}
