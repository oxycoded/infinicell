package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.Combat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CombatRepository extends JpaRepository<Combat, String> {
  Optional<Combat> findByCombatantsId(String combatantId);
}
