package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.CreatureInstance;
import com.oxycoded.infinicell.domain.model.NpcCreatureInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CreatureInstanceRepository extends JpaRepository<CreatureInstance, String> {
  List<CreatureInstance> findAllByCreatureId(String creatureId);
  List<CreatureInstance> findAllByRosterId(String rosterId);
  Optional<CreatureInstance> findByConsistentCreatureSpawnsId(String consistentCreatureSpawnsId);
  Optional<CreatureInstance> findByCombatantsId(String combatantId);
}
