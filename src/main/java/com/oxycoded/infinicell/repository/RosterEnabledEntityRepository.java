package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.RosterEnabledEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RosterEnabledEntityRepository extends JpaRepository<RosterEnabledEntity, String> {
  Optional<RosterEnabledEntity> findByRosterId(String rosterId);
}
