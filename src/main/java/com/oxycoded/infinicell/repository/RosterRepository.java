package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.Roster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RosterRepository extends JpaRepository<Roster, String> {
  Optional<Roster> findByRosterEnabledEntityId(String rosterEnabledEntityId);
  Optional<Roster> findByCreatureInstancesId(String creatureInstancesId);
}
