package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ItemRepository extends JpaRepository<Item, String> {
  Optional<Item> findByName(String name);
  Optional<Item> findByItemInstancesId(String itemInstanceId);
  Optional<Item> findByRandomizedItemDropsId(String randomizedItemDropId);
}
