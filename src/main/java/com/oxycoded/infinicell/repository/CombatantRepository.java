package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.Combatant;
import com.oxycoded.infinicell.domain.model.CombatantStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CombatantRepository extends JpaRepository<Combatant, String> {
  List<Combatant> findAllByCombatId(String combatId);
  List<Combatant> findAllByCreatureInstanceId(String creatureInstanceId);
  Optional<Combatant> findByCreatureInstanceIdAndCombatantStatusIn(String creatureInstanceId, List<CombatantStatus> combatantStatusList);
}
