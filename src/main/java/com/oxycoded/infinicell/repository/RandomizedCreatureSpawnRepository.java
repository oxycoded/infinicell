package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.RandomizedCreatureSpawn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RandomizedCreatureSpawnRepository extends JpaRepository<RandomizedCreatureSpawn, String> {
  List<RandomizedCreatureSpawn> findAllBySpawnTableId(String spawnTableId);
  List<RandomizedCreatureSpawn> findAllByCreatureId(String creatureId);
}
