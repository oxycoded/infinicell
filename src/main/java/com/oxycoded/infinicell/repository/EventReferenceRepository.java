package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.EventReference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventReferenceRepository extends JpaRepository<EventReference, String> {
  List<EventReference> findAllByEventReferenceEnabledEntityId(String eventReferenceEnabledEntityId);
  List<EventReference> findAllByEventId(String eventId);
}
