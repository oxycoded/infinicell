package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EventRepository extends JpaRepository<Event, String> {
  Optional<Event> findByEventReferencesId(String eventReferencesId);
}
