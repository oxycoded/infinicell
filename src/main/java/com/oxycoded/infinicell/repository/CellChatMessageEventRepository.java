package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.CellChatMessageEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CellChatMessageEventRepository extends JpaRepository<CellChatMessageEvent, String> {
}
