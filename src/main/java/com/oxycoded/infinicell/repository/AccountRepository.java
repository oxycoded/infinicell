package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, String> {
  Optional<Account> findByLogInCode(String logInCode);
  Optional<Account> findByFriendsId(String friendId);
  Optional<Account> findByFriendOfsId(String friendOfId);
  Optional<Account> findByIncomingFriendRequestsId(String friendRequestId);
  Optional<Account> findByOutgoingFriendRequestsId(String friendRequestId);
}
