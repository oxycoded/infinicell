package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.Cell;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CellRepository extends JpaRepository<Cell, String> {
  Optional<Cell> findByXAndYAndZ(long x, long y, long z);
  Optional<Cell> findByPcCreatureInstanceCellsId(String pcCreatureInstanceCellsId);
}
