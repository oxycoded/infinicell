package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.ConsistentCreatureSpawn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsistentCreatureSpawnRepository extends JpaRepository<ConsistentCreatureSpawn, String> {
  List<ConsistentCreatureSpawn> findAllBySpawnTableId(String spawnTableId);
  List<ConsistentCreatureSpawn> findAllByCreatureInstanceToCopyId(String creatureInstanceToCopyId);
}
