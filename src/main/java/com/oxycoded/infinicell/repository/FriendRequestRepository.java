package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.FriendRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FriendRequestRepository extends JpaRepository<FriendRequest, String> {

}
