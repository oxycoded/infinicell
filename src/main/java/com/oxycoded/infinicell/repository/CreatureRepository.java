package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.Creature;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CreatureRepository extends JpaRepository<Creature, String> {
  Optional<Creature> findByName(String name);
  Optional<Creature> findByCreatureInstancesId(final String creatureInstancesId);
  Optional<Creature> findByRandomizedCreatureSpawnsId(final String ramdomizedCreatureSpawnsId);
}
