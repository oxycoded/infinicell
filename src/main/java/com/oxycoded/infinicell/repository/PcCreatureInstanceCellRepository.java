package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.PcCreatureInstanceCell;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PcCreatureInstanceCellRepository extends JpaRepository<PcCreatureInstanceCell, String> {
  List<PcCreatureInstanceCell> findAllByPcCreatureInstanceId(String creatureInstanceId);
  Optional<PcCreatureInstanceCell> findByCellIdAndPcCreatureInstanceId(String cellId, String pcCreatureInstanceId);
  Optional<PcCreatureInstanceCell> findByPcCreatureInstanceIdAndCurrent(String pcCreatureInstanceId, boolean current);
  Optional<PcCreatureInstanceCell> findByCombatsId(String combatId);
}
