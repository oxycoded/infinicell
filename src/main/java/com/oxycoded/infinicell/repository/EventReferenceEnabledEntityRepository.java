package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.EventReferenceEnabledEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EventReferenceEnabledEntityRepository extends JpaRepository<EventReferenceEnabledEntity, String> {
  Optional<EventReferenceEnabledEntity> findByEventReferencesId(String eventReferencesId);
}
