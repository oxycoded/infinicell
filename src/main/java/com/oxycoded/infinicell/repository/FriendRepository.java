package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.Friend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FriendRepository extends JpaRepository<Friend, String> {
  List<Friend> findAllByAccountId(String accountId);
  Optional<Friend> findByAccountIdAndAccountOfFriendId(String accountId, String accountOfFriendId);
}
