package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.DropTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DropTableRepository extends JpaRepository<DropTable, String> {
  Optional<DropTable> findByDropTableEnabledEntityId(String dropTableEnabledEntityId);
  Optional<DropTable> findByConsistentItemDropsId(String consistentItemDropsId);
  Optional<DropTable> findByRandomizedItemDropsId(String randomizedItemDropsId);
}
