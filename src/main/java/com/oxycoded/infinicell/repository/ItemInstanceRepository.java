package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.ItemInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ItemInstanceRepository extends JpaRepository<ItemInstance, String> {
  Optional<ItemInstance> findByConsistentItemDropsId(String consistentItemDropsId);
  List<ItemInstance> findAllByIdIn(List<String> ids);
  List<ItemInstance> findAllByItemId(String itemId);
  List<ItemInstance> findAllByInventoryId(String inventoryId);
}
