package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InventoryRepository extends JpaRepository<Inventory, String> {
  Optional<Inventory> findByInventoryEnabledEntityId(String inventoryEnabledEntityId);
}
