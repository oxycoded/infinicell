package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.DropTableEnabledEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DropTableEnabledEntityRepository extends JpaRepository<DropTableEnabledEntity, String> {
  Optional<DropTableEnabledEntity> findByDropTableId(String dropTableId);
}
