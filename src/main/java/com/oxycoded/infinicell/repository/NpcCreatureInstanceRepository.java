package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.NpcCreatureInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NpcCreatureInstanceRepository extends JpaRepository<NpcCreatureInstance, String> {
  List<NpcCreatureInstance> findAllByRosterId(String rosterId);
}
