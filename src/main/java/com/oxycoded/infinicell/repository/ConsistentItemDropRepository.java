package com.oxycoded.infinicell.repository;

import com.oxycoded.infinicell.domain.model.ConsistentItemDrop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsistentItemDropRepository extends JpaRepository<ConsistentItemDrop, String> {
  List<ConsistentItemDrop> findAllByDropTableId(String dropTableId);
  List<ConsistentItemDrop> findAllByItemInstanceToCopyId(String itemInstanceToCopyId);
}
