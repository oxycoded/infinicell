package com.oxycoded.infinicell.graphql.mutation.resolver;

import com.oxycoded.infinicell.application.configuration.security.JwtUserDetails;
import com.oxycoded.infinicell.domain.model.*;
import com.oxycoded.infinicell.exception.DuplicateUsernameException;
import com.oxycoded.infinicell.service.*;
import com.oxycoded.infinicell.service.intermediate.FriendRequestManagementService;
import com.oxycoded.infinicell.service.intermediate.PcCreatureInstanceManagementService;
import com.oxycoded.infinicell.service.intermediate.LogInService;
import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
@Slf4j
public class Mutation implements GraphQLMutationResolver {

  private final AccountService accountService;
  private final LogInService logInService;
  private final FriendRequestManagementService friendRequestManagementService;
  private final PcCreatureInstanceManagementService pcCreatureInstanceManagementService;

  private final PcCreatureInstanceService pcCreatureInstanceService;
  private final FriendRequestService friendRequestService;
  private final FriendService friendService;
  private final EventService eventService;
  private final EventReferenceService eventReferenceService;

  @PreAuthorize("isAnonymous()")
  public AnonymousAccount createAnonymousAccount(final String username) {
    try {
      final Account createdAccount = getAccountService().createAnonymousAccount(username);
      return new AnonymousAccount(createdAccount, createdAccount.getLogInCode());
    } catch (DataIntegrityViolationException dive) {
      if(StringUtils.contains(dive.getMessage(), "USERNAME")) {
        log.info("Attempted to create anonymous account with duplicate username: {}", username, dive);
        throw new DuplicateUsernameException(username);
      }
      log.error("Unexpected exception while creating anonymous account", dive);
      throw dive;
    } catch (final Exception e) {
      log.error("Unexpected exception while creating anonymous account", e);
      throw e;
    }
  }

  @PreAuthorize("isAnonymous()")
  public Account createAccount(final String name, final String email, final String password, final String confirmPassword) {
    return getAccountService().createAccount(name, email, password, confirmPassword);
  }

  @PreAuthorize("isAnonymous()")
  public LogInAccount accountLogInWithCode(final String logInCode) {
    return getLogInService().accountLogInWithCode(logInCode);
  }

  @PreAuthorize("isAnonymous()")
  public LogInAccount accountLogInWithUsernamePassword(final String username, final String password) {
    return getLogInService().accountLogInWithUsernamePassword(username, password);
  }

  @PreAuthorize("isAuthenticated()")
  public FriendRequest createOutgoingFriendRequest(final String recipientAccountId) {
    return getFriendRequestManagementService().createOutgoingFriendRequest(getAccountIdFromSecurityContext(), recipientAccountId);
  }

  @PreAuthorize("isAuthenticated()")
  public FriendRequest cancelOutgoingFriendRequest(final String friendRequestId) {
    return getFriendRequestManagementService().cancelOutgoingFriendRequest(getAccountIdFromSecurityContext(), friendRequestId);
  }

  @PreAuthorize("isAuthenticated()")
  public FriendRequest acceptIncomingFriendRequest(final String friendRequestId) {
    return getFriendRequestManagementService().acceptIncomingFriendRequest(getAccountIdFromSecurityContext(), friendRequestId);
  }

  @PreAuthorize("isAuthenticated()")
  public FriendRequest denyIncomingFriendRequest(final String friendRequestId) {
    return getFriendRequestManagementService().denyIncomingFriendRequest(getAccountIdFromSecurityContext(), friendRequestId);
  }

  @PreAuthorize("isAuthenticated()")
  public FriendRequest ignoreIncomingFriendRequest(final String friendRequestId) {
    return getFriendRequestManagementService().ignoreIncomingFriendRequest(getAccountIdFromSecurityContext(), friendRequestId);
  }

  @PreAuthorize("isAuthenticated()")
  public PcCreatureInstance createPcCreatureInstance(final String name, final String description) {
    return getPcCreatureInstanceManagementService().createPcCreatureInstance(getAccountIdFromSecurityContext(), name, description);
  }

  @PreAuthorize("isAuthenticated()")
  public PcCreatureInstance loadPcCreatureInstance(final String pcCreatureInstanceId) {
    return getPcCreatureInstanceManagementService().loadPcCreatureInstance(getAccountIdFromSecurityContext(), pcCreatureInstanceId);
  }

  @PreAuthorize("isAuthenticated()")
  public PcCreatureInstance step(final Direction direction) {
    return getPcCreatureInstanceManagementService().step(getAccountIdFromSecurityContext(), direction);
  }

  @PreAuthorize("isAuthenticated()")
  public PcCreatureInstance searchCurrentCell() {
    return getPcCreatureInstanceManagementService().searchCurrentCell(getAccountIdFromSecurityContext());
  }

  @PreAuthorize("isAuthenticated()")
  public PcCreatureInstance takeItemInstances(final List<String> itemInstanceIds) {
    return getPcCreatureInstanceManagementService().takeItemInstances(getAccountIdFromSecurityContext(), itemInstanceIds);
  }

  @PreAuthorize("isAuthenticated()")
  public PcCreatureInstance shareItemInstances(final List<String> itemInstanceIds) {
    return getPcCreatureInstanceManagementService().shareItemInstances(getAccountIdFromSecurityContext(), itemInstanceIds);
  }

  @PreAuthorize("isAuthenticated()")
  public PcCreatureInstance scoutCurrentCell() {
    return getPcCreatureInstanceManagementService().scoutCurrentCell(getAccountIdFromSecurityContext());
  }

  @PreAuthorize("isAuthenticated()")
  public PcCreatureInstance startCombat(final List<String> creatureInstanceIds) {
    return getPcCreatureInstanceManagementService().startCombat(getAccountIdFromSecurityContext(), creatureInstanceIds);
  }





  public Event sendLocalChat(final String pcCreatureInstanceId, final String message) {
    return getPcCreatureInstanceService().sendLocalChat(pcCreatureInstanceId, message);
  }

  public PcCreatureInstance claimCurrentCell(final String pcCreatureInstanceId) {
    return getPcCreatureInstanceService().claimCurrentCell(pcCreatureInstanceId);
  }

  private String getAccountIdFromSecurityContext() {
    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    if (authentication == null) {
      throw new RuntimeException(
          "Not authenticated; Make sure the method you're calling this one from is annotated with @PreAuthorize(\"isAuthenticated()\")");
    }

    final Object principal = authentication.getPrincipal();

    if (principal == null) {
      throw new RuntimeException("Authentication principal does not exist");
    }

    if (!(principal instanceof JwtUserDetails)) {
      throw new RuntimeException("Authentication principal is not a JwtUserDetails");
    }

    final JwtUserDetails jwtUserDetails = (JwtUserDetails) principal;
    final String accountId = jwtUserDetails.getAccountId();

    if (StringUtils.isBlank(accountId)) {
      throw new RuntimeException("Authenticated accountId is blank");
    }

    return accountId;
  }
}
