package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.Creature;
import com.oxycoded.infinicell.domain.model.RandomizedCreatureSpawn;
import com.oxycoded.infinicell.domain.model.SpawnTable;
import com.oxycoded.infinicell.service.CreatureService;
import com.oxycoded.infinicell.service.SpawnTableService;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class RandomizedCreatureSpawnResolver implements GraphQLResolver<RandomizedCreatureSpawn> {

  private final SpawnTableService spawnTableService;
  private final CreatureService creatureService;

  public SpawnTable spawnTable(final RandomizedCreatureSpawn randomizedCreatureSpawn) {
    return getSpawnTableService().findByRandomizedCreatureSpawnId(randomizedCreatureSpawn.getId());
  }
  
  public Creature creature(final RandomizedCreatureSpawn randomizedCreatureSpawn) {
    return getCreatureService().findByRandomizedCreatureSpawnId(randomizedCreatureSpawn.getId());
  }
}
