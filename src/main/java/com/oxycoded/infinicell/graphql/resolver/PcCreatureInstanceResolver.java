package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.*;
import com.oxycoded.infinicell.service.*;
import graphql.kickstart.tools.GraphQLResolver;
import graphql.relay.Connection;
import graphql.relay.SimpleListConnection;
import graphql.schema.DataFetchingEnvironment;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class PcCreatureInstanceResolver implements GraphQLResolver<PcCreatureInstance> {

  private final EventReferenceService eventReferenceService;
  private final InventoryService inventoryService;
  private final DropTableService dropTableService;
  private final CreatureService creatureService;
  private final PcCreatureInstanceCellService pcCreatureInstanceCellService;
  private final AccountService accountService;
  private final RosterService rosterService;
  private final ConsistentCreatureSpawnService consistentCreatureSpawnService;
  private final CombatantService combatantService;

  public Connection<EventReference> eventReferences(final PcCreatureInstance pcCreatureInstance, final int first, final int last,
      final String before, final String after, final DataFetchingEnvironment dataFetchingEnvironment) {
    final List<EventReference> list = getEventReferenceService().findAllByEventReferenceEnabledEntityId(pcCreatureInstance.getId());
    return new SimpleListConnection<>(list, "cursor").get(dataFetchingEnvironment);
  }

//  public List<EventReference> eventReferences(final PcCreatureInstance pcCreatureInstance) {
//    return getEventReferenceService().findAllByEventReferenceEnabledEntityId(pcCreatureInstance.getId());
//  }

  public Inventory inventory(final PcCreatureInstance pcCreatureInstance) {
    return getInventoryService().findByInventoryEnabledEntityId(pcCreatureInstance.getId());
  }

  public DropTable dropTable(final PcCreatureInstance pcCreatureInstance) {
    return getDropTableService().findByDropTableEnabledEntityId(pcCreatureInstance.getId());
  }

  public Creature creature(final PcCreatureInstance pcCreatureInstance) {
    return getCreatureService().findByCreatureInstanceId(pcCreatureInstance.getId());
  }

  public List<Combatant> combatants(final PcCreatureInstance pcCreatureInstance) {
    return getCombatantService().findAllByCreatureInstanceId(pcCreatureInstance.getId());
  }

  public Combatant currentCombatant(final PcCreatureInstance pcCreatureInstance) {
    return getCombatantService().findCurrentByCreatureInstanceId(pcCreatureInstance.getId());
  }

  public Roster roster(final PcCreatureInstance pcCreatureInstance) {
    return getRosterService().findByCreatureInstanceId(pcCreatureInstance.getId());
  }

  public List<ConsistentCreatureSpawn> consistentCreatureSpawns(final PcCreatureInstance pcCreatureInstance) {
    return getConsistentCreatureSpawnService().findAllByCreatureInstanceToCopyId(pcCreatureInstance.getId());
  }

  // TODO This should probably be a connection/paginated request
  // TODO It might also makes sense to create accessors for current cell, claimed cell, discovered cells, etc., then this can just paginate through all of them
  public List<PcCreatureInstanceCell> pcCreatureInstanceCells(final PcCreatureInstance pcCreatureInstance, final boolean onlyClaimed,
      final boolean onlyDiscovered, final boolean onlyCurrent) {
    if (onlyClaimed || onlyDiscovered || onlyCurrent) {
      return pcCreatureInstance.getPcCreatureInstanceCells().stream()
          .filter(creatureInstanceCell -> onlyClaimed == creatureInstanceCell.isClaimed()
              || onlyDiscovered == creatureInstanceCell.isDiscovered()
              || onlyCurrent == creatureInstanceCell.isCurrent())
          .collect(Collectors.toList());
    }
    return pcCreatureInstance.getPcCreatureInstanceCells();
  }

  public PcCreatureInstanceCell currentPcCreatureInstanceCell(final PcCreatureInstance pcCreatureInstance) {
    return getPcCreatureInstanceCellService().findByPcCreatureInstanceIdAsCurrent(pcCreatureInstance.getId());
  }

  public Account account(final PcCreatureInstance pcCreatureInstance) {
    return pcCreatureInstance.getAccount();
  }
}
