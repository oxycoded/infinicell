package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.Inventory;
import com.oxycoded.infinicell.domain.model.InventoryEnabledEntity;
import com.oxycoded.infinicell.domain.model.ItemInstance;
import com.oxycoded.infinicell.service.InventoryEnabledEntityService;
import com.oxycoded.infinicell.service.ItemInstanceService;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class InventoryResolver implements GraphQLResolver<Inventory> {

  private final InventoryEnabledEntityService inventoryEnabledEntityService;
  private final ItemInstanceService itemInstanceService;

  public InventoryEnabledEntity inventoryEnabledEntity(final Inventory inventory) {
    return getInventoryEnabledEntityService().findByInventoryId(inventory.getId());
  }

  public List<ItemInstance> itemInstances(final Inventory inventory) {
    return getItemInstanceService().findAllByInventoryId(inventory.getId());
  }
}
