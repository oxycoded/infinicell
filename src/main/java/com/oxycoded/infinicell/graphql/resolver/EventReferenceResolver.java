package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.Event;
import com.oxycoded.infinicell.domain.model.EventReference;
import com.oxycoded.infinicell.domain.model.EventReferenceEnabledEntity;
import com.oxycoded.infinicell.service.EventReferenceEnabledEntityService;
import com.oxycoded.infinicell.service.EventService;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class EventReferenceResolver implements GraphQLResolver<EventReference> {

  private final EventService eventService;
  private final EventReferenceEnabledEntityService eventReferenceEnabledEntityService;

  public Event event(final EventReference eventReference) {
    return getEventService().findByEventReferenceId(eventReference.getId());
  }

  public EventReferenceEnabledEntity eventReferenceEnabledEntity(final EventReference eventReference) {
    return getEventReferenceEnabledEntityService().findByEventReferenceId(eventReference.getId());
  }
}
