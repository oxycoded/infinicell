package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.*;
import com.oxycoded.infinicell.service.*;
import graphql.kickstart.tools.GraphQLResolver;
import graphql.relay.Connection;
import graphql.relay.SimpleListConnection;
import graphql.schema.DataFetchingEnvironment;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class CreatureResolver implements GraphQLResolver<Creature> {

  private final EventReferenceService eventReferenceService;
  private final InventoryService inventoryService;
  private final DropTableService dropTableService;
  private final CreatureInstanceService creatureInstanceService;
  private final RandomizedCreatureSpawnService randomizedCreatureSpawnService;

  public Connection<EventReference> eventReferences(final Creature creature, final int first, final int last,
      final String before, final String after, final DataFetchingEnvironment dataFetchingEnvironment) {
    final List<EventReference> list = getEventReferenceService().findAllByEventReferenceEnabledEntityId(creature.getId());
    return new SimpleListConnection<>(list, "cursor").get(dataFetchingEnvironment);
  }

  //  Non connection event references
  //  public List<EventReference> eventReferences(final Creature creature) {
  //    return getEventReferenceService().findAllByEventReferenceEnabledEntityId(creature.getId());
  //  }

  public Inventory inventory(final Creature creature) {
    return getInventoryService().findByInventoryEnabledEntityId(creature.getId());
  }

  public DropTable dropTable(final Creature creature) {
    return getDropTableService().findByDropTableEnabledEntityId(creature.getId());
  }

  // TODO This should probably be a connection/paginated request
  public List<CreatureInstance> creatureInstances(final Creature creature) {
    return getCreatureInstanceService().findAllByCreatureId(creature.getId());
  }

  // TODO This should probably be a connection/paginated request
  public List<RandomizedCreatureSpawn> randomizedCreatureSpawns(final Creature creature) {
    return randomizedCreatureSpawnService.findAllByCreatureId(creature.getId());
  }
}
