package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.Item;
import com.oxycoded.infinicell.domain.model.ItemInstance;
import com.oxycoded.infinicell.domain.model.RandomizedItemDrop;
import com.oxycoded.infinicell.service.ItemInstanceService;
import com.oxycoded.infinicell.service.RandomizedItemDropService;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class ItemResolver implements GraphQLResolver<Item> {

  private final ItemInstanceService itemInstanceService;
  private final RandomizedItemDropService randomizedItemDropService;

  // TODO This should probably be a connection/paginated request
  public List<ItemInstance> itemInstances(final Item item) {
    return itemInstanceService.findAllByItemId(item.getId());
  }

  // TODO This should probably be a connection/paginated request
  public List<RandomizedItemDrop> randomizedItemDrops(final Item item) {
    return randomizedItemDropService.findAllByItemId(item.getId());
  }
}
