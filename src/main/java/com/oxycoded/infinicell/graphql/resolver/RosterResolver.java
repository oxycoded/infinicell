package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.*;
import com.oxycoded.infinicell.service.*;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class RosterResolver implements GraphQLResolver<Roster> {

  private final RosterEnabledEntityService rosterEnabledEntityService;
  private final CreatureInstanceService creatureInstanceService;

  public RosterEnabledEntity rosterEnabledEntity(final Roster roster) {
    return getRosterEnabledEntityService().findByRosterId(roster.getId());
  }

  public List<CreatureInstance> creatureInstances(final Roster roster) {
    return getCreatureInstanceService().findAllByRosterId(roster.getId());
  }
}
