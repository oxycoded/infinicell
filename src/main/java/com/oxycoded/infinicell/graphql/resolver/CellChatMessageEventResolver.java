package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.CellChatMessageEvent;
import com.oxycoded.infinicell.domain.model.EventReference;
import com.oxycoded.infinicell.service.EventReferenceService;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class CellChatMessageEventResolver implements GraphQLResolver<CellChatMessageEvent> {

  private final EventReferenceService eventReferenceService;

  public List<EventReference> eventReferences(final CellChatMessageEvent cellChatMessageEvent) {
    return getEventReferenceService().findAllByEventId(cellChatMessageEvent.getId());
  }
}
