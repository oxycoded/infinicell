package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.DropTable;
import com.oxycoded.infinicell.domain.model.Item;
import com.oxycoded.infinicell.domain.model.RandomizedItemDrop;
import com.oxycoded.infinicell.service.DropTableService;
import com.oxycoded.infinicell.service.ItemService;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class RandomizedItemDropResolver implements GraphQLResolver<RandomizedItemDrop> {

  private final DropTableService dropTableService;
  private final ItemService itemService;

  public DropTable dropTable(final RandomizedItemDrop randomizedItemDrop) {
    return getDropTableService().findByRandomizedItemDropId(randomizedItemDrop.getId());
  }
  
  public Item item(final RandomizedItemDrop randomizedItemDrop) {
    return getItemService().findByRandomizedItemDropId(randomizedItemDrop.getId());
  }
}
