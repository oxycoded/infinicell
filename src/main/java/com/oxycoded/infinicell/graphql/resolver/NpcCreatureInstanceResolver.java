package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.*;
import com.oxycoded.infinicell.service.*;
import graphql.kickstart.tools.GraphQLResolver;
import graphql.relay.Connection;
import graphql.relay.SimpleListConnection;
import graphql.schema.DataFetchingEnvironment;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class NpcCreatureInstanceResolver implements GraphQLResolver<NpcCreatureInstance> {

  private final EventReferenceService eventReferenceService;
  private final InventoryService inventoryService;
  private final DropTableService dropTableService;
  private final CreatureService creatureService;
  private final RosterService rosterService;
  private final ConsistentCreatureSpawnService consistentCreatureSpawnService;
  private final CombatantService combatantService;

  public Connection<EventReference> eventReferences(final NpcCreatureInstance npcCreatureInstance, final int first, final int last,
      final String before, final String after, final DataFetchingEnvironment dataFetchingEnvironment) {
    final List<EventReference> list = getEventReferenceService().findAllByEventReferenceEnabledEntityId(npcCreatureInstance.getId());
    return new SimpleListConnection<>(list, "cursor").get(dataFetchingEnvironment);
  }

//  public List<EventReference> eventReferences(final NpcCreatureInstance npcCreatureInstance) {
//    return getEventReferenceService().findAllByEventReferenceEnabledEntityId(pcCreatureInstance.getId());
//  }

  public Inventory inventory(final NpcCreatureInstance npcCreatureInstance) {
    return getInventoryService().findByInventoryEnabledEntityId(npcCreatureInstance.getId());
  }

  public DropTable dropTable(final NpcCreatureInstance npcCreatureInstance) {
    return getDropTableService().findByDropTableEnabledEntityId(npcCreatureInstance.getId());
  }

  public Creature creature(final NpcCreatureInstance npcCreatureInstance) {
    return getCreatureService().findByCreatureInstanceId(npcCreatureInstance.getId());
  }

  public List<Combatant> combatants(final NpcCreatureInstance npcCreatureInstance) {
    return getCombatantService().findAllByCreatureInstanceId(npcCreatureInstance.getId());
  }

  public Combatant currentCombatant(final NpcCreatureInstance npcCreatureInstance) {
    return getCombatantService().findCurrentByCreatureInstanceId(npcCreatureInstance.getId());
  }

  public Roster roster(final NpcCreatureInstance npcCreatureInstance) {
    return getRosterService().findByCreatureInstanceId(npcCreatureInstance.getId());
  }

  public List<ConsistentCreatureSpawn> consistentCreatureSpawns(final NpcCreatureInstance npcCreatureInstance) {
    return getConsistentCreatureSpawnService().findAllByCreatureInstanceToCopyId(npcCreatureInstance.getId());
  }
}
