package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.*;
import com.oxycoded.infinicell.exception.PcCreatureInstanceNotFoundException;
import com.oxycoded.infinicell.service.CombatService;
import com.oxycoded.infinicell.service.CreatureInstanceService;
import com.oxycoded.infinicell.service.EventReferenceService;
import com.oxycoded.infinicell.service.PcCreatureInstanceService;
import graphql.kickstart.tools.GraphQLResolver;
import graphql.relay.Connection;
import graphql.relay.SimpleListConnection;
import graphql.schema.DataFetchingEnvironment;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
@Slf4j
public class CombatantResolver implements GraphQLResolver<Combatant> {

  private final EventReferenceService eventReferenceService;
  private final CreatureInstanceService creatureInstanceService;
  private final CombatService combatService;

  public Connection<EventReference> eventReferences(final Combatant combatant, final int first, final int last,
      final String before, final String after, final DataFetchingEnvironment dataFetchingEnvironment) {
    final List<EventReference> list = getEventReferenceService().findAllByEventReferenceEnabledEntityId(combatant.getId());
    return new SimpleListConnection<>(list, "cursor").get(dataFetchingEnvironment);
  }

  public CreatureInstance creatureInstance(final Combatant combatant) {
    return getCreatureInstanceService().findByCombatantId(combatant.getId());
  }

  public Combat combat(final Combatant combatant) {
    return getCombatService().findByCombatantId(combatant.getId());
  }
}
