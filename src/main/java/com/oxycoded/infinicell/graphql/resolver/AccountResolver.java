package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.Account;
import com.oxycoded.infinicell.domain.model.EventReference;
import com.oxycoded.infinicell.domain.model.PcCreatureInstance;
import com.oxycoded.infinicell.exception.PcCreatureInstanceNotFoundException;
import com.oxycoded.infinicell.service.EventReferenceService;
import com.oxycoded.infinicell.service.PcCreatureInstanceService;
import graphql.kickstart.tools.GraphQLResolver;
import graphql.relay.Connection;
import graphql.relay.SimpleListConnection;
import graphql.schema.DataFetchingEnvironment;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
@Slf4j
public class AccountResolver implements GraphQLResolver<Account> {

  private final EventReferenceService eventReferenceService;
  private final PcCreatureInstanceService pcCreatureInstanceService;

  public Connection<EventReference> eventReferences(final Account account, final int first, final int last,
      final String before, final String after, final DataFetchingEnvironment dataFetchingEnvironment) {
    final List<EventReference> list = getEventReferenceService().findAllByEventReferenceEnabledEntityId(account.getId());
    return new SimpleListConnection<>(list, "cursor").get(dataFetchingEnvironment);
  }

  public List<PcCreatureInstance> pcCreatureInstances(final Account account) {
    return getPcCreatureInstanceService().findAllByAccountId(account.getId());
  }

  public PcCreatureInstance selectedPcCreatureInstance(final Account account) {
    try {
      return getPcCreatureInstanceService().findBySelectedByAccountId(account.getId());
    } catch (final PcCreatureInstanceNotFoundException iwnfe) {
      log.debug("Account does not have an pc creature instance selected", iwnfe);
      return null;
    }
  }
}
