package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.Account;
import com.oxycoded.infinicell.domain.model.EventReference;
import com.oxycoded.infinicell.domain.model.Friend;
import com.oxycoded.infinicell.service.AccountService;
import com.oxycoded.infinicell.service.EventReferenceService;
import graphql.kickstart.tools.GraphQLResolver;
import graphql.relay.Connection;
import graphql.relay.SimpleListConnection;
import graphql.schema.DataFetchingEnvironment;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class FriendResolver implements GraphQLResolver<Friend> {

  private final EventReferenceService eventReferenceService;
  private final AccountService accountService;

  public Connection<EventReference> eventReferences(final Friend friend, final int first, final int last,
      final String before, final String after, final DataFetchingEnvironment dataFetchingEnvironment) {
    final List<EventReference> list = getEventReferenceService().findAllByEventReferenceEnabledEntityId(friend.getId());
    return new SimpleListConnection<>(list, "cursor").get(dataFetchingEnvironment);
  }

  public Account getAccount(final Friend friend) {
    return getAccountService().findByFriendId(friend.getId());
  }

  public Account getAccountOfFriend(final Friend friend) {
    return getAccountService().findByFriendOfId(friend.getId());
  }
}
