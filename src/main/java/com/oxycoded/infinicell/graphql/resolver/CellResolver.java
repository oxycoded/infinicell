package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.*;
import com.oxycoded.infinicell.service.*;
import graphql.kickstart.tools.GraphQLResolver;
import graphql.relay.Connection;
import graphql.relay.SimpleListConnection;
import graphql.schema.DataFetchingEnvironment;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class CellResolver implements GraphQLResolver<Cell> {

  private final EventReferenceService eventReferenceService;
  private final InventoryService inventoryService;
  private final DropTableService dropTableService;
  private final RosterService rosterService;
  private final SpawnTableService spawnTableService;
  private final PcCreatureInstanceService pcCreatureInstanceService;

  public Connection<EventReference> eventReferences(final Cell cell, final int first, final int last,
      final String before, final String after, final DataFetchingEnvironment dataFetchingEnvironment) {
    final List<EventReference> list = getEventReferenceService().findAllByEventReferenceEnabledEntityId(cell.getId());
    return new SimpleListConnection<>(list, "cursor").get(dataFetchingEnvironment);
  }

  //  Non connection event references
  //  public List<EventReference> eventReferences(final Cell cell) {
  //    return getEventReferenceService().findAllByEventReferenceEnabledEntityId(cell.getId());
  //  }

  public Inventory inventory(final Cell cell) {
    return getInventoryService().findByInventoryEnabledEntityId(cell.getId());
  }

  public DropTable dropTable(final Cell cell) {
    return getDropTableService().findByDropTableEnabledEntityId(cell.getId());
  }

  public Roster roster(final Cell cell) {
    return getRosterService().findByRosterEnabledEntityId(cell.getId());
  }

  public SpawnTable spawnTable(final Cell cell) {
    return getSpawnTableService().findBySpawnTableEnabledEntityId(cell.getId());
  }

  // TODO This should probably be a connection/paginated request
  // TODO It might also makes sense to create accessors for current pc creature instances, owner pc creature instance, discoverer pc creature instance, etc., then this can just paginate through all of them
  public List<PcCreatureInstanceCell> creatureInstanceCells(final Cell cell, final boolean onlyClaimed, final boolean onlyDiscovered,
      final boolean onlyCurrent) {
    if (onlyClaimed || onlyDiscovered || onlyCurrent) {
      return cell.getPcCreatureInstanceCells().stream()
          .filter(creatureInstanceCell -> onlyClaimed == creatureInstanceCell.isClaimed()
              || onlyDiscovered == creatureInstanceCell.isDiscovered()
              || onlyCurrent == creatureInstanceCell.isCurrent())
          .collect(Collectors.toList());
    }
    return cell.getPcCreatureInstanceCells();
  }

  public PcCreatureInstance discoveredBy(final Cell cell) {
    return getPcCreatureInstanceService().findByDiscoveredCellId(cell.getId());
  }
}
