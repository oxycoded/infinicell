package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.EventReference;
import com.oxycoded.infinicell.domain.model.ExperienceGainedEvent;
import com.oxycoded.infinicell.service.EventReferenceService;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class ExperienceGainedEventResolver implements GraphQLResolver<ExperienceGainedEvent> {

  private final EventReferenceService eventReferenceService;

  public List<EventReference> eventReferences(final ExperienceGainedEvent experienceGainedEvent) {
    return eventReferenceService.findAllByEventId(experienceGainedEvent.getId());
  }
}
