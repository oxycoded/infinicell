package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.ConsistentCreatureSpawn;
import com.oxycoded.infinicell.domain.model.RandomizedCreatureSpawn;
import com.oxycoded.infinicell.domain.model.SpawnTable;
import com.oxycoded.infinicell.domain.model.SpawnTableEnabledEntity;
import com.oxycoded.infinicell.service.ConsistentCreatureSpawnService;
import com.oxycoded.infinicell.service.RandomizedCreatureSpawnService;
import com.oxycoded.infinicell.service.SpawnTableEnabledEntityService;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class SpawnTableResolver implements GraphQLResolver<SpawnTable> {

  private final SpawnTableEnabledEntityService spawnTableEnabledEntityService;
  private final ConsistentCreatureSpawnService consistentCreatureSpawnService;
  private final RandomizedCreatureSpawnService randomizedCreatureSpawnService;

  public SpawnTableEnabledEntity spawnTableEnabledEntity(final SpawnTable spawnTable) {
    return getSpawnTableEnabledEntityService().findBySpawnTableId(spawnTable.getId());
  }

  public List<ConsistentCreatureSpawn> consistentCreatureSpawns(final SpawnTable spawnTable) {
    return getConsistentCreatureSpawnService().findAllBySpawnTableId(spawnTable.getId());
  }

  public List<RandomizedCreatureSpawn> randomizedCreatureSpawns(final SpawnTable spawnTable) {
    return getRandomizedCreatureSpawnService().findAllBySpawnTableId(spawnTable.getId());
  }
}
