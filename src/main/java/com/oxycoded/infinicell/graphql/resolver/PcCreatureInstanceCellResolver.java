package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.*;
import com.oxycoded.infinicell.service.*;
import graphql.kickstart.tools.GraphQLResolver;
import graphql.relay.Connection;
import graphql.relay.SimpleListConnection;
import graphql.schema.DataFetchingEnvironment;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class PcCreatureInstanceCellResolver implements GraphQLResolver<PcCreatureInstanceCell> {

  private final EventReferenceService eventReferenceService;
  private final InventoryService inventoryService;
  private final DropTableService dropTableService;
  private final RosterService rosterService;
  private final SpawnTableService spawnTableService;
  private final CellService cellService;
  private final PcCreatureInstanceService pcCreatureInstanceService;

  public Connection<EventReference> eventReferences(final PcCreatureInstanceCell pcCreatureInstanceCell, final int first, final int last,
      final String before, final String after, final DataFetchingEnvironment dataFetchingEnvironment) {
    final List<EventReference> list = getEventReferenceService().findAllByEventReferenceEnabledEntityId(pcCreatureInstanceCell.getId());
    return new SimpleListConnection<>(list, "cursor").get(dataFetchingEnvironment);
  }

  //  public List<EventReference> eventReferences(final CreatureInstanceCell creatureInstanceCell) {
  //    return getEventReferenceService().findAllByEventReferenceEnabledEntityId(creatureInstanceCell.getId());
  //  }

  public Inventory inventory(final PcCreatureInstanceCell pcCreatureInstanceCell) {
    return getInventoryService().findByInventoryEnabledEntityId(pcCreatureInstanceCell.getId());
  }

  public DropTable dropTable(final PcCreatureInstanceCell pcCreatureInstanceCell) {
    return getDropTableService().findByDropTableEnabledEntityId(pcCreatureInstanceCell.getId());
  }

  public Roster roster(final PcCreatureInstanceCell pcCreatureInstanceCell) {
    return getRosterService().findByRosterEnabledEntityId(pcCreatureInstanceCell.getId());
  }

  public SpawnTable spawnTable(final PcCreatureInstanceCell pcCreatureInstanceCell) {
    return getSpawnTableService().findBySpawnTableEnabledEntityId(pcCreatureInstanceCell.getId());
  }

  public Cell cell(final PcCreatureInstanceCell pcCreatureInstanceCell) {
    return getCellService().findByPcCreatureInstanceCellId(pcCreatureInstanceCell.getId());
  }

  public PcCreatureInstance pcCreatureInstance(final PcCreatureInstanceCell pcCreatureInstanceCell) {
    return getPcCreatureInstanceService().findByPcCreatureInstanceCellId(pcCreatureInstanceCell.getId());
  }
}
