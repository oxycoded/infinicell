package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.ConsistentCreatureSpawn;
import com.oxycoded.infinicell.domain.model.CreatureInstance;
import com.oxycoded.infinicell.domain.model.NpcCreatureInstance;
import com.oxycoded.infinicell.domain.model.SpawnTable;
import com.oxycoded.infinicell.service.CreatureInstanceService;
import com.oxycoded.infinicell.service.NpcCreatureInstanceService;
import com.oxycoded.infinicell.service.SpawnTableService;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class ConsistentCreatureSpawnResolver implements GraphQLResolver<ConsistentCreatureSpawn> {

  private final SpawnTableService spawnTableService;
  private final CreatureInstanceService creatureInstanceService;

  public SpawnTable spawnTable(final ConsistentCreatureSpawn consistentCreatureSpawn) {
    return getSpawnTableService().findByConsistentCreatureSpawnId(consistentCreatureSpawn.getId());
  }

  public CreatureInstance creatureInstanceToCopy(final ConsistentCreatureSpawn consistentCreatureSpawn) {
    return getCreatureInstanceService().findByConsistentCreatureSpawnId(consistentCreatureSpawn.getId());
  }
}
