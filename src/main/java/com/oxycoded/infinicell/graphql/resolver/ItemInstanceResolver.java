package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.ConsistentItemDrop;
import com.oxycoded.infinicell.domain.model.Item;
import com.oxycoded.infinicell.domain.model.ItemInstance;
import com.oxycoded.infinicell.service.ConsistentItemDropService;
import com.oxycoded.infinicell.service.ItemService;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class ItemInstanceResolver implements GraphQLResolver<ItemInstance> {

  private final ItemService itemService;
  private final ConsistentItemDropService consistentItemDropService;

  public Item item(final ItemInstance itemInstance) {
    return getItemService().findByItemInstanceId(itemInstance.getId());
  }

  // TODO This should probably be a connection/paginated request
  public List<ConsistentItemDrop> consistentItemDrops(final ItemInstance itemInstance) {
    return consistentItemDropService.findAllByItemInstanceToCopyId(itemInstance.getId());
  }
}
