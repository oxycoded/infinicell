package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.ConsistentItemDrop;
import com.oxycoded.infinicell.domain.model.DropTable;
import com.oxycoded.infinicell.domain.model.ItemInstance;
import com.oxycoded.infinicell.service.DropTableService;
import com.oxycoded.infinicell.service.ItemInstanceService;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class ConsistentItemDropResolver implements GraphQLResolver<ConsistentItemDrop> {

  private final DropTableService dropTableService;
  private final ItemInstanceService itemInstanceService;

  public DropTable dropTable(final ConsistentItemDrop consistentItemDrop) {
    return getDropTableService().findByConsistentItemDropId(consistentItemDrop.getId());
  }

  public ItemInstance itemInstanceToCopy(final ConsistentItemDrop consistentItemDrop) {
    return getItemInstanceService().findByConsistentItemDropId(consistentItemDrop.getId());
  }
}
