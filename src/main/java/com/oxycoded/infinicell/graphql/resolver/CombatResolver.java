package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.*;
import com.oxycoded.infinicell.service.*;
import graphql.kickstart.tools.GraphQLResolver;
import graphql.relay.Connection;
import graphql.relay.SimpleListConnection;
import graphql.schema.DataFetchingEnvironment;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
@Slf4j
public class CombatResolver implements GraphQLResolver<Combat> {

  private final EventReferenceService eventReferenceService;
  private final PcCreatureInstanceCellService pcCreatureInstanceCellService;
  private final CombatantService combatantService;

  public Connection<EventReference> eventReferences(final Combat combat, final int first, final int last,
      final String before, final String after, final DataFetchingEnvironment dataFetchingEnvironment) {
    final List<EventReference> list = getEventReferenceService().findAllByEventReferenceEnabledEntityId(combat.getId());
    return new SimpleListConnection<>(list, "cursor").get(dataFetchingEnvironment);
  }

  public PcCreatureInstanceCell pcCreatureInstanceCell(final Combat combat) {
    return getPcCreatureInstanceCellService().findByCombatId(combat.getId());
  }

  public List<Combatant> combatants(final Combat combat) {
    return getCombatantService().findAllByCombatId(combat.getId());
  }
}
