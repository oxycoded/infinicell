package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.ConsistentItemDrop;
import com.oxycoded.infinicell.domain.model.DropTable;
import com.oxycoded.infinicell.domain.model.DropTableEnabledEntity;
import com.oxycoded.infinicell.domain.model.RandomizedItemDrop;
import com.oxycoded.infinicell.service.ConsistentItemDropService;
import com.oxycoded.infinicell.service.DropTableEnabledEntityService;
import com.oxycoded.infinicell.service.RandomizedItemDropService;
import graphql.kickstart.tools.GraphQLResolver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class DropTableResolver implements GraphQLResolver<DropTable> {

  private final DropTableEnabledEntityService dropTableEnabledEntityService;
  private final ConsistentItemDropService consistentItemDropService;
  private final RandomizedItemDropService randomizedItemDropService;

  public DropTableEnabledEntity dropTableEnabledEntity(final DropTable dropTable) {
    return getDropTableEnabledEntityService().findByDropTableId(dropTable.getId());
  }

  public List<ConsistentItemDrop> consistentItemDrops(final DropTable dropTable) {
    return getConsistentItemDropService().findAllByDropTableId(dropTable.getId());
  }

  public List<RandomizedItemDrop> randomizedItemDrops(final DropTable dropTable) {
    return getRandomizedItemDropService().findAllByDropTableId(dropTable.getId());
  }
}
