package com.oxycoded.infinicell.graphql.resolver;

import com.oxycoded.infinicell.domain.model.Account;
import com.oxycoded.infinicell.domain.model.EventReference;
import com.oxycoded.infinicell.domain.model.FriendRequest;
import com.oxycoded.infinicell.service.AccountService;
import com.oxycoded.infinicell.service.EventReferenceService;
import graphql.kickstart.tools.GraphQLResolver;
import graphql.relay.Connection;
import graphql.relay.SimpleListConnection;
import graphql.schema.DataFetchingEnvironment;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class FriendRequestResolver implements GraphQLResolver<FriendRequest> {

  private final EventReferenceService eventReferenceService;
  private final AccountService accountService;

  public Account requestingAccount(final FriendRequest friendRequest) {
    return getAccountService().findByRequestingAccountOfFriendRequestId(friendRequest.getId());
  }

  public Account recipientAccount(final FriendRequest friendRequest) {
    return getAccountService().findByRecipientAccountOfFriendRequestId(friendRequest.getId());
  }

  public Connection<EventReference> eventReferences(final FriendRequest friendRequest, final int first, final int last,
      final String before, final String after, final DataFetchingEnvironment dataFetchingEnvironment) {
    final List<EventReference> list = getEventReferenceService().findAllByEventReferenceEnabledEntityId(friendRequest.getId());
    return new SimpleListConnection<>(list, "cursor").get(dataFetchingEnvironment);
  }
}
