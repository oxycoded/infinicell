package com.oxycoded.infinicell.graphql.query.resolver;

import com.oxycoded.infinicell.domain.model.CreatureInstance;
import com.oxycoded.infinicell.domain.model.PcCreatureInstance;
import com.oxycoded.infinicell.domain.model.ItemInstance;
import com.oxycoded.infinicell.service.AccountService;
import com.oxycoded.infinicell.service.CreatureInstanceService;
import com.oxycoded.infinicell.service.PcCreatureInstanceService;
import com.oxycoded.infinicell.service.ItemInstanceService;
import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class Query implements GraphQLQueryResolver {

  private final AccountService accountService;
  private final PcCreatureInstanceService pcCreatureInstanceService;
  private final ItemInstanceService itemInstanceService;
  private final CreatureInstanceService creatureInstanceService;

  public List<PcCreatureInstance> getPcCreatureInstances() {
    return getPcCreatureInstanceService().findAll();
  }

  public PcCreatureInstance getPcCreatureInstance(final String id) {
    return getPcCreatureInstanceService().findById(id);
  }

  public ItemInstance getItemInstance(final String id) {
    return getItemInstanceService().findById(id);
  }

  public CreatureInstance getCreatureInstance(final String id) {
    return getCreatureInstanceService().findById(id);
  }
}
