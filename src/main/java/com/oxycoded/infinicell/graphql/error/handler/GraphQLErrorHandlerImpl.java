package com.oxycoded.infinicell.graphql.error.handler;

import com.oxycoded.infinicell.exception.InfinicellRuntimeException;
import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import graphql.kickstart.execution.error.GenericGraphQLError;
import graphql.kickstart.execution.error.GraphQLErrorHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class GraphQLErrorHandlerImpl implements GraphQLErrorHandler {

  private final boolean debugMode;

  public GraphQLErrorHandlerImpl(@Value("${debug-mode}") final boolean debugMode) {
    this.debugMode = debugMode;
  }

  @Override
  public List<GraphQLError> processErrors(List<GraphQLError> errors) {
    return errors.stream().map(this::processError).collect(Collectors.toList());
  }

  private GraphQLError processError(final GraphQLError graphQLError) {
    if (graphQLError instanceof ExceptionWhileDataFetching) {
      final ExceptionWhileDataFetching exceptionWhileDataFetching = (ExceptionWhileDataFetching) graphQLError;
      final Throwable rootCause = exceptionWhileDataFetching.getException();
      if (rootCause instanceof InfinicellRuntimeException) {
        return (GraphQLError) exceptionWhileDataFetching.getException();
      } else if (rootCause instanceof AccessDeniedException) {
        return new GenericGraphQLError(rootCause.getMessage());
      }
    }
    if(debugMode) {
      return graphQLError;
    }
    return new GenericGraphQLError("Unexpected exception occurred");
  }
}
