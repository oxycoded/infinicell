package com.oxycoded.infinicell.graphql.subscription.resolver;

import com.oxycoded.infinicell.domain.model.Event;
import com.oxycoded.infinicell.publisher.EventPublisher;
import graphql.kickstart.tools.GraphQLSubscriptionResolver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class Subscription implements GraphQLSubscriptionResolver {

  private final EventPublisher eventPublisher;

  public Publisher<Event> cellEvents(final String cellId) {
    return getEventPublisher().getCellEventPublisher(cellId);
  }

  public Publisher<Event> pcCreatureInstanceEvents(final String pcCreatureInstanceId) {
    return getEventPublisher().getPcCreatureInstanceEventPublisher(pcCreatureInstanceId);
  }
}
