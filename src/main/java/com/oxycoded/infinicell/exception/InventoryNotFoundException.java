package com.oxycoded.infinicell.exception;

public class InventoryNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That inventory does not exist";
  public InventoryNotFoundException() {
    super(message);
  }
}
