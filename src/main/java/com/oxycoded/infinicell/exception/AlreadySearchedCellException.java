package com.oxycoded.infinicell.exception;

public class AlreadySearchedCellException extends InfinicellRuntimeException {
  private static final String message = "You have already searched this cell";
  public AlreadySearchedCellException() {
    super(message);
  }
}
