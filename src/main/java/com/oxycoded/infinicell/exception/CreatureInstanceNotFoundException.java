package com.oxycoded.infinicell.exception;

public class CreatureInstanceNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That creature instance does not exist";
  public CreatureInstanceNotFoundException() {
    super(message);
  }
}
