package com.oxycoded.infinicell.exception;

public class ItemNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That item does not exist";
  public ItemNotFoundException() {
    super(message);
  }
}
