package com.oxycoded.infinicell.exception;

public class SpawnTableNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That spawn table does not exist";
  public SpawnTableNotFoundException() {
    super(message);
  }
}
