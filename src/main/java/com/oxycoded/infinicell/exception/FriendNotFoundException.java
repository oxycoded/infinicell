package com.oxycoded.infinicell.exception;

public class FriendNotFoundException extends InfinicellRuntimeException {
  private static final String genericMessage = "That friend does not exist";
  public FriendNotFoundException() {
    super(genericMessage);
  }
}
