package com.oxycoded.infinicell.exception;

public class PcCreatureInstanceNotFoundException extends InfinicellRuntimeException {
  private static final String genericMessage = "That pc creature instance does not exist";
  private static final String specificMessage = "The following pc creature instance id does not exist: ";
  public PcCreatureInstanceNotFoundException() {
    super(genericMessage);
  }
  public PcCreatureInstanceNotFoundException(final String pcCreatureInstanceId) {
    super(specificMessage + pcCreatureInstanceId);
  }
}
