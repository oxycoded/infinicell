package com.oxycoded.infinicell.exception;

public class SpawnTableEnabledEntityNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That spawn table enabled entity does not exist";

  public SpawnTableEnabledEntityNotFoundException() {
    super(message);
  }
}
