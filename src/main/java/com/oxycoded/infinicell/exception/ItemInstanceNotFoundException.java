package com.oxycoded.infinicell.exception;

public class ItemInstanceNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That item instance does not exist";
  public ItemInstanceNotFoundException() {
    super(message);
  }
}
