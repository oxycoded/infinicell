package com.oxycoded.infinicell.exception;

public class AccountNotFoundException extends InfinicellRuntimeException {
  private static final String genericMessage = "That account does not exist";
  private static final String specificMessage = "The following account id does not exist: ";
  public AccountNotFoundException() {
    super(genericMessage);
  }
  public AccountNotFoundException(final String accountId) {
    super(specificMessage + accountId);
  }
}
