package com.oxycoded.infinicell.exception;

public class DropTableNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That drop table does not exist";
  public DropTableNotFoundException() {
    super(message);
  }
}
