package com.oxycoded.infinicell.exception;

public class UnexpectedException extends InfinicellRuntimeException {
  private static final String message = "An unexpected exception occurred.";
  public UnexpectedException() {
    super(message);
  }
}
