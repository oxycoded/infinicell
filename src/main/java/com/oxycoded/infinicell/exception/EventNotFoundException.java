package com.oxycoded.infinicell.exception;

public class EventNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That event does not exist";
  public EventNotFoundException() {
    super(message);
  }
}
