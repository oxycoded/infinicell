package com.oxycoded.infinicell.exception;

public class LogInCodeNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That log in code is invalid";
  public LogInCodeNotFoundException() {
    super(message);
  }
}
