package com.oxycoded.infinicell.exception;

public class DropTableEnabledEntityNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That drop table enabled entity does not exist";

  public DropTableEnabledEntityNotFoundException() {
    super(message);
  }
}
