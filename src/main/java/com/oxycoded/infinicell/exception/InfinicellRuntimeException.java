package com.oxycoded.infinicell.exception;

import graphql.ErrorClassification;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.List;

public class InfinicellRuntimeException extends RuntimeException implements GraphQLError {
  public InfinicellRuntimeException(final String message) {
    super(message);
  }

  @Override
  public List<SourceLocation> getLocations() {
    return null;
  }

  @Override
  public ErrorClassification getErrorType() {
    return null;
  }
}
