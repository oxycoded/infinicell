package com.oxycoded.infinicell.exception;

public class AlreadyScoutedCellException extends InfinicellRuntimeException {
  private static final String message = "You have already scouted this cell";
  public AlreadyScoutedCellException() {
    super(message);
  }
}
