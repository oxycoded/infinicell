package com.oxycoded.infinicell.exception;

public class InventoryEnabledEntityNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That inventory enabled entity does not exist";
  public InventoryEnabledEntityNotFoundException() {
    super(message);
  }
}
