package com.oxycoded.infinicell.exception;

public class FriendRequestNotFoundException extends InfinicellRuntimeException {
  private static final String specificMessage = "The following friend request id does not exist: ";
  public FriendRequestNotFoundException(final String friendRequestId) {
    super(specificMessage + friendRequestId);
  }
}
