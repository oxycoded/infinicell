package com.oxycoded.infinicell.exception;

public class CreatureInstanceCellNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That creature instance cell does not exist";
  public CreatureInstanceCellNotFoundException() {
    super(message);
  }
}
