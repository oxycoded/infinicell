package com.oxycoded.infinicell.exception;

public class EventReferenceEnabledEntityNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That event reference enabled entity does not exist";
  public EventReferenceEnabledEntityNotFoundException() {
    super(message);
  }
}
