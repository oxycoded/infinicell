package com.oxycoded.infinicell.exception;

public class RosterEnabledEntityNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That roster enabled entity does not exist";
  public RosterEnabledEntityNotFoundException() {
    super(message);
  }
}
