package com.oxycoded.infinicell.exception;

public class CreatureNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That creature does not exist";
  public CreatureNotFoundException() {
    super(message);
  }
}
