package com.oxycoded.infinicell.exception;

public class CellNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That cell does not exist";
  public CellNotFoundException() {
    super(message);
  }
}
