package com.oxycoded.infinicell.exception;

public class DuplicateUsernameException extends InfinicellRuntimeException {
  private static final String genericMessage = "That username is already in use";
  private static final String specificMessage = "That following username has already been taken: ";
  public DuplicateUsernameException() {
    super(genericMessage);
  }
  public DuplicateUsernameException(final String username) {
    super(specificMessage + username);
  }
}
