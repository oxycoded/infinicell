package com.oxycoded.infinicell.exception;

public class RosterNotFoundException extends InfinicellRuntimeException {
  private static final String message = "That roster does not exist";
  public RosterNotFoundException() {
    super(message);
  }
}
