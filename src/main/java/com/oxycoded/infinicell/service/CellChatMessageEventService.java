package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.CellChatMessageEvent;
import com.oxycoded.infinicell.repository.CellChatMessageEventRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class CellChatMessageEventService {

  private final CellChatMessageEventRepository cellChatMessageEventRepository;

  public CellChatMessageEvent save(final CellChatMessageEvent cellChatMessageEvent) {
    return getCellChatMessageEventRepository().save(cellChatMessageEvent);
  }
}
