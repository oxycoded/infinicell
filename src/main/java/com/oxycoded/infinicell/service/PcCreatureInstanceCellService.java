package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.Cell;
import com.oxycoded.infinicell.domain.model.CreatureInstance;
import com.oxycoded.infinicell.domain.model.PcCreatureInstanceCell;
import com.oxycoded.infinicell.domain.model.PcCreatureInstance;
import com.oxycoded.infinicell.exception.CreatureInstanceCellNotFoundException;
import com.oxycoded.infinicell.repository.PcCreatureInstanceCellRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class PcCreatureInstanceCellService {

  private final PcCreatureInstanceCellRepository pcCreatureInstanceCellRepository;

  public PcCreatureInstanceCell createPcCreatureInstanceCell(final Cell cell, final PcCreatureInstance pcCreatureInstance) {
    final PcCreatureInstanceCell pcCreatureInstanceCell = new PcCreatureInstanceCell();
    pcCreatureInstanceCell.setPcCreatureInstance(pcCreatureInstance);
    pcCreatureInstanceCell.setCell(cell);
    return save(pcCreatureInstanceCell);
  }

  public PcCreatureInstanceCell findByPcCreatureInstanceIdAsCurrent(final String pcCreatureInstanceId) {
    final Optional<PcCreatureInstanceCell> pcCreatureInstanceCellOptional = getPcCreatureInstanceCellRepository()
        .findByPcCreatureInstanceIdAndCurrent(pcCreatureInstanceId, true);
    return pcCreatureInstanceCellOptional.orElseThrow(CreatureInstanceCellNotFoundException::new);
  }

  public PcCreatureInstanceCell findByCombatId(final String combatId) {
    final Optional<PcCreatureInstanceCell> pcCreatureInstanceCellOptional = getPcCreatureInstanceCellRepository().findByCombatsId(combatId);
    return pcCreatureInstanceCellOptional.orElse(null);
  }

  public List<PcCreatureInstanceCell> findAllByPcCreatureInstanceId(final String pcCreatureInstanceId) {
    return getPcCreatureInstanceCellRepository().findAllByPcCreatureInstanceId(pcCreatureInstanceId);
  }

  public PcCreatureInstanceCell findByCellIdAndPcCreatureInstanceId(final String cellId, final String pcCreatureInstanceId) {
    final Optional<PcCreatureInstanceCell> pcCreatureInstanceCellOptional = getPcCreatureInstanceCellRepository()
        .findByCellIdAndPcCreatureInstanceId(cellId, pcCreatureInstanceId);
    return pcCreatureInstanceCellOptional.orElseThrow(CreatureInstanceCellNotFoundException::new);
  }

  public PcCreatureInstanceCell save(final PcCreatureInstanceCell pcCreatureInstanceCell) {
    return getPcCreatureInstanceCellRepository().save(pcCreatureInstanceCell);
  }
}
