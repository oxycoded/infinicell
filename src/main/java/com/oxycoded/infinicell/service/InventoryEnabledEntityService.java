package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.InventoryEnabledEntity;
import com.oxycoded.infinicell.exception.InventoryEnabledEntityNotFoundException;
import com.oxycoded.infinicell.repository.InventoryEnabledEntityRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class InventoryEnabledEntityService {

  private final InventoryEnabledEntityRepository inventoryEnabledEntityRepository;

  public InventoryEnabledEntity findByInventoryId(final String inventoryId) {
    final Optional<InventoryEnabledEntity> inventoryEnabledEntityOptional = getInventoryEnabledEntityRepository().findByInventoryId(inventoryId);

    if (inventoryEnabledEntityOptional.isEmpty()) {
      throw new InventoryEnabledEntityNotFoundException();
    }

    return inventoryEnabledEntityOptional.get();
  }
}
