package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.ConsistentCreatureSpawn;
import com.oxycoded.infinicell.domain.model.CreatureInstance;
import com.oxycoded.infinicell.domain.model.NpcCreatureInstance;
import com.oxycoded.infinicell.domain.model.SpawnTable;
import com.oxycoded.infinicell.repository.ConsistentCreatureSpawnRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class ConsistentCreatureSpawnService {

  private final ConsistentCreatureSpawnRepository consistentCreatureSpawnRepository;

  public ConsistentCreatureSpawn createConsistentCreatureSpawn(final SpawnTable spawnTable, final CreatureInstance creatureInstance, final long spawnChance) {
    final ConsistentCreatureSpawn consistentCreatureSpawn = new ConsistentCreatureSpawn();
    consistentCreatureSpawn.setSpawnTable(spawnTable);
    consistentCreatureSpawn.setCreatureInstanceToCopy(creatureInstance);
    consistentCreatureSpawn.setSpawnChance(spawnChance);
    return save(consistentCreatureSpawn);
  }

  public List<ConsistentCreatureSpawn> findAllBySpawnTableId(final String spawnTableId) {
    return getConsistentCreatureSpawnRepository().findAllBySpawnTableId(spawnTableId);
  }
  
  public List<ConsistentCreatureSpawn> findAllByCreatureInstanceToCopyId(final String creatureInstanceToCopyId) {
    return getConsistentCreatureSpawnRepository().findAllByCreatureInstanceToCopyId(creatureInstanceToCopyId);
  }

  public ConsistentCreatureSpawn save(final ConsistentCreatureSpawn consistentCreatureSpawn) {
    return getConsistentCreatureSpawnRepository().save(consistentCreatureSpawn);
  }
}
