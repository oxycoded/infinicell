package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.Friend;
import com.oxycoded.infinicell.exception.FriendNotFoundException;
import com.oxycoded.infinicell.repository.FriendRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class FriendService {

  private final FriendRepository friendRepository;

  public List<Friend> findAllByAccountId(final String accountId) {
    return getFriendRepository().findAllByAccountId(accountId);
  }

  public Friend findByAccountIdAndAccountOfFriendId(final String accountId, final String accountOfFriendId) {
    final Optional<Friend> friendOptional = getFriendRepository().findByAccountIdAndAccountOfFriendId(accountId, accountOfFriendId);
    if (friendOptional.isEmpty()) {
      throw new FriendNotFoundException();
    }
    return friendOptional.get();
  }

  public Friend save(final Friend friend) {
    return getFriendRepository().save(friend);
  }
}
