package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.Cell;
import com.oxycoded.infinicell.exception.CellNotFoundException;
import com.oxycoded.infinicell.repository.CellRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Optional;
import java.util.Random;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class CellService {

  private static final Random RANDOM = new SecureRandom();

  private final CellRepository cellRepository;

  public Cell createCell(final long x, final long y, final long z, final long explorationExperienceValue) {
    final Cell newCell = new Cell();
    newCell.setX(x);
    newCell.setY(y);
    newCell.setZ(z);
    newCell.setExplorationExperienceValue(explorationExperienceValue);
    return save(newCell);
  }

  public Cell findByPcCreatureInstanceCellId(final String pcCreatureInstanceCellId) {
    final Optional<Cell> cellOptional = getCellRepository().findByPcCreatureInstanceCellsId(pcCreatureInstanceCellId);
    if (cellOptional.isEmpty()) {
      throw new CellNotFoundException();
    }
    return cellOptional.get();
  }

  public Cell findByXandYandZ(final long x, final long y, final long z) {
    final Optional<Cell> cellOptional = getCellRepository().findByXAndYAndZ(x, y, z);
    if (cellOptional.isEmpty()) {
      throw new CellNotFoundException();
    }
    return cellOptional.get();
  }

  public Cell save(final Cell cell) {
    return getCellRepository().save(cell);
  }
}
