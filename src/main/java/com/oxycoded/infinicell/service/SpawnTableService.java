package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.SpawnTable;
import com.oxycoded.infinicell.domain.model.SpawnTableEnabledEntity;
import com.oxycoded.infinicell.exception.SpawnTableNotFoundException;
import com.oxycoded.infinicell.repository.SpawnTableRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class SpawnTableService {

  private final SpawnTableRepository spawnTableRepository;

  public SpawnTable createSpawnTable(final SpawnTableEnabledEntity spawnTableEnabledEntity) {
    final SpawnTable spawnTable = new SpawnTable();
    spawnTable.setSpawnTableEnabledEntity(spawnTableEnabledEntity);
    return save(spawnTable);
  }

  public SpawnTable findBySpawnTableEnabledEntityId(final String spawnTableEnabledEntityId) {
    final Optional<SpawnTable> spawnTableOptional = getSpawnTableRepository().findBySpawnTableEnabledEntityId(spawnTableEnabledEntityId);

    if (spawnTableOptional.isEmpty()) {
      throw new SpawnTableNotFoundException();
    }

    return spawnTableOptional.get();
  }

  public SpawnTable findByConsistentCreatureSpawnId(final String consistentCreatureSpawnId) {
    final Optional<SpawnTable> spawnTableOptional = getSpawnTableRepository().findByConsistentCreatureSpawnsId(consistentCreatureSpawnId);

    if (spawnTableOptional.isEmpty()) {
      throw new SpawnTableNotFoundException();
    }

    return spawnTableOptional.get();
  }

  public SpawnTable findByRandomizedCreatureSpawnId(final String randomizedSpawnId) {
    final Optional<SpawnTable> spawnTableOptional = getSpawnTableRepository().findByRandomizedCreatureSpawnsId(randomizedSpawnId);

    if (spawnTableOptional.isEmpty()) {
      throw new SpawnTableNotFoundException();
    }

    return spawnTableOptional.get();
  }

  public SpawnTable save(final SpawnTable spawnTable) {
    return getSpawnTableRepository().save(spawnTable);
  }
}
