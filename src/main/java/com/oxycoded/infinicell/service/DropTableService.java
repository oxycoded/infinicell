package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.DropTable;
import com.oxycoded.infinicell.domain.model.DropTableEnabledEntity;
import com.oxycoded.infinicell.exception.DropTableNotFoundException;
import com.oxycoded.infinicell.repository.DropTableRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class DropTableService {

  private final DropTableRepository dropTableRepository;

  public DropTable createDropTable(final DropTableEnabledEntity dropTableEnabledEntity) {
    final DropTable dropTable = new DropTable();
    dropTable.setDropTableEnabledEntity(dropTableEnabledEntity);
    return save(dropTable);
  }

  public DropTable findByDropTableEnabledEntityId(final String dropTableEnabledEntityId) {
    final Optional<DropTable> dropTableOptional = getDropTableRepository().findByDropTableEnabledEntityId(dropTableEnabledEntityId);

    if (dropTableOptional.isEmpty()) {
      throw new DropTableNotFoundException();
    }

    return dropTableOptional.get();
  }

  public DropTable findByConsistentItemDropId(final String consistentItemDropId) {
    final Optional<DropTable> dropTableOptional = getDropTableRepository().findByConsistentItemDropsId(consistentItemDropId);

    if (dropTableOptional.isEmpty()) {
      throw new DropTableNotFoundException();
    }

    return dropTableOptional.get();
  }

  public DropTable findByRandomizedItemDropId(final String randomizedItemDropId) {
    final Optional<DropTable> dropTableOptional = getDropTableRepository().findByRandomizedItemDropsId(randomizedItemDropId);

    if (dropTableOptional.isEmpty()) {
      throw new DropTableNotFoundException();
    }

    return dropTableOptional.get();
  }

  public DropTable save(final DropTable dropTable) {
    return getDropTableRepository().save(dropTable);
  }
}
