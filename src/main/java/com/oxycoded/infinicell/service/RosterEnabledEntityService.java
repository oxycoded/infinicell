package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.RosterEnabledEntity;
import com.oxycoded.infinicell.exception.RosterEnabledEntityNotFoundException;
import com.oxycoded.infinicell.repository.RosterEnabledEntityRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class RosterEnabledEntityService {

  private final RosterEnabledEntityRepository rosterEnabledEntityRepository;

  public RosterEnabledEntity findByRosterId(final String rosterId) {
    final Optional<RosterEnabledEntity> rosterEnabledEntityOptional = getRosterEnabledEntityRepository().findByRosterId(rosterId);

    if (rosterEnabledEntityOptional.isEmpty()) {
      throw new RosterEnabledEntityNotFoundException();
    }

    return rosterEnabledEntityOptional.get();
  }
}
