package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.Event;
import com.oxycoded.infinicell.domain.model.EventReference;
import com.oxycoded.infinicell.domain.model.EventReferenceEnabledEntity;
import com.oxycoded.infinicell.domain.model.EventReferenceType;
import com.oxycoded.infinicell.repository.EventReferenceRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class EventReferenceService {

  private final EventReferenceRepository eventReferenceRepository;

  public EventReference createEventReference(final Event event, final EventReferenceEnabledEntity eventReferenceEnabledEntity,
      final EventReferenceType eventReferenceType) {
    final EventReference eventReference = new EventReference();
    eventReference.setEvent(event);
    eventReference.setEventReferenceEnabledEntity(eventReferenceEnabledEntity);
    eventReference.setEventReferenceType(eventReferenceType);
    return save(eventReference);
  }

  public EventReference save(final EventReference eventReference) {
    return getEventReferenceRepository().save(eventReference);
  }

  public List<EventReference> findAllByEventReferenceEnabledEntityId(final String eventReferenceEnabledEntityId) {
    return getEventReferenceRepository().findAllByEventReferenceEnabledEntityId(eventReferenceEnabledEntityId);
  }

  public List<EventReference> findAllByEventId(final String eventId) {
    return getEventReferenceRepository().findAllByEventId(eventId);
  }
}
