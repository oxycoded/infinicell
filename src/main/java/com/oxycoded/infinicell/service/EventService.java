package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.Event;
import com.oxycoded.infinicell.exception.EventNotFoundException;
import com.oxycoded.infinicell.publisher.EventPublisher;
import com.oxycoded.infinicell.repository.EventRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class EventService {

  private final EventRepository eventRepository;
  private final EventPublisher eventPublisher;

  public Event findByEventReferenceId(final String eventReferenceId) {
    final Optional<Event> eventOptional = getEventRepository().findByEventReferencesId(eventReferenceId);

    if(eventOptional.isEmpty()) {
      throw new EventNotFoundException();
    }

    return eventOptional.get();
  }

  public Event save(final Event event) {
    return getEventRepository().save(event);
  }

  public void sendCellEvent(final String destinationCellId, final Event event) {
    getEventPublisher().publishCellEvent(destinationCellId, event);
  }

  public void sendPcCreatureInstanceEvent(final String destinationPcCreatureInstanceId, final Event event) {
    getEventPublisher().publishPcCreatureInstanceEvent(destinationPcCreatureInstanceId, event);
  }

  public void sendAccountEvent(final String destinationAccountId, final Event event) {
    getEventPublisher().publishAccountEvent(destinationAccountId, event);
  }
}
