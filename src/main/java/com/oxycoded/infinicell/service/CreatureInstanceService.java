package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.Creature;
import com.oxycoded.infinicell.domain.model.CreatureInstance;
import com.oxycoded.infinicell.exception.CreatureInstanceNotFoundException;
import com.oxycoded.infinicell.repository.CreatureInstanceRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class CreatureInstanceService {

  private final CreatureInstanceRepository creatureInstanceRepository;

  public CreatureInstance findById(final String id) {
    final Optional<CreatureInstance> creatureInstanceOptional = getCreatureInstanceRepository().findById(id);
    return creatureInstanceOptional.orElse(null);
  }

  public List<CreatureInstance> findAllById(final List<String> creatureInstanceIds) {
    return getCreatureInstanceRepository().findAllById(creatureInstanceIds);
  }

  public CreatureInstance findByCombatantId(final String combatantId) {
    final Optional<CreatureInstance> creatureInstanceOptional = getCreatureInstanceRepository().findByCombatantsId(combatantId);
    return creatureInstanceOptional.orElse(null);
  }

  public List<CreatureInstance> findAllByCreatureId(final String creatureId) {
    return getCreatureInstanceRepository().findAllByCreatureId(creatureId);
  }

  public List<CreatureInstance> findAllByRosterId(final String rosterId) {
    return getCreatureInstanceRepository().findAllByRosterId(rosterId);
  }

  public CreatureInstance findByConsistentCreatureSpawnId(final String consistentCreatureSpawnId) {
    final Optional<CreatureInstance> creatureInstanceOptional = getCreatureInstanceRepository()
        .findByConsistentCreatureSpawnsId(consistentCreatureSpawnId);
    return creatureInstanceOptional.orElseThrow(CreatureInstanceNotFoundException::new);
  }

  public List<CreatureInstance> saveAll(final List<CreatureInstance> creatureInstances) {
    return getCreatureInstanceRepository().saveAll(creatureInstances);
  }
}
