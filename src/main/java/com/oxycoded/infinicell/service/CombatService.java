package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.Combat;
import com.oxycoded.infinicell.domain.model.CombatStatus;
import com.oxycoded.infinicell.domain.model.CreatureInstance;
import com.oxycoded.infinicell.domain.model.PcCreatureInstanceCell;
import com.oxycoded.infinicell.repository.CombatRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class CombatService {

  private final CombatRepository combatRepository;

  public Combat createCombat(final PcCreatureInstanceCell pcCreatureInstanceCell) {
    final Combat combat = new Combat();
    combat.setCombatStatus(CombatStatus.IN_PROGRESS);
    combat.setPcCreatureInstanceCell(pcCreatureInstanceCell);
    return save(combat);
  }

  public Combat findByCombatantId(final String combatantId) {
    final Optional<Combat> combatOptional = getCombatRepository().findByCombatantsId(combatantId);
    return combatOptional.orElse(null);
  }

  public Combat save(final Combat combat) {
    return getCombatRepository().save(combat);
  }
}
