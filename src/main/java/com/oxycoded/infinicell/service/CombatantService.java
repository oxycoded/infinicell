package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.*;
import com.oxycoded.infinicell.repository.CombatantRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class CombatantService {

  private final CombatantRepository combatantRepository;

  public Combatant createCombatant(final CreatureInstance creatureInstance, final Combat combat, final CombatPosition combatPosition,
      final CombatTeam combatTeam) {
    final Combatant combatant = new Combatant();
    combatant.setCombat(combat);
    combatant.setCreatureInstance(creatureInstance);
    combatant.setCombatantStatus(CombatantStatus.OK);
    combatant.setCombatPosition(combatPosition);
    combatant.setCombatTeam(combatTeam);
    return save(combatant);
  }

  public List<Combatant> findAllByCombatId(final String combatId) {
    return getCombatantRepository().findAllByCombatId(combatId);
  }

  public List<Combatant> findAllByCreatureInstanceId(final String creatureInstanceId) {
    return getCombatantRepository().findAllByCreatureInstanceId(creatureInstanceId);
  }

  public Combatant findCurrentByCreatureInstanceId(final String creatureInstanceId) {
    final Optional<Combatant> combatantOptional = getCombatantRepository().findByCreatureInstanceIdAndCombatantStatusIn(creatureInstanceId,
        Arrays.asList(CombatantStatus.OK, CombatantStatus.INCAPACITATED));
    return combatantOptional.orElse(null);
  }

  public Combatant save(final Combatant combatant) {
    return getCombatantRepository().save(combatant);
  }
}
