package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.SpawnTableEnabledEntity;
import com.oxycoded.infinicell.exception.SpawnTableEnabledEntityNotFoundException;
import com.oxycoded.infinicell.repository.SpawnTableEnabledEntityRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class SpawnTableEnabledEntityService {

  private final SpawnTableEnabledEntityRepository spawnTableEnabledEntityRepository;

  public SpawnTableEnabledEntity findBySpawnTableId(final String spawnTableId) {
    final Optional<SpawnTableEnabledEntity> spawnTableEnabledEntityOptional = getSpawnTableEnabledEntityRepository().findBySpawnTableId(spawnTableId);

    if (spawnTableEnabledEntityOptional.isEmpty()) {
      throw new SpawnTableEnabledEntityNotFoundException();
    }

    return spawnTableEnabledEntityOptional.get();
  }
}
