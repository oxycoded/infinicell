package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.*;
import com.oxycoded.infinicell.exception.CreatureInstanceNotFoundException;
import com.oxycoded.infinicell.exception.PcCreatureInstanceNotFoundException;
import com.oxycoded.infinicell.repository.PcCreatureInstanceRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class PcCreatureInstanceService {

  private final PcCreatureInstanceRepository pcCreatureInstanceRepository;

  private final PcCreatureInstanceCellService pcCreatureInstanceCellService;
  private final EventService eventService;
  private final EventReferenceService eventReferenceService;
  private final CellService cellService;
  private final CellChatMessageEventService cellChatMessageEventService;

  @Transactional
  public PcCreatureInstance createPcCreatureInstance(final Account account, final Creature creature, final String name, final String description) {
    final PcCreatureInstance pcCreatureInstance = new PcCreatureInstance();
    pcCreatureInstance.setName(name);
    pcCreatureInstance.setDescription(description);
    pcCreatureInstance.setAccount(account);
    pcCreatureInstance.setCreature(creature);
    pcCreatureInstance.setIconSpriteUrl(creature.getIconSpriteUrl());
    pcCreatureInstance.setDetailedSpriteUrl(creature.getDetailedSpriteUrl());
    return save(pcCreatureInstance);
  }

  @Transactional
  public PcCreatureInstance findBySelectedByAccountId(final String accountId) {
    final Optional<PcCreatureInstance> pcCreatureInstanceOptional = getPcCreatureInstanceRepository().findBySelectedByAccountId(accountId);
    if (pcCreatureInstanceOptional.isEmpty()) {
      throw new PcCreatureInstanceNotFoundException();
    }
    return pcCreatureInstanceOptional.get();
  }

  public PcCreatureInstance findByDiscoveredCellId(final String cellId) {
    final Optional<PcCreatureInstance> pcCreatureInstanceOptional = getPcCreatureInstanceRepository()
        .findByPcCreatureInstanceCellsCellIdAndPcCreatureInstanceCellsDiscovered(cellId, true);
    if (pcCreatureInstanceOptional.isEmpty()) {
      throw new CreatureInstanceNotFoundException();
    }
    return pcCreatureInstanceOptional.get();
  }

  public PcCreatureInstance findByPcCreatureInstanceCellId(final String pcCreatureInstanceCellId) {
    final Optional<PcCreatureInstance> pcCreatureInstanceOptional = getPcCreatureInstanceRepository().findByPcCreatureInstanceCellsId(pcCreatureInstanceCellId);
    if (pcCreatureInstanceOptional.isEmpty()) {
      throw new CreatureInstanceNotFoundException();
    }
    return pcCreatureInstanceOptional.get();
  }

  public PcCreatureInstance claimCurrentCell(final String pcCreatureInstanceId) {
    final PcCreatureInstance pcCreatureInstance = findById(pcCreatureInstanceId);
    if (pcCreatureInstance == null) {
      throw new RuntimeException("Infinity walker not found");
    }
    final PcCreatureInstanceCell currentPcCreatureInstanceCell = pcCreatureInstance.getCurrentPcCreatureInstanceCell();
    if (currentPcCreatureInstanceCell.getCell().isClaimed()) {
      throw new RuntimeException("Cell has already been claimed!");
    }

    pcCreatureInstance.getPcCreatureInstanceCells().stream().filter(PcCreatureInstanceCell::isClaimed)
        .forEach(pcCreatureInstanceCell -> {
          pcCreatureInstanceCell.setClaimed(false);
          getPcCreatureInstanceCellService().save(pcCreatureInstanceCell);
        });

    currentPcCreatureInstanceCell.setClaimed(true);

    final PcCreatureInstanceCell savedPcCreatureInstanceCell = getPcCreatureInstanceCellService().save(currentPcCreatureInstanceCell);

    return persistAndDistributeClaimEvent(pcCreatureInstance, savedPcCreatureInstanceCell.getCell());
  }

  public List<PcCreatureInstance> findAll() {
    return getPcCreatureInstanceRepository().findAll();
  }

  public List<PcCreatureInstance> findAllByAccountId(final String accountId) {
    return getPcCreatureInstanceRepository().findAllByAccountId(accountId);
  }

  public PcCreatureInstance findById(final String id) {
    final Optional<PcCreatureInstance> pcCreatureInstanceOptional = getPcCreatureInstanceRepository().findById(id);
    if (pcCreatureInstanceOptional.isEmpty()) {
      return null;
    }
    return pcCreatureInstanceOptional.get();
  }

  public CellChatMessageEvent sendLocalChat(final String pcCreatureInstanceId, final String message) {
    final PcCreatureInstance pcCreatureInstance = findById(pcCreatureInstanceId);
    if (pcCreatureInstance == null) {
      throw new RuntimeException("Infinity walker not found");
    }

    return persistAndDistributeLocalChatEvent(pcCreatureInstance, message);
  }

  public PcCreatureInstance save(final PcCreatureInstance pcCreatureInstance) {
    return getPcCreatureInstanceRepository().save(pcCreatureInstance);
  }

  private PcCreatureInstance persistAndDistributeClaimEvent(final PcCreatureInstance claimingPcCreatureInstance, final Cell claimedCell) {
    final Event claimCellEvent = new Event();
    claimCellEvent.setEventType(EventType.CLAIM_CELL);
    final Event savedClaimCellEvent = getEventService().save(claimCellEvent);

    final EventReference claimedCellEventReference = getEventReferenceService()
        .createEventReference(savedClaimCellEvent, claimedCell, EventReferenceType.CLAIMED_CELL);
    final EventReference claimingPcCreatureInstanceEventReference = getEventReferenceService()
        .createEventReference(savedClaimCellEvent, claimingPcCreatureInstance, EventReferenceType.CLAIMING_INFINITY_WALKER);

    savedClaimCellEvent.getEventReferences().add(claimedCellEventReference);
    savedClaimCellEvent.getEventReferences().add(claimingPcCreatureInstanceEventReference);

    claimedCell.getEventReferences().add(claimedCellEventReference);
    claimingPcCreatureInstance.getEventReferences().add(claimingPcCreatureInstanceEventReference);

    getEventService().sendCellEvent(claimedCell.getId(), savedClaimCellEvent);
    getEventService().sendPcCreatureInstanceEvent(claimingPcCreatureInstance.getId(), savedClaimCellEvent);

    getEventService().save(savedClaimCellEvent);
    getCellService().save(claimedCell);
    return save(claimingPcCreatureInstance);
  }

  private CellChatMessageEvent persistAndDistributeLocalChatEvent(final PcCreatureInstance messageAuthor, final String message) {
    final CellChatMessageEvent cellChatMessageEvent = new CellChatMessageEvent();
    cellChatMessageEvent.setMessage(message);
    final CellChatMessageEvent savedCellChatMessageEvent = getCellChatMessageEventService().save(cellChatMessageEvent);

    final Cell messageSentInCell = messageAuthor.getCurrentPcCreatureInstanceCell().getCell();

    final EventReference messageSentInCellEventReference = getEventReferenceService()
        .createEventReference(savedCellChatMessageEvent, messageSentInCell, EventReferenceType.MESSAGE_SENT_IN_CELL);
    final EventReference messageAuthorEventReference = getEventReferenceService()
        .createEventReference(savedCellChatMessageEvent, messageAuthor, EventReferenceType.MESSAGE_AUTHOR);

    savedCellChatMessageEvent.getEventReferences().add(messageSentInCellEventReference);
    savedCellChatMessageEvent.getEventReferences().add(messageAuthorEventReference);

    messageSentInCell.getEventReferences().add(messageSentInCellEventReference);
    messageAuthor.getEventReferences().add(messageAuthorEventReference);

    getEventService().sendCellEvent(messageSentInCell.getId(), savedCellChatMessageEvent);
    getEventService().sendPcCreatureInstanceEvent(messageAuthor.getId(), savedCellChatMessageEvent);

    save(messageAuthor);
    getCellService().save(messageSentInCell);
    return getCellChatMessageEventService().save(savedCellChatMessageEvent);
  }
}
