package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.Creature;
import com.oxycoded.infinicell.domain.model.Item;
import com.oxycoded.infinicell.exception.CreatureNotFoundException;
import com.oxycoded.infinicell.exception.ItemNotFoundException;
import com.oxycoded.infinicell.repository.CreatureRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class CreatureService {

  private final CreatureRepository creatureRepository;

  public Creature getTravelerCreature() {
    return findByName("Traveler");
  }

  public Creature findByName(final String name) {
    final Optional<Creature> creatureOptional = getCreatureRepository().findByName(name);
    if (creatureOptional.isEmpty()) {
      throw new CreatureNotFoundException();
    }
    return creatureOptional.get();
  }

  public Creature findByCreatureInstanceId(final String creatureInstanceId) {
    final Optional<Creature> creatureOptional = getCreatureRepository().findByCreatureInstancesId(creatureInstanceId);

    if(creatureOptional.isEmpty()) {
      throw new CreatureNotFoundException();
    }

    return creatureOptional.get();
  }

  public Creature findByRandomizedCreatureSpawnId(final String randomizedCreatureSpawnId) {
    final Optional<Creature> creatureOptional = getCreatureRepository().findByRandomizedCreatureSpawnsId(randomizedCreatureSpawnId);
    if (creatureOptional.isEmpty()) {
      throw new CreatureNotFoundException();
    }
    return creatureOptional.get();
  }

  public Creature save(final Creature creature) {
    return getCreatureRepository().save(creature);
  }

  public void saveAll(final Creature... creatures) {
    getCreatureRepository().saveAll(Arrays.asList(creatures));
  }

  @PostConstruct
  private void createTravelerCreature() {
    final Creature traveler = new Creature();
    traveler.setName("Traveler");
    traveler.setDescription("Goes places. Does things.");

    final Creature goldfish = new Creature();
    goldfish.setName("Goldfish");
    goldfish.setDescription("It may look harmless, but it doesn't take any carp.");
    goldfish.setIconSpriteUrl("goldfish-icon.png");

    final Creature magGoldfish = new Creature();
    magGoldfish.setName("Mag'Goldfish");
    magGoldfish.setDescription("What do they feed this thing?");
    magGoldfish.setIconSpriteUrl("maggoldfish-icon.png");

    final Creature forGoldfish = new Creature();
    forGoldfish.setName("For'Goldfish");
    forGoldfish.setDescription("I am afraid.");

    saveAll(traveler, goldfish, magGoldfish, forGoldfish);
  }
}
