package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.DropTableEnabledEntity;
import com.oxycoded.infinicell.exception.DropTableEnabledEntityNotFoundException;
import com.oxycoded.infinicell.repository.DropTableEnabledEntityRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class DropTableEnabledEntityService {

  private final DropTableEnabledEntityRepository dropTableEnabledEntityRepository;

  public DropTableEnabledEntity findByDropTableId(final String dropTableId) {
    final Optional<DropTableEnabledEntity> dropTableEnabledEntityOptional = getDropTableEnabledEntityRepository().findByDropTableId(dropTableId);

    if (dropTableEnabledEntityOptional.isEmpty()) {
      throw new DropTableEnabledEntityNotFoundException();
    }

    return dropTableEnabledEntityOptional.get();
  }
}
