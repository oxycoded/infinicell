package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.Account;
import com.oxycoded.infinicell.domain.model.AccountActivityType;
import com.oxycoded.infinicell.exception.AccountNotFoundException;
import com.oxycoded.infinicell.exception.LogInCodeNotFoundException;
import com.oxycoded.infinicell.repository.AccountRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class AccountService {

  private final AccountRepository accountRepository;

  @Transactional
  public Account createAnonymousAccount(final String username) {
    return save(Account.createAnonymousAccount(username));
  }

  @Transactional
  public Account createAccount(final String name, final String email, final String password, final String confirmPassword) {
    return getAccountRepository().save(Account.createAccount(name, email, password, confirmPassword));
  }

  @Transactional
  public Account findByLogInCode(final String logInCode) {
    final Optional<Account> accountOptional = getAccountRepository().findByLogInCode(logInCode);
    if (accountOptional.isEmpty()) {
      throw new LogInCodeNotFoundException();
    }
    return accountOptional.get();
  }

  @Transactional
  public Account findById(final String id) {
    final Optional<Account> accountOptional = getAccountRepository().findById(id);
    if (accountOptional.isEmpty()) {
      throw new AccountNotFoundException(id);
    }
    return accountOptional.get();
  }

  @Transactional
  public Account findByFriendId(final String friendId) {
    final Optional<Account> accountOptional = getAccountRepository().findByFriendsId(friendId);
    if (accountOptional.isEmpty()) {
      throw new AccountNotFoundException();
    }
    return accountOptional.get();
  }

  @Transactional
  public Account findByFriendOfId(final String friendOfId) {
    final Optional<Account> accountOptional = getAccountRepository().findByFriendOfsId(friendOfId);
    if (accountOptional.isEmpty()) {
      throw new AccountNotFoundException();
    }
    return accountOptional.get();
  }

  @Transactional
  public Account findByRequestingAccountOfFriendRequestId(final String friendRequestId) {
    final Optional<Account> accountOptional = getAccountRepository().findByOutgoingFriendRequestsId(friendRequestId);
    if (accountOptional.isEmpty()) {
      throw new AccountNotFoundException();
    }
    return accountOptional.get();
  }

  @Transactional
  public Account findByRecipientAccountOfFriendRequestId(final String friendRequestId) {
    final Optional<Account> accountOptional = getAccountRepository().findByIncomingFriendRequestsId(friendRequestId);
    if (accountOptional.isEmpty()) {
      throw new AccountNotFoundException();
    }
    return accountOptional.get();
  }

  public Account save(final Account account) {
    return getAccountRepository().save(account);
  }

  public void saveAll(final Iterable<Account> accounts) {
    getAccountRepository().saveAll(accounts);
  }
}
