package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.Creature;
import com.oxycoded.infinicell.domain.model.CreatureInstance;
import com.oxycoded.infinicell.domain.model.ItemInstance;
import com.oxycoded.infinicell.domain.model.NpcCreatureInstance;
import com.oxycoded.infinicell.exception.CreatureInstanceNotFoundException;
import com.oxycoded.infinicell.repository.NpcCreatureInstanceRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class NpcCreatureInstanceService {

  private final NpcCreatureInstanceRepository npcCreatureInstanceRepository;

  public NpcCreatureInstance createNpcCreatureInstance(final Creature creature) {
    final NpcCreatureInstance npcCreatureInstance = new NpcCreatureInstance();
    npcCreatureInstance.setName(creature.getName());
    npcCreatureInstance.setDescription(creature.getDescription());
    npcCreatureInstance.setCreature(creature);
    npcCreatureInstance.setIconSpriteUrl(creature.getIconSpriteUrl());
    npcCreatureInstance.setDetailedSpriteUrl(creature.getDetailedSpriteUrl());
    return save(npcCreatureInstance);
  }

  public NpcCreatureInstance createCopyOfCreatureInstance(final CreatureInstance creatureInstanceToCopy) {
    final NpcCreatureInstance npcCreatureInstance = new NpcCreatureInstance();
    npcCreatureInstance.setName(creatureInstanceToCopy.getName());
    npcCreatureInstance.setDescription(creatureInstanceToCopy.getDescription());
    npcCreatureInstance.setCreature(creatureInstanceToCopy.getCreature());
    npcCreatureInstance.setIconSpriteUrl(creatureInstanceToCopy.getIconSpriteUrl());
    npcCreatureInstance.setDetailedSpriteUrl(creatureInstanceToCopy.getDetailedSpriteUrl());
    return save(npcCreatureInstance);
  }

  public List<NpcCreatureInstance> findAllByRosterId(final String rosterId) {
    return getNpcCreatureInstanceRepository().findAllByRosterId(rosterId);
  }

  public NpcCreatureInstance save(final NpcCreatureInstance npcCreatureInstance) {
    return getNpcCreatureInstanceRepository().save(npcCreatureInstance);
  }
}
