package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.EventReferenceEnabledEntity;
import com.oxycoded.infinicell.exception.EventReferenceEnabledEntityNotFoundException;
import com.oxycoded.infinicell.repository.EventReferenceEnabledEntityRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class EventReferenceEnabledEntityService {

  private final EventReferenceEnabledEntityRepository eventReferenceEnabledEntityRepository;

  public EventReferenceEnabledEntity findByEventReferenceId(final String eventReferenceId) {
    final Optional<EventReferenceEnabledEntity> eventReferenceEnabledEntityOptional = getEventReferenceEnabledEntityRepository().findByEventReferencesId(eventReferenceId);

    if(eventReferenceEnabledEntityOptional.isEmpty()) {
      throw new EventReferenceEnabledEntityNotFoundException();
    }

    return eventReferenceEnabledEntityOptional.get();
  }
}
