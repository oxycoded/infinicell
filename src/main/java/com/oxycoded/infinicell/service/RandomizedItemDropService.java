package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.DropTable;
import com.oxycoded.infinicell.domain.model.Item;
import com.oxycoded.infinicell.domain.model.RandomizedItemDrop;
import com.oxycoded.infinicell.repository.RandomizedItemDropRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class RandomizedItemDropService {

  private final RandomizedItemDropRepository randomizedItemDropRepository;

  public RandomizedItemDrop createRandomizedItemDrop(final DropTable dropTable, final Item item, final long dropChance, final long minStacks,
      final long maxStacks) {
    final RandomizedItemDrop randomizedItemDrop = new RandomizedItemDrop();
    randomizedItemDrop.setDropTable(dropTable);
    randomizedItemDrop.setItem(item);
    randomizedItemDrop.setDropChance(dropChance);
    randomizedItemDrop.setMinStacks(minStacks);
    randomizedItemDrop.setMaxStacks(maxStacks);
    return save(randomizedItemDrop);
  }

  public List<RandomizedItemDrop> findAllByDropTableId(final String dropTableId) {
    return getRandomizedItemDropRepository().findAllByDropTableId(dropTableId);
  }

  public List<RandomizedItemDrop> findAllByItemId(final String itemId) {
    return getRandomizedItemDropRepository().findAllByItemId(itemId);
  }

  public RandomizedItemDrop save(final RandomizedItemDrop randomizedItemDrop) {
    return getRandomizedItemDropRepository().save(randomizedItemDrop);
  }
}
