package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.Item;
import com.oxycoded.infinicell.domain.model.ItemInstance;
import com.oxycoded.infinicell.exception.ItemInstanceNotFoundException;
import com.oxycoded.infinicell.repository.ItemInstanceRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class ItemInstanceService {

  private final ItemInstanceRepository itemInstanceRepository;

  public ItemInstance createItemInstance(final Item item, final long currentStacks) {
    final ItemInstance itemInstance = new ItemInstance();
    itemInstance.setItem(item);
    itemInstance.setName(item.getName());
    itemInstance.setRarity(item.getRarity());
    itemInstance.setTier(item.getTier());
    itemInstance.setValue(item.getValue());
    itemInstance.setCurrentStacks(currentStacks);
    itemInstance.setIconSpriteUrl(item.getIconSpriteUrl());
    itemInstance.setDetailedSpriteUrl(item.getDetailedSpriteUrl());
    return save(itemInstance);
  }

  public ItemInstance createCopyOfItemInstance(final ItemInstance itemInstanceToCopy) {
    final ItemInstance itemInstance = new ItemInstance();
    itemInstance.setItem(itemInstanceToCopy.getItem());
    itemInstance.setName(itemInstanceToCopy.getName());
    itemInstance.setRarity(itemInstanceToCopy.getRarity());
    itemInstance.setTier(itemInstanceToCopy.getTier());
    itemInstance.setValue(itemInstanceToCopy.getValue());
    itemInstance.setCurrentStacks(itemInstanceToCopy.getCurrentStacks());
    itemInstance.setIconSpriteUrl(itemInstanceToCopy.getIconSpriteUrl());
    itemInstance.setDetailedSpriteUrl(itemInstanceToCopy.getDetailedSpriteUrl());
    return save(itemInstance);
  }

  public ItemInstance findById(final String id) {
    final Optional<ItemInstance> itemInstanceOptional = getItemInstanceRepository().findById(id);
    return itemInstanceOptional.orElse(null);
  }

  public ItemInstance findByConsistentItemDropId(final String consistentItemDropId) {
    final Optional<ItemInstance> itemInstanceOptional = getItemInstanceRepository().findByConsistentItemDropsId(consistentItemDropId);
    return itemInstanceOptional.orElseThrow(ItemInstanceNotFoundException::new);
  }

  public List<ItemInstance> findAllByIdIn(final List<String> itemInstanceIds) {
    return getItemInstanceRepository().findAllByIdIn(itemInstanceIds);
  }

  public List<ItemInstance> findAllByItemId(final String itemId) {
    return getItemInstanceRepository().findAllByItemId(itemId);
  }

  public List<ItemInstance> findAllByInventoryId(final String inventoryId) {
    return getItemInstanceRepository().findAllByInventoryId(inventoryId);
  }

  public ItemInstance save(final ItemInstance itemInstance) {
    return getItemInstanceRepository().save(itemInstance);
  }

  public List<ItemInstance> saveAll(final List<ItemInstance> itemInstances) {
    return getItemInstanceRepository().saveAll(itemInstances);
  }

  public void deleteAll(final List<ItemInstance> itemInstances) {
    getItemInstanceRepository().deleteAll(itemInstances);
  }
}
