package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.Account;
import com.oxycoded.infinicell.domain.model.FriendRequest;
import com.oxycoded.infinicell.domain.model.FriendRequestStatus;
import com.oxycoded.infinicell.exception.FriendRequestNotFoundException;
import com.oxycoded.infinicell.repository.FriendRequestRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class FriendRequestService {

  private final FriendRequestRepository friendRequestRepository;

  @Transactional
  public FriendRequest createOutgoingFriendRequest(final Account requestingAccount, final Account recipientAccount) {
    final FriendRequest friendRequest = new FriendRequest();
    friendRequest.setRequestingAccount(requestingAccount);
    friendRequest.setRecipientAccount(recipientAccount);
    friendRequest.setFriendRequestStatus(FriendRequestStatus.OPEN);
    return save(friendRequest);
  }

  @Transactional
  public FriendRequest cancelOutgoingFriendRequest(final String authenticatedAccountId, final String friendRequestId) {
    final FriendRequest friendRequest = findById(friendRequestId);

    final FriendRequestStatus status = friendRequest.getFriendRequestStatus();
    if(!status.isCancelable()) {
      // TODO should this be more informational? (Friend request is not in a cancellable state)
      throw new FriendRequestNotFoundException(friendRequestId);
    }

    final Account requestingAccount = friendRequest.getRequestingAccount();
    if(!requestingAccount.getId().equalsIgnoreCase(authenticatedAccountId)) {
      // TODO should this be more informational? (Logged in account is not the requester)
      throw new FriendRequestNotFoundException(friendRequestId);
    }

    friendRequest.setFriendRequestStatus(FriendRequestStatus.CANCELED);
    return save(friendRequest);
  }

  @Transactional
  public FriendRequest acceptFriendRequest(final String authenticatedAccountId, final String friendRequestId) {
    final FriendRequest friendRequest = findById(friendRequestId);
    final Account recipientAccount = friendRequest.getRecipientAccount();
    if(!recipientAccount.getId().equalsIgnoreCase(authenticatedAccountId)) {
      // TODO should this be more informational? (Logged in account is not the recipient)
      throw new FriendRequestNotFoundException(friendRequestId);
    }
    friendRequest.setFriendRequestStatus(FriendRequestStatus.ACCEPTED);
    return save(friendRequest);
  }

  @Transactional
  public FriendRequest denyFriendRequest(final String authenticatedAccountId, final String friendRequestId) {
    final FriendRequest friendRequest = findById(friendRequestId);
    final Account requestingAccount = friendRequest.getRequestingAccount();
    if(!requestingAccount.getId().equalsIgnoreCase(authenticatedAccountId)) {
      // TODO should this be more informational?
      throw new FriendRequestNotFoundException(friendRequestId);
    }
    friendRequest.setFriendRequestStatus(FriendRequestStatus.DENIED);
    return save(friendRequest);
  }

  @Transactional
  public FriendRequest ignoreFriendRequest(final String authenticatedAccountId, final String friendRequestId) {
    final FriendRequest friendRequest = findById(friendRequestId);
    final Account requestingAccount = friendRequest.getRequestingAccount();
    if(!requestingAccount.getId().equalsIgnoreCase(authenticatedAccountId)) {
      // TODO should this be more informational?
      throw new FriendRequestNotFoundException(friendRequestId);
    }
    friendRequest.setFriendRequestStatus(FriendRequestStatus.IGNORED);
    return save(friendRequest);
  }

  @Transactional
  public FriendRequest findById(final String friendRequestId) {
    final Optional<FriendRequest> friendRequestOptional = getFriendRequestRepository().findById(friendRequestId);
    if(friendRequestOptional.isEmpty()) {
      throw new FriendRequestNotFoundException(friendRequestId);
    }
    return friendRequestOptional.get();
  }

  @Transactional
  public FriendRequest save(final FriendRequest friendRequest) {
    return getFriendRequestRepository().save(friendRequest);
  }
}
