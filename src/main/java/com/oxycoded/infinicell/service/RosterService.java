package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.Roster;
import com.oxycoded.infinicell.domain.model.RosterEnabledEntity;
import com.oxycoded.infinicell.domain.model.Roster;
import com.oxycoded.infinicell.domain.model.RosterEnabledEntity;
import com.oxycoded.infinicell.exception.RosterNotFoundException;
import com.oxycoded.infinicell.repository.RosterRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class RosterService {

  private final RosterRepository rosterRepository;

  public Roster createRoster(final RosterEnabledEntity rosterEnabledEntity) {
    final Roster roster = new Roster();
    roster.setRosterEnabledEntity(rosterEnabledEntity);
    return save(roster);
  }

  public Roster findByRosterEnabledEntityId(final String rosterEnabledEntityId) {
    final Optional<Roster> rosterOptional = getRosterRepository().findByRosterEnabledEntityId(rosterEnabledEntityId);
    if (rosterOptional.isEmpty()) {
      throw new RosterNotFoundException();
    }
    return rosterOptional.get();
  }

  public Roster findByCreatureInstanceId(final String creatureInstanceId) {
    final Optional<Roster> rosterOptional = getRosterRepository().findByCreatureInstancesId(creatureInstanceId);
    if (rosterOptional.isEmpty()) {
      throw new RosterNotFoundException();
    }
    return rosterOptional.get();
  }

  public Roster save(final Roster roster) {
    return getRosterRepository().save(roster);
  }
}
