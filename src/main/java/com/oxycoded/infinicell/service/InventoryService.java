package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.Inventory;
import com.oxycoded.infinicell.domain.model.InventoryEnabledEntity;
import com.oxycoded.infinicell.exception.InventoryNotFoundException;
import com.oxycoded.infinicell.repository.InventoryRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class InventoryService {

  private final InventoryRepository inventoryRepository;

  public Inventory createInventory(final InventoryEnabledEntity inventoryEnabledEntity) {
    final Inventory inventory = new Inventory();
    inventory.setInventoryEnabledEntity(inventoryEnabledEntity);
    return save(inventory);
  }

  public Inventory findByInventoryEnabledEntityId(final String inventoryEnabledEntityId) {
    final Optional<Inventory> inventoryOptional = getInventoryRepository().findByInventoryEnabledEntityId(inventoryEnabledEntityId);

    if (inventoryOptional.isEmpty()) {
      throw new InventoryNotFoundException();
    }

    return inventoryOptional.get();
  }

  public Inventory save(final Inventory inventory) {
    return getInventoryRepository().save(inventory);
  }
}
