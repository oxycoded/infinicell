package com.oxycoded.infinicell.service.intermediate;

import com.oxycoded.infinicell.domain.model.*;
import com.oxycoded.infinicell.exception.*;
import com.oxycoded.infinicell.service.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
@Slf4j
public class PcCreatureInstanceManagementService {

  private final AccountService accountService;
  private final PcCreatureInstanceService pcCreatureInstanceService;
  private final NpcCreatureInstanceService npcCreatureInstanceService;
  private final CreatureInstanceService creatureInstanceService;
  private final CreatureService creatureService;
  private final PcCreatureInstanceCellService pcCreatureInstanceCellService;
  private final CellService cellService;
  private final EventService eventService;
  private final EventReferenceService eventReferenceService;
  private final DropTableService dropTableService;
  private final SpawnTableService spawnTableService;
  private final ItemService itemService;
  private final ItemInstanceService itemInstanceService;
  private final ConsistentItemDropService consistentItemDropService;
  private final RandomizedItemDropService randomizedItemDropService;
  private final InventoryService inventoryService;
  private final RosterService rosterService;
  private final ConsistentCreatureSpawnService consistentCreatureSpawnService;
  private final RandomizedCreatureSpawnService randomizedCreatureSpawnService;
  private final CombatService combatService;
  private final CombatantService combatantService;

  @Transactional
  public PcCreatureInstance createPcCreatureInstance(final String currentlyAuthenticatedAccountId, final String name, final String description) {
    log.info("Creating pc creature instance: {}", name);
    final Account account = getAccountService().findById(currentlyAuthenticatedAccountId);
    final Creature travelerCreature = getCreatureService().getTravelerCreature();
    final PcCreatureInstance createdPcCreatureInstance = getPcCreatureInstanceService().createPcCreatureInstance(account, travelerCreature, name, description);
    final Inventory inventory = getInventoryService().createInventory(createdPcCreatureInstance);
    createdPcCreatureInstance.setInventory(inventory);

    try {
      final Cell existingStartingCell = getCellService().findByXandYandZ(0, 0, 0);
      return initializePcCreatureInstanceInCell(createdPcCreatureInstance, existingStartingCell);
    } catch (final CellNotFoundException cnfe) {
      log.info("Cell that pc creature instance is being created in does not exist; Creating a new one");
    }

    final Cell newlyCreatedStartingCell = getCellService().createCell(0, 0, 0, 0);
    newlyCreatedStartingCell.setDropTable(getDropTableService().createDropTable(newlyCreatedStartingCell));
    newlyCreatedStartingCell.setInventory(getInventoryService().createInventory(newlyCreatedStartingCell));
    newlyCreatedStartingCell.setRoster(getRosterService().createRoster(newlyCreatedStartingCell));
    newlyCreatedStartingCell.setSpawnTable(getSpawnTableService().createSpawnTable(newlyCreatedStartingCell));
    final Cell updatedCell = getCellService().save(newlyCreatedStartingCell);

    return initializePcCreatureInstanceInCell(createdPcCreatureInstance, updatedCell);
  }

  @Transactional
  public PcCreatureInstance loadPcCreatureInstance(final String currentlyAuthenticatedAccountId, final String pcCreatureInstanceId) {
    final PcCreatureInstance pcCreatureInstance = getPcCreatureInstanceService().findById(pcCreatureInstanceId);

    final Account pcCreatureInstanceAccount = pcCreatureInstance.getAccount();
    if (!pcCreatureInstanceAccount.getId().equalsIgnoreCase(currentlyAuthenticatedAccountId)) {
      // TODO should this be more informational?
      throw new PcCreatureInstanceNotFoundException(pcCreatureInstanceId);
    }

    pcCreatureInstanceAccount.selectPcCreatureInstance(pcCreatureInstance);
    getAccountService().save(pcCreatureInstanceAccount);

    pcCreatureInstance.setSelectedByAccount(pcCreatureInstanceAccount);
    return getPcCreatureInstanceService().save(pcCreatureInstance);
  }

  @Transactional
  public PcCreatureInstance step(final String currentlyAuthenticatedAccountId, final Direction direction) {
    final Account account = getAccountService().findById(currentlyAuthenticatedAccountId);
    final PcCreatureInstance pcCreatureInstance = account.getSelectedPcCreatureInstance();
    final Cell originationCell = pcCreatureInstance.getCurrentCell();
    final long destinationX = originationCell.getX() + direction.getXChange();
    final long destinationY = originationCell.getY() + direction.getYChange();
    final long destinationZ = originationCell.getZ() + direction.getZChange();

    releaseAllItemInstances(pcCreatureInstance);
    return movePcCreatureInstanceToCoordinates(pcCreatureInstance, destinationX, destinationY, destinationZ);
  }

  @Transactional
  public PcCreatureInstance searchCurrentCell(final String currentlyAuthenticatedAccountId) {
    final Account account = getAccountService().findById(currentlyAuthenticatedAccountId);
    final PcCreatureInstance pcCreatureInstance = account.getSelectedPcCreatureInstance();
    final PcCreatureInstanceCell pcCreatureInstanceCell = pcCreatureInstance.getCurrentPcCreatureInstanceCell();

    if (pcCreatureInstanceCell.isSearched()) {
      throw new AlreadySearchedCellException();
    }

    final DropTable creatureInstanceCellDropTable = pcCreatureInstanceCell.getDropTable();
    final Cell cell = pcCreatureInstanceCell.getCell();
    final DropTable cellDropTable = cell.getDropTable();

    pcCreatureInstanceCell.setSearched(true);

    final List<ItemInstance> itemInstances = new ArrayList<>();

    for (RandomizedItemDrop randomizedItemDrop : creatureInstanceCellDropTable.getRandomizedItemDrops()) {
      final ItemInstance itemInstance = generateItemInstanceFrom(randomizedItemDrop);
      if (ItemInstance.NOT_DROPPED.equals(itemInstance)) {
        continue;
      }
      itemInstances.add(itemInstance);
    }

    for (RandomizedItemDrop randomizedItemDrop : cellDropTable.getRandomizedItemDrops()) {
      final ItemInstance itemInstance = generateItemInstanceFrom(randomizedItemDrop);
      if (ItemInstance.NOT_DROPPED.equals(itemInstance)) {
        continue;
      }
      itemInstances.add(itemInstance);
    }

    for (ConsistentItemDrop consistentItemDrop : creatureInstanceCellDropTable.getConsistentItemDrops()) {
      final ItemInstance itemInstance = generateItemInstanceFrom(consistentItemDrop);
      if (ItemInstance.NOT_DROPPED.equals(itemInstance)) {
        continue;
      }
      itemInstances.add(itemInstance);
    }

    for (ConsistentItemDrop consistentItemDrop : cellDropTable.getConsistentItemDrops()) {
      final ItemInstance itemInstance = generateItemInstanceFrom(consistentItemDrop);
      if (ItemInstance.NOT_DROPPED.equals(itemInstance)) {
        continue;
      }
      itemInstances.add(itemInstance);
    }

    if(CollectionUtils.isEmpty(itemInstances)) {
      log.info("No items were generated for this search.");
      return pcCreatureInstance;
    }

    final Inventory creatureInstanceCellInventory = pcCreatureInstanceCell.getInventory();
    addAllItemInstancesToInventory(itemInstances, creatureInstanceCellInventory);

    return pcCreatureInstance;
  }

  @Transactional
  public PcCreatureInstance takeItemInstances(final String currentlyAuthenticatedAccountId, final List<String> itemInstanceIds) {
    final Account account = getAccountService().findById(currentlyAuthenticatedAccountId);
    final PcCreatureInstance pcCreatureInstance = account.getSelectedPcCreatureInstance();
    final PcCreatureInstanceCell pcCreatureInstanceCell = pcCreatureInstance.getCurrentPcCreatureInstanceCell();
    final Cell cell = pcCreatureInstanceCell.getCell();

    final List<ItemInstance> itemInstances = getItemInstanceService().findAllByIdIn(itemInstanceIds);

    final List<ItemInstance> itemInstancesRemovedFromPcCreatureInstanceCell = removeAllItemInstancesFromInventory(itemInstances,
        pcCreatureInstanceCell.getInventory());
    final List<ItemInstance> itemInstancesRemovedFromCell = removeAllItemInstancesFromInventory(itemInstances, cell.getInventory());

    final List<ItemInstance> itemInstancesToAddToInventory = Stream.concat(itemInstancesRemovedFromPcCreatureInstanceCell.parallelStream(),
        itemInstancesRemovedFromCell.parallelStream()).collect(Collectors.toList());

    addAllItemInstancesToInventory(itemInstancesToAddToInventory, pcCreatureInstance.getInventory());

    return getPcCreatureInstanceService().save(pcCreatureInstance);
  }

  @Transactional
  public PcCreatureInstance shareItemInstances(final String currentlyAuthenticatedAccountId, final List<String> itemInstanceIds) {
    final Account account = getAccountService().findById(currentlyAuthenticatedAccountId);
    final PcCreatureInstance pcCreatureInstance = account.getSelectedPcCreatureInstance();
    final PcCreatureInstanceCell pcCreatureInstanceCell = pcCreatureInstance.getCurrentPcCreatureInstanceCell();
    final Cell cell = pcCreatureInstanceCell.getCell();

    final List<ItemInstance> itemInstances = getItemInstanceService().findAllByIdIn(itemInstanceIds);

    final List<ItemInstance> itemInstancesRemovedFromCreatureInstanceCell = removeAllItemInstancesFromInventory(
        itemInstances, pcCreatureInstanceCell.getInventory());

    addAllItemInstancesToInventory(itemInstancesRemovedFromCreatureInstanceCell, cell.getInventory());

    return getPcCreatureInstanceService().save(pcCreatureInstance);
  }

  @Transactional
  public PcCreatureInstance scoutCurrentCell(final String currentlyAuthenticatedAccountId) {
    final Account account = getAccountService().findById(currentlyAuthenticatedAccountId);
    final PcCreatureInstance pcCreatureInstance = account.getSelectedPcCreatureInstance();
    final PcCreatureInstanceCell pcCreatureInstanceCell = pcCreatureInstance.getCurrentPcCreatureInstanceCell();

    if (pcCreatureInstanceCell.isScouted()) {
      throw new AlreadyScoutedCellException();
    }

    final SpawnTable creatureInstanceCellSpawnTable = pcCreatureInstanceCell.getSpawnTable();
    final Cell cell = pcCreatureInstanceCell.getCell();
    final SpawnTable cellSpawnTable = cell.getSpawnTable();

    pcCreatureInstanceCell.setScouted(true);

    final List<CreatureInstance> creatureInstances = new ArrayList<>();

    for (RandomizedCreatureSpawn randomizedCreatureSpawn : creatureInstanceCellSpawnTable.getRandomizedCreatureSpawns()) {
      final List<CreatureInstance> randomGeneratedCreatureInstances = generateCreatureInstancesFrom(randomizedCreatureSpawn);
      if (CollectionUtils.isEmpty(randomGeneratedCreatureInstances)) {
        continue;
      }
      creatureInstances.addAll(randomGeneratedCreatureInstances);
    }

    for (RandomizedCreatureSpawn randomizedCreatureSpawn : cellSpawnTable.getRandomizedCreatureSpawns()) {
      final List<CreatureInstance> randomGeneratedCreatureInstances = generateCreatureInstancesFrom(randomizedCreatureSpawn);
      if (CollectionUtils.isEmpty(randomGeneratedCreatureInstances)) {
        continue;
      }
      creatureInstances.addAll(randomGeneratedCreatureInstances);
    }

    for (ConsistentCreatureSpawn consistentCreatureSpawn : creatureInstanceCellSpawnTable.getConsistentCreatureSpawns()) {
      final List<CreatureInstance> consistentGeneratedCreatureInstances = generateCreatureInstancesFrom(consistentCreatureSpawn);
      if (CollectionUtils.isEmpty(consistentGeneratedCreatureInstances)) {
        continue;
      }
      creatureInstances.addAll(consistentGeneratedCreatureInstances);
    }

    for (ConsistentCreatureSpawn consistentCreatureSpawn : cellSpawnTable.getConsistentCreatureSpawns()) {
      final List<CreatureInstance> consistentGeneratedCreatureInstances = generateCreatureInstancesFrom(consistentCreatureSpawn);
      if (CollectionUtils.isEmpty(consistentGeneratedCreatureInstances)) {
        continue;
      }
      creatureInstances.addAll(consistentGeneratedCreatureInstances);
    }

    if(CollectionUtils.isEmpty(creatureInstances)) {
      log.info("No creatures were generated for this scout.");
      return pcCreatureInstance;
    }

    final Roster pcCreatureInstanceCellRoster = pcCreatureInstanceCell.getRoster();
    pcCreatureInstanceCellRoster.getCreatureInstances().addAll(creatureInstances);
    creatureInstances.forEach(creatureInstance -> creatureInstance.setRoster(pcCreatureInstanceCellRoster));

    getRosterService().save(pcCreatureInstanceCellRoster);
    getCreatureInstanceService().saveAll(creatureInstances);

    return pcCreatureInstance;
  }

  @Transactional
  public PcCreatureInstance startCombat(final String currentlyAuthenticatedAccountId, final List<String> creatureInstanceIds) {
    final Account account = getAccountService().findById(currentlyAuthenticatedAccountId);
    final PcCreatureInstance pcCreatureInstance = account.getSelectedPcCreatureInstance();
    final PcCreatureInstanceCell pcCreatureInstanceCell = pcCreatureInstance.getCurrentPcCreatureInstanceCell();

    final Combat combat = getCombatService().createCombat(pcCreatureInstanceCell);

    final List<Combatant> combatants = new ArrayList<>();

    combatants.add(generatePlayerCombatant(pcCreatureInstance, combat));

    //TODO
    // combatants.addAll(generateAllyCombatants());

    final List<CreatureInstance> creatureInstances = getCreatureInstanceService().findAllById(creatureInstanceIds);
    combatants.addAll(generateEnemyCombatants(creatureInstances, combat));

    combat.getCombatants().addAll(combatants);
    getCombatService().save(combat);

    return getPcCreatureInstanceService().save(pcCreatureInstance);
  }

  private PcCreatureInstance initializePcCreatureInstanceInCell(final PcCreatureInstance createdPcCreatureInstance, final Cell cell) {
    log.info("Creating creature instance cell for pc creature instance: {} and cell: ({},{},{})", createdPcCreatureInstance.getName(), cell.getX(),
        cell.getY(), cell.getZ());
    final PcCreatureInstanceCell pcCreatureInstanceCell = getPcCreatureInstanceCellService().createPcCreatureInstanceCell(cell, createdPcCreatureInstance);

    pcCreatureInstanceCell.setInventory(getInventoryService().createInventory(pcCreatureInstanceCell));
    pcCreatureInstanceCell.setDropTable(getDropTableService().createDropTable(pcCreatureInstanceCell));
    pcCreatureInstanceCell.setRoster(getRosterService().createRoster(pcCreatureInstanceCell));
    pcCreatureInstanceCell.setSpawnTable(getSpawnTableService().createSpawnTable(pcCreatureInstanceCell));
    pcCreatureInstanceCell.setDiscovered(cell.getPcCreatureInstanceCells().isEmpty());
    pcCreatureInstanceCell.setCurrent(true);

    cell.getPcCreatureInstanceCells().add(pcCreatureInstanceCell);
    final Cell updatedCell = getCellService().save(cell);

    createdPcCreatureInstance.getPcCreatureInstanceCells().add(pcCreatureInstanceCell);
    final PcCreatureInstance updatedPcCreatureInstance = getPcCreatureInstanceService().save(createdPcCreatureInstance);

    return persistAndSendPcCreatureInstanceCreationEvent(updatedPcCreatureInstance, updatedCell);
  }

  private PcCreatureInstance persistAndSendPcCreatureInstanceCreationEvent(final PcCreatureInstance createdPcCreatureInstance, final Cell startingCell) {
    final Event creationEvent = new Event();
    creationEvent.setEventType(EventType.INFINITY_WALKER_CREATION);
    final Event savedCreationEvent = getEventService().save(creationEvent);

    final EventReference createdInCellEventReference = getEventReferenceService()
        .createEventReference(savedCreationEvent, startingCell, EventReferenceType.CREATED_IN_CELL);
    final EventReference createdPcCreatureInstanceEventReference = getEventReferenceService()
        .createEventReference(savedCreationEvent, createdPcCreatureInstance, EventReferenceType.CREATED_INFINITY_WALKER);

    savedCreationEvent.getEventReferences().add(createdInCellEventReference);
    savedCreationEvent.getEventReferences().add(createdPcCreatureInstanceEventReference);

    startingCell.getEventReferences().add(createdInCellEventReference);
    createdPcCreatureInstance.getEventReferences().add(createdPcCreatureInstanceEventReference);

    getEventService().sendCellEvent(startingCell.getId(), savedCreationEvent);
    getEventService().sendPcCreatureInstanceEvent(createdPcCreatureInstance.getId(), savedCreationEvent);

    getEventService().save(savedCreationEvent);
    getCellService().save(startingCell);
    return getPcCreatureInstanceService().save(createdPcCreatureInstance);
  }

  private void releaseAllItemInstances(final PcCreatureInstance pcCreatureInstance) {
    final PcCreatureInstanceCell pcCreatureInstanceCell = pcCreatureInstance.getCurrentPcCreatureInstanceCell();

    if (CollectionUtils.isEmpty(pcCreatureInstanceCell.getInventory().getItemInstances())) {
      return;
    }

    final Cell cell = pcCreatureInstanceCell.getCell();

    final Inventory creatureInstanceCellInventory = pcCreatureInstanceCell.getInventory();
    final Inventory cellInventory = cell.getInventory();

    transferAllItemInstances(creatureInstanceCellInventory, cellInventory);

    final List<ItemInstance> remainingItemInstances = creatureInstanceCellInventory.getItemInstances();
    getItemInstanceService().deleteAll(remainingItemInstances);
    remainingItemInstances.clear();
    getInventoryService().save(creatureInstanceCellInventory);
  }

  private void transferAllItemInstances(final Inventory fromInventory, final Inventory toInventory) {
    final List<ItemInstance> removedItemInstances = removeAllItemInstancesFromInventory(fromInventory.getItemInstances(), fromInventory);
    addAllItemInstancesToInventory(removedItemInstances, toInventory);
  }

  private PcCreatureInstance movePcCreatureInstanceToCoordinates(final PcCreatureInstance pcCreatureInstance, final long x, final long y, final long z) {
    try {
      final Cell existingDestinationCell = getCellService().findByXandYandZ(x, y, z);
      return movePcCreatureInstanceToCell(pcCreatureInstance, existingDestinationCell);
    } catch (final CellNotFoundException cnfe) {
      log.info("Cell that pc creature instance is moving into does not exist (Nothing has ever been in this cell); Creating a new cell");
    }

    final long explorationExperienceValue = calculateExplorationExperienceValue(x, y, z);
    final Cell newlyCreatedDestinationCell = getCellService().createCell(x, y, z, explorationExperienceValue);

    final Inventory cellInventory = generateCellInventory(newlyCreatedDestinationCell);
    newlyCreatedDestinationCell.setInventory(cellInventory);

    final DropTable cellDropTable = generateCellDropTable(newlyCreatedDestinationCell);
    newlyCreatedDestinationCell.setDropTable(cellDropTable);

    final Roster roster = generateCellRoster(newlyCreatedDestinationCell);
    newlyCreatedDestinationCell.setRoster(roster);

    final SpawnTable cellSpawnTable = generateCellSpawnTable(newlyCreatedDestinationCell);
    newlyCreatedDestinationCell.setSpawnTable(cellSpawnTable);

    final Cell updatedCell = getCellService().save(newlyCreatedDestinationCell);

    return movePcCreatureInstanceToCell(pcCreatureInstance, updatedCell);
  }

  private PcCreatureInstance movePcCreatureInstanceToCell(final PcCreatureInstance pcCreatureInstance, final Cell destinationCell) {
    try {
      final PcCreatureInstanceCell existingDestinationPcCreatureInstanceCell = getPcCreatureInstanceCellService()
          .findByCellIdAndPcCreatureInstanceId(destinationCell.getId(), pcCreatureInstance.getId());
      return movePcCreatureInstanceToCreatureInstanceCell(pcCreatureInstance, existingDestinationPcCreatureInstanceCell);
    } catch (final CreatureInstanceCellNotFoundException cicnfe) {
      log.info(
          "Creature instance cell that pc creature instance is moving into does not exist (Infinity Walker has never been in this cell); Creating a new creature instance cell");
    }

    final PcCreatureInstanceCell newlyCreatedDestinationPcCreatureInstanceCell = getPcCreatureInstanceCellService()
        .createPcCreatureInstanceCell(destinationCell, pcCreatureInstance);

    newlyCreatedDestinationPcCreatureInstanceCell.setDiscovered(destinationCell.getPcCreatureInstanceCells().isEmpty());

    final Inventory inventory = generateCreatureInstanceCellInventory(newlyCreatedDestinationPcCreatureInstanceCell);
    newlyCreatedDestinationPcCreatureInstanceCell.setInventory(inventory);

    final DropTable dropTable = generateCreatureInstanceCellDropTable(newlyCreatedDestinationPcCreatureInstanceCell);
    newlyCreatedDestinationPcCreatureInstanceCell.setDropTable(dropTable);

    final Roster roster = generateCreatureInstanceCellRoster(newlyCreatedDestinationPcCreatureInstanceCell);
    newlyCreatedDestinationPcCreatureInstanceCell.setRoster(roster);

    final SpawnTable spawnTable = generateCreatureInstanceCellSpawnTable(newlyCreatedDestinationPcCreatureInstanceCell);
    newlyCreatedDestinationPcCreatureInstanceCell.setSpawnTable(spawnTable);

    final PcCreatureInstanceCell updatedPcCreatureInstanceCell = getPcCreatureInstanceCellService().save(newlyCreatedDestinationPcCreatureInstanceCell);

    destinationCell.getPcCreatureInstanceCells().add(updatedPcCreatureInstanceCell);
    getCellService().save(destinationCell);

    pcCreatureInstance.getPcCreatureInstanceCells().add(updatedPcCreatureInstanceCell);
    final PcCreatureInstance updatedPcCreatureInstance = getPcCreatureInstanceService().save(pcCreatureInstance);

    return movePcCreatureInstanceToCreatureInstanceCell(updatedPcCreatureInstance, updatedPcCreatureInstanceCell);
  }

  private PcCreatureInstance movePcCreatureInstanceToCreatureInstanceCell(final PcCreatureInstance pcCreatureInstance,
      final PcCreatureInstanceCell destinationPcCreatureInstanceCell) {
    final PcCreatureInstanceCell originationPcCreatureInstanceCell = pcCreatureInstance.getCurrentPcCreatureInstanceCell();
    originationPcCreatureInstanceCell.setCurrent(false);
    final PcCreatureInstanceCell updatedOriginationPcCreatureInstanceCell = getPcCreatureInstanceCellService().save(originationPcCreatureInstanceCell);

    destinationPcCreatureInstanceCell.setCurrent(true);
    destinationPcCreatureInstanceCell.setCleared(true); //TODO this will depend on cell type eventually
    destinationPcCreatureInstanceCell.setTimesVisited(destinationPcCreatureInstanceCell.getTimesVisited() + 1);
    final PcCreatureInstanceCell updatedDestinationPcCreatureInstanceCell = getPcCreatureInstanceCellService().save(destinationPcCreatureInstanceCell);

    final PcCreatureInstance updatedPcCreatureInstance = persistAndDistributeStepToNewCoordinatesEvents(pcCreatureInstance,
        updatedOriginationPcCreatureInstanceCell, updatedDestinationPcCreatureInstanceCell);

    if (updatedDestinationPcCreatureInstanceCell.getTimesVisited() == 1L) {
      updatedPcCreatureInstance
          .setExperience(updatedPcCreatureInstance.getExperience() + updatedDestinationPcCreatureInstanceCell.getExplorationExperienceValue());
      return persistAndDistributeFirstTimeInCellEvents(updatedPcCreatureInstance, updatedDestinationPcCreatureInstanceCell);
    }

    return updatedPcCreatureInstance;
  }

  private PcCreatureInstance persistAndDistributeStepToNewCoordinatesEvents(final PcCreatureInstance steppingPcCreatureInstance,
      final PcCreatureInstanceCell originationPcCreatureInstanceCell, final PcCreatureInstanceCell destinationPcCreatureInstanceCell) {
    final PcCreatureInstance updatedPcCreatureInstance = persistAndDistributeExitCellEvent(steppingPcCreatureInstance, originationPcCreatureInstanceCell);
    return persistAndDistributeEnterCellEvent(updatedPcCreatureInstance, destinationPcCreatureInstanceCell);
  }

  private PcCreatureInstance persistAndDistributeExitCellEvent(final PcCreatureInstance exitingPcCreatureInstance,
      final PcCreatureInstanceCell originationPcCreatureInstanceCell) {
    final Cell originationCell = originationPcCreatureInstanceCell.getCell();

    final Event exitCellEvent = new Event();
    exitCellEvent.setEventType(EventType.EXIT_CELL);
    final Event savedExitCellEvent = getEventService().save(exitCellEvent);

    final EventReference originationCreatureInstanceCellEventReference = getEventReferenceService()
        .createEventReference(savedExitCellEvent, originationPcCreatureInstanceCell, EventReferenceType.EXITED_CREATURE_INSTANCE_CELL);
    final EventReference originationCellEventReference = getEventReferenceService()
        .createEventReference(savedExitCellEvent, originationCell, EventReferenceType.EXITED_CELL);
    final EventReference exitingPcCreatureInstanceEventReference = getEventReferenceService()
        .createEventReference(savedExitCellEvent, exitingPcCreatureInstance, EventReferenceType.EXITING_INFINITY_WALKER);

    savedExitCellEvent.getEventReferences().add(originationCreatureInstanceCellEventReference);
    savedExitCellEvent.getEventReferences().add(originationCellEventReference);
    savedExitCellEvent.getEventReferences().add(exitingPcCreatureInstanceEventReference);

    originationPcCreatureInstanceCell.getEventReferences().add(originationCreatureInstanceCellEventReference);
    originationCell.getEventReferences().add(originationCellEventReference);
    exitingPcCreatureInstance.getEventReferences().add(exitingPcCreatureInstanceEventReference);

    getEventService().sendCellEvent(originationCell.getId(), savedExitCellEvent);
    getEventService().sendPcCreatureInstanceEvent(exitingPcCreatureInstance.getId(), savedExitCellEvent);

    getEventService().save(savedExitCellEvent);
    getCellService().save(originationCell);
    getPcCreatureInstanceCellService().save(originationPcCreatureInstanceCell);
    return getPcCreatureInstanceService().save(exitingPcCreatureInstance);
  }

  private PcCreatureInstance persistAndDistributeEnterCellEvent(final PcCreatureInstance enteringPcCreatureInstance,
      final PcCreatureInstanceCell destinationPcCreatureInstanceCell) {
    final Cell destinationCell = destinationPcCreatureInstanceCell.getCell();

    final Event enterCellEvent = new Event();
    enterCellEvent.setEventType(EventType.ENTER_CELL);
    final Event savedEnterCellEvent = getEventService().save(enterCellEvent);

    final EventReference destinationCreatureInstanceCellEventReference = getEventReferenceService()
        .createEventReference(savedEnterCellEvent, destinationPcCreatureInstanceCell, EventReferenceType.ENTERED_CREATURE_INSTANCE_CELL);
    final EventReference destinationCellEventReference = getEventReferenceService()
        .createEventReference(savedEnterCellEvent, destinationCell, EventReferenceType.ENTERED_CELL);
    final EventReference enteringPcCreatureInstanceEventReference = getEventReferenceService()
        .createEventReference(savedEnterCellEvent, enteringPcCreatureInstance, EventReferenceType.ENTERING_INFINITY_WALKER);

    savedEnterCellEvent.getEventReferences().add(destinationCreatureInstanceCellEventReference);
    savedEnterCellEvent.getEventReferences().add(destinationCellEventReference);
    savedEnterCellEvent.getEventReferences().add(enteringPcCreatureInstanceEventReference);

    destinationPcCreatureInstanceCell.getEventReferences().add(destinationCreatureInstanceCellEventReference);
    destinationCell.getEventReferences().add(destinationCellEventReference);
    enteringPcCreatureInstance.getEventReferences().add(enteringPcCreatureInstanceEventReference);

    getEventService().sendCellEvent(destinationPcCreatureInstanceCell.getId(), savedEnterCellEvent);
    getEventService().sendPcCreatureInstanceEvent(enteringPcCreatureInstance.getId(), savedEnterCellEvent);

    getEventService().save(savedEnterCellEvent);
    getCellService().save(destinationCell);
    getPcCreatureInstanceCellService().save(destinationPcCreatureInstanceCell);
    return getPcCreatureInstanceService().save(enteringPcCreatureInstance);
  }

  private PcCreatureInstance persistAndDistributeFirstTimeInCellEvents(final PcCreatureInstance pcCreatureInstance,
      final PcCreatureInstanceCell pcCreatureInstanceCell) {
    final PcCreatureInstance updatedPcCreatureInstance = persistAndDistributeExperienceGainedEvent(pcCreatureInstance,
        pcCreatureInstanceCell.getExplorationExperienceValue());
    if (pcCreatureInstanceCell.isDiscovered()) {
      return persistAndDistributeDiscoverEvent(updatedPcCreatureInstance, pcCreatureInstanceCell);
    }
    return updatedPcCreatureInstance;
  }

  private PcCreatureInstance persistAndDistributeExperienceGainedEvent(final PcCreatureInstance experienceGainingPcCreatureInstance,
      final long experienceGained) {
    final ExperienceGainedEvent experienceGainedEvent = new ExperienceGainedEvent();
    experienceGainedEvent.setExperienceGained(experienceGained);
    final Event savedExperienceGainedEvent = getEventService().save(experienceGainedEvent);

    final PcCreatureInstanceCell experienceGainedInPcCreatureInstanceCell = experienceGainingPcCreatureInstance.getCurrentPcCreatureInstanceCell();
    final Cell experienceGainedInCell = experienceGainedInPcCreatureInstanceCell.getCell();

    final EventReference experienceGainedInCreatureInstanceCellEventReference = getEventReferenceService()
        .createEventReference(savedExperienceGainedEvent, experienceGainedInPcCreatureInstanceCell,
            EventReferenceType.EXPERIENCE_GAINED_IN_CREATURE_INSTANCE_CELL);
    final EventReference experienceGainedInCellEventReference = getEventReferenceService()
        .createEventReference(savedExperienceGainedEvent, experienceGainedInCell, EventReferenceType.EXPERIENCE_GAINED_IN_CELL);
    final EventReference experienceGainingPcCreatureInstanceEventReference = getEventReferenceService()
        .createEventReference(savedExperienceGainedEvent, experienceGainingPcCreatureInstance, EventReferenceType.EXPERIENCE_GAINING_INFINITY_WALKER);

    savedExperienceGainedEvent.getEventReferences().add(experienceGainedInCreatureInstanceCellEventReference);
    savedExperienceGainedEvent.getEventReferences().add(experienceGainedInCellEventReference);
    savedExperienceGainedEvent.getEventReferences().add(experienceGainingPcCreatureInstanceEventReference);

    experienceGainedInPcCreatureInstanceCell.getEventReferences().add(experienceGainedInCreatureInstanceCellEventReference);
    experienceGainedInCell.getEventReferences().add(experienceGainedInCellEventReference);
    experienceGainingPcCreatureInstance.getEventReferences().add(experienceGainingPcCreatureInstanceEventReference);

    getEventService().sendCellEvent(experienceGainedInCell.getId(), savedExperienceGainedEvent);
    getEventService().sendPcCreatureInstanceEvent(experienceGainingPcCreatureInstance.getId(), savedExperienceGainedEvent);

    getPcCreatureInstanceCellService().save(experienceGainedInPcCreatureInstanceCell);
    getCellService().save(experienceGainedInCell);
    getEventService().save(savedExperienceGainedEvent);
    return getPcCreatureInstanceService().save(experienceGainingPcCreatureInstance);
  }

  private PcCreatureInstance persistAndDistributeDiscoverEvent(final PcCreatureInstance discoveringPcCreatureInstance,
      final PcCreatureInstanceCell discoveredPcCreatureInstanceCell) {
    final Cell discoveredCell = discoveredPcCreatureInstanceCell.getCell();

    final Event discoverEvent = new Event();
    discoverEvent.setEventType(EventType.CELL_DISCOVERED);
    final Event savedDiscoverEvent = getEventService().save(discoverEvent);

    final EventReference discoveredCreatureInstanceCellEventReference = getEventReferenceService()
        .createEventReference(savedDiscoverEvent, discoveredPcCreatureInstanceCell, EventReferenceType.DISCOVERED_CREATURE_INSTANCE_CELL);
    final EventReference discoveredCellEventReference = getEventReferenceService()
        .createEventReference(savedDiscoverEvent, discoveredCell, EventReferenceType.DISCOVERED_CELL);
    final EventReference discoveringPcCreatureInstanceEventReference = getEventReferenceService()
        .createEventReference(savedDiscoverEvent, discoveringPcCreatureInstance, EventReferenceType.DISCOVERING_INFINITY_WALKER);

    savedDiscoverEvent.getEventReferences().add(discoveredCreatureInstanceCellEventReference);
    savedDiscoverEvent.getEventReferences().add(discoveredCellEventReference);
    savedDiscoverEvent.getEventReferences().add(discoveringPcCreatureInstanceEventReference);

    discoveredPcCreatureInstanceCell.getEventReferences().add(discoveredCreatureInstanceCellEventReference);
    discoveredCell.getEventReferences().add(discoveredCellEventReference);
    discoveringPcCreatureInstance.getEventReferences().add(discoveringPcCreatureInstanceEventReference);

    getEventService().sendCellEvent(discoveredCell.getId(), savedDiscoverEvent);
    getEventService().sendPcCreatureInstanceEvent(discoveringPcCreatureInstance.getId(), savedDiscoverEvent);

    getPcCreatureInstanceCellService().save(discoveredPcCreatureInstanceCell);
    getCellService().save(discoveredCell);
    getEventService().save(savedDiscoverEvent);
    return getPcCreatureInstanceService().save(discoveringPcCreatureInstance);
  }

  private long calculateExplorationExperienceValue(final long x, final long y, final long z) {
    final long xValue = Math.abs(x);
    final long yValue = Math.abs(y);
    final long zValue = Math.abs(z);
    final long totalCoordinateValue = xValue + yValue + zValue;

    long minValue;
    long maxValue;

    if (totalCoordinateValue < 0) {
      minValue = (long) Math.ceil(Long.MAX_VALUE * 0.75);
      maxValue = (long) Math.ceil(Long.MAX_VALUE);
    } else {
      minValue = (long) Math.ceil(totalCoordinateValue * 0.75);
      maxValue = (long) Math.ceil(totalCoordinateValue * 1.25);
    }

    final long diffValue = maxValue - minValue;
    final double random = Math.random();
    final double randomTimesDiff = random * diffValue;
    final double randomTimesDiffCeil = Math.round(randomTimesDiff);
    final long randDiff = (long) randomTimesDiffCeil;
    return randDiff + minValue;
  }

  private DropTable generateCreatureInstanceCellDropTable(final PcCreatureInstanceCell pcCreatureInstanceCell) {
    return getDropTableService().createDropTable(pcCreatureInstanceCell);
  }

  private Roster generateCreatureInstanceCellRoster(final PcCreatureInstanceCell pcCreatureInstanceCell) {
    return getRosterService().createRoster(pcCreatureInstanceCell);
  }

  private SpawnTable generateCreatureInstanceCellSpawnTable(final PcCreatureInstanceCell pcCreatureInstanceCell) {
    return getSpawnTableService().createSpawnTable(pcCreatureInstanceCell);
  }

  private DropTable generateCellDropTable(final Cell cell) {
    final DropTable dropTable = getDropTableService().createDropTable(cell);
    final List<ConsistentItemDrop> consistentItemDrops = generateConsistentItemDrops(dropTable);
    final List<RandomizedItemDrop> randomizedItemDrops = generateRandomizedItemDrops(dropTable);

    dropTable.getConsistentItemDrops().addAll(consistentItemDrops);
    dropTable.getRandomizedItemDrops().addAll(randomizedItemDrops);

    return getDropTableService().save(dropTable);
  }
  
  private SpawnTable generateCellSpawnTable(final Cell cell) {
    final SpawnTable spawnTable = getSpawnTableService().createSpawnTable(cell);
    final List<ConsistentCreatureSpawn> consistentCreatureSpawns = generateConsistentCreatureSpawns(spawnTable);
    final List<RandomizedCreatureSpawn> randomizedCreatureSpawns = generateRandomizedCreatureSpawns(spawnTable);
    
    spawnTable.getConsistentCreatureSpawns().addAll(consistentCreatureSpawns);
    spawnTable.getRandomizedCreatureSpawns().addAll(randomizedCreatureSpawns);
    
    return getSpawnTableService().save(spawnTable);
  }

  private List<ConsistentItemDrop> generateConsistentItemDrops(final DropTable dropTable) {
    final Item item = getItemService().findByName("Rotten Wood Sword");
    final ItemInstance itemInstanceToCopy = getItemInstanceService().createItemInstance(item, 1L);
    final ConsistentItemDrop consistentItemDrop = getConsistentItemDropService().createConsistentItemDrop(dropTable, itemInstanceToCopy, 2500L);

    itemInstanceToCopy.getConsistentItemDrops().add(consistentItemDrop);
    getItemInstanceService().save(itemInstanceToCopy);

    return Collections.singletonList(consistentItemDrop);
  }

  private List<RandomizedItemDrop> generateRandomizedItemDrops(final DropTable dropTable) {
    final Item item = getItemService().findByName("Rotten Wood Pieces");
    final RandomizedItemDrop randomizedItemDrop = getRandomizedItemDropService().createRandomizedItemDrop(dropTable, item, 10000L, 1L, 5L);

    item.getRandomizedItemDrops().add(randomizedItemDrop);
    getItemService().save(item);

    return Collections.singletonList(randomizedItemDrop);
  }

  private List<ConsistentCreatureSpawn> generateConsistentCreatureSpawns(final SpawnTable spawnTable) {
    final Creature creature = getCreatureService().findByName("Mag'Goldfish");
    final NpcCreatureInstance npcCreatureInstanceToCopy = getNpcCreatureInstanceService().createNpcCreatureInstance(creature);
    final ConsistentCreatureSpawn consistentCreatureSpawn = getConsistentCreatureSpawnService().createConsistentCreatureSpawn(spawnTable, npcCreatureInstanceToCopy, 2500L);

    npcCreatureInstanceToCopy.getConsistentCreatureSpawns().add(consistentCreatureSpawn);
    getNpcCreatureInstanceService().save(npcCreatureInstanceToCopy);

    return Collections.singletonList(consistentCreatureSpawn);
  }

  private List<RandomizedCreatureSpawn> generateRandomizedCreatureSpawns(final SpawnTable spawnTable) {
    final Creature creature = getCreatureService().findByName("Goldfish");
    final RandomizedCreatureSpawn randomizedCreatureSpawn = getRandomizedCreatureSpawnService().createRandomizedCreatureSpawn(spawnTable, creature, 10000L, 1L, 2L);

    creature.getRandomizedCreatureSpawns().add(randomizedCreatureSpawn);
    getCreatureService().save(creature);

    return Collections.singletonList(randomizedCreatureSpawn);
  }

  private Inventory generateCreatureInstanceCellInventory(final PcCreatureInstanceCell pcCreatureInstanceCell) {
    return getInventoryService().createInventory(pcCreatureInstanceCell);
  }

  private Inventory generateCellInventory(final Cell cell) {
    return getInventoryService().createInventory(cell);
  }

  private Roster generateCellRoster(final Cell cell) {
    return getRosterService().createRoster(cell);
  }

  private ItemInstance generateItemInstanceFrom(final RandomizedItemDrop randomizedItemDrop) {
    final long dropChance = randomizedItemDrop.getDropChance();
    final long roll = ThreadLocalRandom.current().nextLong(10000L);

    if (roll > dropChance) {
      return ItemInstance.NOT_DROPPED;
    }

    final Item item = randomizedItemDrop.getItem();

    final long minStacks = randomizedItemDrop.getMinStacks();
    final long maxStacks = randomizedItemDrop.getMaxStacks();

    final long numStacks = minStacks + ThreadLocalRandom.current().nextLong((maxStacks - minStacks) + 1L);

    final ItemInstance createdItemInstance = getItemInstanceService().createItemInstance(item, numStacks);

    item.getItemInstances().add(createdItemInstance);
    getItemService().save(item);

    return createdItemInstance;
  }

  private ItemInstance generateItemInstanceFrom(final ConsistentItemDrop consistentItemDrop) {
    final long dropChance = consistentItemDrop.getDropChance();
    final long roll = ThreadLocalRandom.current().nextLong(10000);

    if (roll > dropChance) {
      return ItemInstance.NOT_DROPPED;
    }

    final ItemInstance itemInstanceToCopy = consistentItemDrop.getItemInstanceToCopy();
    final Item item = itemInstanceToCopy.getItem();

    final ItemInstance itemInstanceCopy = getItemInstanceService().createCopyOfItemInstance(itemInstanceToCopy);
    item.getItemInstances().add(itemInstanceCopy);
    getItemService().save(item);

    return itemInstanceCopy;
  }

  private List<CreatureInstance> generateCreatureInstancesFrom(final RandomizedCreatureSpawn randomizedCreatureSpawn) {
    final long spawnChance = randomizedCreatureSpawn.getSpawnChance();
    final long roll = ThreadLocalRandom.current().nextLong(10000L);

    if (roll > spawnChance) {
      return Collections.emptyList();
    }

    final Creature creature = randomizedCreatureSpawn.getCreature();

    final long minSpawns = randomizedCreatureSpawn.getMinSpawns();
    final long maxSpawns = randomizedCreatureSpawn.getMaxSpawns();

    final long numSpawns = minSpawns + ThreadLocalRandom.current().nextLong((maxSpawns - minSpawns) + 1L);

    final List<CreatureInstance> generatedCreatureInstances = new ArrayList<>();

    for(int i=0; i<numSpawns; i++) {
      final CreatureInstance createdCreatureInstance = getNpcCreatureInstanceService().createNpcCreatureInstance(creature);

      creature.getCreatureInstances().add(createdCreatureInstance);
      generatedCreatureInstances.add(createdCreatureInstance);
    }

    getCreatureService().save(creature);

    return generatedCreatureInstances;
  }

  private List<CreatureInstance> generateCreatureInstancesFrom(final ConsistentCreatureSpawn consistentCreatureSpawn) {
    final long spawnChance = consistentCreatureSpawn.getSpawnChance();
    final long roll = ThreadLocalRandom.current().nextLong(10000);

    if (roll > spawnChance) {
      return Collections.emptyList();
    }

    final CreatureInstance creatureInstanceToCopy = consistentCreatureSpawn.getCreatureInstanceToCopy();
    final Creature creature = creatureInstanceToCopy.getCreature();

    final NpcCreatureInstance npcCreatureInstanceCopy = getNpcCreatureInstanceService().createCopyOfCreatureInstance(creatureInstanceToCopy);
    creature.getCreatureInstances().add(npcCreatureInstanceCopy);
    getCreatureService().save(creature);

    return Collections.singletonList(npcCreatureInstanceCopy);
  }

  private void addAllItemInstancesToInventory(final List<ItemInstance> itemInstancesToAdd, final Inventory inventory) {
    log.info("Adding {} item instances to inventory {}", itemInstancesToAdd.size(), inventory.getId());

    final List<ItemInstance> inventoryItemInstancesToUpdate = new ArrayList<>();
    final List<ItemInstance> itemInstancesToDelete = new ArrayList<>();

    final Iterator<ItemInstance> itemInstancesToAddIterator = itemInstancesToAdd.iterator();

    while (itemInstancesToAddIterator.hasNext()) {
      final ItemInstance itemInstanceToAdd = itemInstancesToAddIterator.next();
      log.info("Adding item instance {} to inventory", itemInstanceToAdd.getId());

      if (!itemInstanceToAdd.isStackable()) {
        log.info("Item instance {} is not stackable, adding it to inventory item list", itemInstanceToAdd.getId());
        itemInstanceToAdd.setInventory(inventory);
        inventory.getItemInstances().add(itemInstanceToAdd);
        continue;
      }

      final Optional<ItemInstance> existingItemInstanceOptional = inventory.getItemInstances().stream()
          .filter(inventoryItemInstance -> inventoryItemInstance.getItemIdLod().equals(itemInstanceToAdd.getItemIdLod()))
          .findFirst();

      if (existingItemInstanceOptional.isEmpty()) {
        log.info("Item {} ({}) does not already exist in inventory; Adding item instance {} to inventory item list", itemInstanceToAdd.getItemIdLod(),
            itemInstanceToAdd.getItemName(), itemInstanceToAdd.getId());
        itemInstanceToAdd.setInventory(inventory);
        inventory.getItemInstances().add(itemInstanceToAdd);
        continue;
      }

      final ItemInstance existingItemInstance = existingItemInstanceOptional.get();

      log.info("Item {} ({}) already exists in inventory as item instance {}", existingItemInstance.getItemIdLod(),
          existingItemInstance.getItemName(), existingItemInstance.getId());

      if (existingItemInstance.getCurrentStacks() == existingItemInstance.getMaxStacks()) {
        log.info("Existing inventory item instance {} is already at the maximum stacks, not adding stacks from item instance {}",
            existingItemInstance.getId(), itemInstanceToAdd.getId());
        // TODO Message that this item instance cannot hold any more stacks
        continue;
      }

      if (existingItemInstance.getCurrentStacks() + itemInstanceToAdd.getCurrentStacks() > existingItemInstance.getMaxStacks()) {
        log.info("Existing inventory item instance {} cannot hold all of the stacks from item instance {}; " +
                "It would cause the current stacks to be greater than the max stacks value; Adding enough stacks to put it at its max value.",
            existingItemInstance.getId(), itemInstanceToAdd.getId());

        // TODO Message that some stacks were added but not all of them could be added

        final long difference = existingItemInstance.getMaxStacks() - existingItemInstance.getCurrentStacks();

        log.info("Adding {} stacks from item instance {} into existing inventory item instance {}", difference, itemInstanceToAdd.getId(),
            existingItemInstance.getId());

        existingItemInstance.setCurrentStacks(existingItemInstance.getMaxStacks());
        inventoryItemInstancesToUpdate.add(existingItemInstance);
        itemInstanceToAdd.setCurrentStacks(itemInstanceToAdd.getCurrentStacks() - difference);
        continue;
      }

      if (existingItemInstance.getCurrentStacks() + itemInstanceToAdd.getCurrentStacks() < 0L) {
        log.info("Existing inventory item instance {} cannot hold all of the stacks from item instance {}; " +
                "It would cause the current stacks to be greater than the maximum long value (holy shit); Adding enough stacks to put it at long's max value.",
            existingItemInstance.getId(), itemInstanceToAdd.getId());

        // TODO Message that some stacks were added but not all of them could be added

        final long difference = Long.MAX_VALUE - existingItemInstance.getCurrentStacks();

        log.info("Adding {} stacks from item instance {} into existing inventory item instance {}", difference, itemInstanceToAdd.getId(),
            existingItemInstance.getId());

        existingItemInstance.setCurrentStacks(Long.MAX_VALUE);
        inventoryItemInstancesToUpdate.add(existingItemInstance);
        itemInstanceToAdd.setCurrentStacks(itemInstanceToAdd.getCurrentStacks() - difference);
        continue;
      }

      log.info("Adding all {} stacks from item instance {} into existing inventory item instance {}", itemInstanceToAdd.getCurrentStacks(),
          itemInstanceToAdd.getId(), existingItemInstance.getId());

      existingItemInstance.setCurrentStacks(existingItemInstance.getCurrentStacks() + itemInstanceToAdd.getCurrentStacks());
      inventoryItemInstancesToUpdate.add(existingItemInstance);

      itemInstanceToAdd.setCurrentStacks(0);
      itemInstancesToDelete.add(itemInstanceToAdd);
      itemInstancesToAddIterator.remove();
    }

    log.info("Saving all item instances that were updated");
    final List<ItemInstance> itemInstancesToUpdate = Stream.concat(
        inventoryItemInstancesToUpdate.stream(), itemInstancesToAdd.stream())
        .collect(Collectors.toList());
    getItemInstanceService().saveAll(itemInstancesToUpdate);

    log.info("Deleting all item instances that no longer have stacks");
    getItemInstanceService().deleteAll(itemInstancesToDelete);

    log.info("Saving the inventory that had items added to it");
    getInventoryService().save(inventory);
  }

  private List<ItemInstance> removeAllItemInstancesFromInventory(final List<ItemInstance> itemInstancesToRemove,
      final Inventory inventory) {
    final List<ItemInstance> removedItemInstances = new ArrayList<>();

    final List<ItemInstance> inventoryItemInstances = inventory.getItemInstances();

    final Iterator<ItemInstance> iterator = itemInstancesToRemove.iterator();

    while (iterator.hasNext()) {
      final ItemInstance itemInstanceToRemove = iterator.next();

      final Optional<ItemInstance> inventoryItemInstanceToRemoveOptional = inventoryItemInstances.stream()
          .filter(inventoryItemInstance -> inventoryItemInstance.getId().equals(itemInstanceToRemove.getId())).findFirst();

      if (inventoryItemInstanceToRemoveOptional.isEmpty()) {
        log.warn("Tried to remove an item instance from an inventory where that item instance does not exist");
        continue;
      }

      iterator.remove();
      itemInstanceToRemove.setInventory(null);
      removedItemInstances.add(itemInstanceToRemove);
    }

    getInventoryService().save(inventory);

    return getItemInstanceService().saveAll(removedItemInstances);
  }

  private Combatant generatePlayerCombatant(final PcCreatureInstance pcCreatureInstance, final Combat combat) {
    return generateCombatant(pcCreatureInstance, combat, CombatPosition.FIRST, CombatTeam.BLUE);
  }

  private List<Combatant> generateEnemyCombatants(final List<CreatureInstance> creatureInstances, final Combat combat) {

    final CombatTeam combatTeam = CombatTeam.RED;

    return IntStream.range(0, creatureInstances.size())
        .mapToObj(index -> {
          final CreatureInstance creatureInstance = creatureInstances.get(index);
          final CombatPosition combatPosition = CombatPosition.getByIntValue(index + 1);
          return generateCombatant(creatureInstance, combat, combatPosition, combatTeam);
        }).collect(Collectors.toList());
  }

  private Combatant generateCombatant(final CreatureInstance creatureInstance, final Combat combat, final CombatPosition combatPosition,
      final CombatTeam combatTeam) {
    return getCombatantService().createCombatant(creatureInstance, combat, combatPosition, combatTeam);
  }
}
