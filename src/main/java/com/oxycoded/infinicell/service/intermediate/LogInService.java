package com.oxycoded.infinicell.service.intermediate;

import com.oxycoded.infinicell.application.configuration.security.JwtTokenService;
import com.oxycoded.infinicell.domain.model.*;
import com.oxycoded.infinicell.service.AccountService;
import com.oxycoded.infinicell.service.EventReferenceService;
import com.oxycoded.infinicell.service.EventService;
import com.oxycoded.infinicell.service.FriendService;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
@Slf4j
public class LogInService {

  private final AccountService accountService;
  private final JwtTokenService jwtTokenService;

  private final FriendService friendService;
  private final EventService eventService;
  private final EventReferenceService eventReferenceService;

  @Transactional
  public LogInAccount accountLogInWithCode(final String logInCode) {
    final Account account = getAccountService().findByLogInCode(logInCode);
    account.setAccountActivityType(AccountActivityType.ONLINE);
    final Account updatedActivityAccount = getAccountService().save(account);

    final String token = getJwtTokenService().createToken(updatedActivityAccount);

    final LogInAccount logInAccount = new LogInAccount();
    logInAccount.setAccount(updatedActivityAccount);
    logInAccount.setToken(token);

    final Account logInEventAccount = persistLogInEvent(updatedActivityAccount);

    for (final Friend friend : logInEventAccount.getFriends()) {
      try {
        persistAndSendFriendLogInEvent(logInEventAccount, friend);
      } catch (final Exception e) {
        log.error("Unexpected exception while trying to create and/or send friend log in event: ", e);
      }
    }

    return logInAccount;
  }

  @Transactional
  public LogInAccount accountLogInWithUsernamePassword(final String username, final String password) {
    //    TODO Implement username and password logins

    //    UsernamePasswordAuthenticationToken credentials = new UsernamePasswordAuthenticationToken(username, password);
    //    try {
    //      final Authentication authentication = getAuthenticationProvider().authenticate(credentials);
    //    } catch (AuthenticationException ex) {
    //      throw new BadCredentialsException(email);
    //    }
    //
    //    final Account account = getAccountService().findByUsername(username);
    //    final String token = getInfinicellUserDetailsService().createToken(account)
    //
    //    final LogInAccount logInAccount = new LogInAccount();
    //    logInAccount.setAccount(account);
    //    logInAccount.setToken(token);
    //
    //    return logInAccount;
    //
    return null;
  }

  private Account persistLogInEvent(final Account loggingInAccount) {
    final Event accountLogInViaCodeEvent = new Event();
    accountLogInViaCodeEvent.setEventType(EventType.ACCOUNT_LOG_IN_VIA_CODE);
    final Event savedAccountLogInViaCodeEvent = getEventService().save(accountLogInViaCodeEvent);

    final EventReference loggingInAccountEventReference = new EventReference();
    loggingInAccountEventReference.setEventReferenceEnabledEntity(loggingInAccount);
    loggingInAccountEventReference.setEventReferenceType(EventReferenceType.LOGGING_IN_ACCOUNT);
    loggingInAccountEventReference.setEvent(savedAccountLogInViaCodeEvent);
    getEventReferenceService().save(loggingInAccountEventReference);

    savedAccountLogInViaCodeEvent.getEventReferences().add(loggingInAccountEventReference);
    getEventService().save(savedAccountLogInViaCodeEvent);

    loggingInAccount.getEventReferences().add(loggingInAccountEventReference);
    return getAccountService().save(loggingInAccount);
  }

  private void persistAndSendFriendLogInEvent(Account loggingInAccount, Friend friendToNotify) {

    final Event friendLogInEvent = new Event();
    friendLogInEvent.setEventType(EventType.FRIEND_LOG_IN);
    final Event savedFriendLogInEvent = getEventService().save(friendLogInEvent);

    final String accountId = loggingInAccount.getId();
    final String friendAccountId = friendToNotify.getAccountOfFriend().getId();

    final Friend loggingInAccountAsFriend = getFriendService().findByAccountIdAndAccountOfFriendId(friendAccountId, accountId);

    final EventReference loggingInFriendEventReference = new EventReference();
    loggingInFriendEventReference.setEventReferenceEnabledEntity(loggingInAccountAsFriend);
    loggingInFriendEventReference.setEventReferenceType(EventReferenceType.LOGGING_IN_FRIEND);
    loggingInFriendEventReference.setEvent(savedFriendLogInEvent);
    getEventReferenceService().save(loggingInFriendEventReference);

    savedFriendLogInEvent.getEventReferences().add(loggingInFriendEventReference);
    final Event finalSavedFriendLogInEvent = getEventService().save(savedFriendLogInEvent);

    loggingInAccountAsFriend.getEventReferences().add(loggingInFriendEventReference);
    getFriendService().save(loggingInAccountAsFriend);

    if (friendToNotify.isOffline()) {
      return;
    }

    getEventService().sendAccountEvent(friendAccountId, finalSavedFriendLogInEvent);
  }
}
