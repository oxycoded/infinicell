package com.oxycoded.infinicell.service.intermediate;

import com.oxycoded.infinicell.domain.model.*;
import com.oxycoded.infinicell.service.AccountService;
import com.oxycoded.infinicell.service.EventReferenceService;
import com.oxycoded.infinicell.service.EventService;
import com.oxycoded.infinicell.service.FriendRequestService;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class FriendRequestManagementService {
  private final AccountService accountService;
  private final FriendRequestService friendRequestService;
  private final EventService eventService;
  private final EventReferenceService eventReferenceService;

  @Transactional
  public FriendRequest createOutgoingFriendRequest(final String currentlyAuthenticatedAccountId, final String recipientAccountId) {
    // TODO Add validation to make sure the requesting account has not already sent a friend request to the recipient account
    final Account requestingAccount = getAccountService().findById(currentlyAuthenticatedAccountId);
    final Account recipientAccount = getAccountService().findById(recipientAccountId);
    final FriendRequest friendRequest = getFriendRequestService().createOutgoingFriendRequest(requestingAccount, recipientAccount);
    requestingAccount.getOutgoingFriendRequests().add(friendRequest);
    recipientAccount.getIncomingFriendRequests().add(friendRequest);
    getAccountService().saveAll(Arrays.asList(recipientAccount, requestingAccount));

    return persistAndSendFriendRequestCreatedEvent(friendRequest);
  }

  private FriendRequest persistAndSendFriendRequestCreatedEvent(final FriendRequest friendRequest) {
    final Event friendRequestCreatedEvent = new Event();
    friendRequestCreatedEvent.setEventType(EventType.FRIEND_REQUEST_CREATED);
    final Event savedFriendRequestCreatedEvent = getEventService().save(friendRequestCreatedEvent);

    final EventReference createdFriendRequestEventReference = new EventReference();
    createdFriendRequestEventReference.setEventReferenceEnabledEntity(friendRequest);
    createdFriendRequestEventReference.setEventReferenceType(EventReferenceType.LOGGING_IN_ACCOUNT);
    createdFriendRequestEventReference.setEvent(savedFriendRequestCreatedEvent);
    getEventReferenceService().save(createdFriendRequestEventReference);

    savedFriendRequestCreatedEvent.getEventReferences().add(createdFriendRequestEventReference);
    getEventService().save(savedFriendRequestCreatedEvent);

    friendRequest.getEventReferences().add(createdFriendRequestEventReference);
    final FriendRequest savedFriendRequest = getFriendRequestService().save(friendRequest);

    final Account destinationAccount = savedFriendRequest.getRecipientAccount();

    if(destinationAccount.isOnline()) {
      CompletableFuture.runAsync(() -> getEventService().sendAccountEvent(destinationAccount.getId(), savedFriendRequestCreatedEvent));
    }

    return savedFriendRequest;
  }

  @Transactional
  public FriendRequest cancelOutgoingFriendRequest(final String currentlyAuthenticatedAccountId, final String friendRequestId) {
    // TODO May need to update the accounts that reference the friend request to counteract caching
    // TODO Send subscription messages
    return getFriendRequestService().cancelOutgoingFriendRequest(currentlyAuthenticatedAccountId, friendRequestId);
  }

  @Transactional
  public FriendRequest acceptIncomingFriendRequest(final String currentlyAuthenticatedAccountId, final String friendRequestId) {
    // TODO May need to update the accounts that reference the friend request to counteract caching
    // TODO Update accounts that reference the friend request to add them to each other's list of friends
    // TODO Send subscription messages
    return getFriendRequestService().acceptFriendRequest(currentlyAuthenticatedAccountId, friendRequestId);
  }

  @Transactional
  public FriendRequest denyIncomingFriendRequest(final String currentlyAuthenticatedAccountId, final String friendRequestId) {
    // TODO May need to update the accounts that reference the friend request to counteract caching
    // TODO Send subscription messages
    return getFriendRequestService().denyFriendRequest(currentlyAuthenticatedAccountId, friendRequestId);
  }

  @Transactional
  public FriendRequest ignoreIncomingFriendRequest(final String currentlyAuthenticatedAccountId, final String friendRequestId) {
    // TODO May need to update the accounts that reference the friend request to counteract caching
    // TODO Send subscription messages - Maybe? Do we want to let the sender know they were ignored?
    return getFriendRequestService().ignoreFriendRequest(currentlyAuthenticatedAccountId, friendRequestId);
  }
}
