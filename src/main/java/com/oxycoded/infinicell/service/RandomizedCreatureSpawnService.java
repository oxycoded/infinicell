package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.Creature;
import com.oxycoded.infinicell.domain.model.RandomizedCreatureSpawn;
import com.oxycoded.infinicell.domain.model.SpawnTable;
import com.oxycoded.infinicell.repository.RandomizedCreatureSpawnRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class RandomizedCreatureSpawnService {

  private final RandomizedCreatureSpawnRepository randomizedCreatureSpawnRepository;

  public RandomizedCreatureSpawn createRandomizedCreatureSpawn(final SpawnTable spawnTable, final Creature creature, final long spawnChance,
      final long minSpawns, final long maxSpawns) {
    final RandomizedCreatureSpawn randomizedCreatureSpawn = new RandomizedCreatureSpawn();
    randomizedCreatureSpawn.setSpawnTable(spawnTable);
    randomizedCreatureSpawn.setCreature(creature);
    randomizedCreatureSpawn.setSpawnChance(spawnChance);
    randomizedCreatureSpawn.setMinSpawns(minSpawns);
    randomizedCreatureSpawn.setMaxSpawns(maxSpawns);
    return save(randomizedCreatureSpawn);
  }

  public List<RandomizedCreatureSpawn> findAllBySpawnTableId(final String spawnTableId) {
    return getRandomizedCreatureSpawnRepository().findAllBySpawnTableId(spawnTableId);
  }

  public List<RandomizedCreatureSpawn> findAllByCreatureId(final String creatureId) {
    return getRandomizedCreatureSpawnRepository().findAllByCreatureId(creatureId);
  }

  public RandomizedCreatureSpawn save(final RandomizedCreatureSpawn randomizedCreatureSpawn) {
    return getRandomizedCreatureSpawnRepository().save(randomizedCreatureSpawn);
  }
}
