package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.ConsistentItemDrop;
import com.oxycoded.infinicell.domain.model.DropTable;
import com.oxycoded.infinicell.domain.model.ItemInstance;
import com.oxycoded.infinicell.repository.ConsistentItemDropRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class ConsistentItemDropService {

  private final ConsistentItemDropRepository consistentItemDropRepository;

  public ConsistentItemDrop createConsistentItemDrop(final DropTable dropTable, final ItemInstance itemInstanceToCopy, final long dropChance) {
    final ConsistentItemDrop consistentItemDrop = new ConsistentItemDrop();
    consistentItemDrop.setDropTable(dropTable);
    consistentItemDrop.setItemInstanceToCopy(itemInstanceToCopy);
    consistentItemDrop.setDropChance(dropChance);
    return save(consistentItemDrop);
  }

  public List<ConsistentItemDrop> findAllByDropTableId(final String dropTableId) {
    return getConsistentItemDropRepository().findAllByDropTableId(dropTableId);
  }

  public List<ConsistentItemDrop> findAllByItemInstanceToCopyId(final String itemInstanceToCopyId) {
    return getConsistentItemDropRepository().findAllByItemInstanceToCopyId(itemInstanceToCopyId);
  }

  public ConsistentItemDrop save(final ConsistentItemDrop consistentItemDrop) {
    return getConsistentItemDropRepository().save(consistentItemDrop);
  }
}
