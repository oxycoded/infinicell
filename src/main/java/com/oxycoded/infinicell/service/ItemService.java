package com.oxycoded.infinicell.service;

import com.oxycoded.infinicell.domain.model.Item;
import com.oxycoded.infinicell.domain.model.Rarity;
import com.oxycoded.infinicell.domain.model.Tier;
import com.oxycoded.infinicell.exception.ItemNotFoundException;
import com.oxycoded.infinicell.repository.ItemRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Optional;
import java.util.Random;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class ItemService {

  private static final Random RANDOM = new SecureRandom();

  private final ItemRepository itemRepository;

  public Item findByName(final String name) {
    final Optional<Item> itemOptional = getItemRepository().findByName(name);
    if (itemOptional.isEmpty()) {
      throw new ItemNotFoundException();
    }
    return itemOptional.get();
  }

  public Item findByItemInstanceId(final String itemInstanceId) {
    final Optional<Item> itemOptional = getItemRepository().findByItemInstancesId(itemInstanceId);
    if (itemOptional.isEmpty()) {
      throw new ItemNotFoundException();
    }
    return itemOptional.get();
  }

  public Item findByRandomizedItemDropId(final String randomizedItemDropId) {
    final Optional<Item> itemOptional = getItemRepository().findByRandomizedItemDropsId(randomizedItemDropId);
    if (itemOptional.isEmpty()) {
      throw new ItemNotFoundException();
    }
    return itemOptional.get();
  }

  public void save(final Item item) {
    getItemRepository().save(item);
  }

  public void saveAll(final Item... items) {
    getItemRepository().saveAll(Arrays.asList(items));
  }

  @PostConstruct
  private void initializeData() {
    final Item rottenWoodSword = new Item();
    rottenWoodSword.setName("Rotten Wood Sword");
    rottenWoodSword.setRarity(Rarity.COMMON);
    rottenWoodSword.setTier(Tier.TIER_1);
    rottenWoodSword.setMaxStacks(1L);
    rottenWoodSword.setValue(4L);
    rottenWoodSword.setIconSpriteUrl("rotten-wood-sword-icon.png");
    rottenWoodSword.setDetailedSpriteUrl("rotten-wood-sword-detailed.png");

    final Item rottenWoodPieces = new Item();
    rottenWoodPieces.setName("Rotten Wood Pieces");
    rottenWoodPieces.setRarity(Rarity.COMMON);
    rottenWoodPieces.setTier(Tier.TIER_1);
    rottenWoodPieces.setMaxStacks(Long.MAX_VALUE);
    rottenWoodPieces.setValue(1L);
    rottenWoodPieces.setIconSpriteUrl("rotten-wood-pieces-icon.png");
    rottenWoodPieces.setDetailedSpriteUrl("rotten-wood-pieces-detailed.png");


    saveAll(rottenWoodPieces, rottenWoodSword);
  }
}
