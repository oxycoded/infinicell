package com.oxycoded.infinicell;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InfinicellApplication {
  public static void main(String[] args) {
    SpringApplication.run(InfinicellApplication.class, args);
  }
}
