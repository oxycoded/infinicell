package com.oxycoded.infinicell.publisher;

import com.oxycoded.infinicell.domain.model.Event;
import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.subjects.PublishSubject;
import lombok.AccessLevel;
import lombok.Getter;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

@Component
@Getter(AccessLevel.PRIVATE)
public class EventPublisher {

  private static final ConcurrentHashMap<String, PublishSubject<Event>> CELL_PUBLISH_SUBJECTS = new ConcurrentHashMap<>();
  private static final ConcurrentHashMap<String, PublishSubject<Event>> INFINITY_WALKER_PUBLISH_SUBJECTS = new ConcurrentHashMap<>();
  private static final ConcurrentHashMap<String, PublishSubject<Event>> ACCOUNT_PUBLISH_SUBJECTS = new ConcurrentHashMap<>();

  public Publisher<Event> getCellEventPublisher(final String cellId) {
    CELL_PUBLISH_SUBJECTS.putIfAbsent(cellId, PublishSubject.create());
    return CELL_PUBLISH_SUBJECTS.get(cellId).toFlowable(BackpressureStrategy.BUFFER);
  }

  public void publishCellEvent(final String destinationCellId, final Event event) {
    final PublishSubject<Event> publishSubject = CELL_PUBLISH_SUBJECTS.get(destinationCellId);
    if (publishSubject == null) {
      return;
    }
    publishSubject.onNext(event);
  }

  public Publisher<Event> getPcCreatureInstanceEventPublisher(final String pcCreatureInstanceId) {
    final PublishSubject<Event> publishSubject = PublishSubject.create();
    INFINITY_WALKER_PUBLISH_SUBJECTS.putIfAbsent(pcCreatureInstanceId, PublishSubject.create());
    return INFINITY_WALKER_PUBLISH_SUBJECTS.get(pcCreatureInstanceId).toFlowable(BackpressureStrategy.BUFFER);
  }

  public void publishPcCreatureInstanceEvent(final String destinationPcCreatureInstanceId, final Event event) {
    final PublishSubject<Event> publishSubject = INFINITY_WALKER_PUBLISH_SUBJECTS.get(destinationPcCreatureInstanceId);
    if (publishSubject == null) {
      return;
    }
    publishSubject.onNext(event);
  }

  public Publisher<Event> getAccountEventPublisher(final String accountId) {
    ACCOUNT_PUBLISH_SUBJECTS.putIfAbsent(accountId, PublishSubject.create());
    return ACCOUNT_PUBLISH_SUBJECTS.get(accountId).toFlowable(BackpressureStrategy.BUFFER);
  }

  public void publishAccountEvent(final String destinationAccountId, final Event event) {
    final PublishSubject<Event> publishSubject = ACCOUNT_PUBLISH_SUBJECTS.get(destinationAccountId);
    if (publishSubject == null) {
      return;
    }
    publishSubject.onNext(event);
  }
}
