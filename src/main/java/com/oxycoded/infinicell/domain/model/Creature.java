package com.oxycoded.infinicell.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Creature extends DropTableEnabledEntity {

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private String description;

  @Column(nullable = false)
  private String iconSpriteUrl = "unimplemented-icon.png";

  @Column(nullable = false)
  private String detailedSpriteUrl = "unimplemented-detailed.png";

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "creature")
  private List<RandomizedCreatureSpawn> randomizedCreatureSpawns = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "creature")
  private List<CreatureInstance> creatureInstances = new ArrayList<>();
}
