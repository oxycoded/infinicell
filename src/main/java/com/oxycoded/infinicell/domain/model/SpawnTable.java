package com.oxycoded.infinicell.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class SpawnTable extends InfinicellEntity {
  @OneToOne
  @JoinColumn(name = "spawn_table_enabled_entity_id")
  private SpawnTableEnabledEntity spawnTableEnabledEntity;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "spawnTable")
  private List<ConsistentCreatureSpawn> consistentCreatureSpawns = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "spawnTable")
  private List<RandomizedCreatureSpawn> randomizedCreatureSpawns = new ArrayList<>();
}
