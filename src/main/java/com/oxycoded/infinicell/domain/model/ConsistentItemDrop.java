package com.oxycoded.infinicell.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ConsistentItemDrop extends InfinicellEntity {

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "drop_table_id", nullable = false)
  private DropTable dropTable;

  @Column(nullable = false)
  private long dropChance;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "item_instance_id", nullable = false)
  private ItemInstance itemInstanceToCopy;
}
