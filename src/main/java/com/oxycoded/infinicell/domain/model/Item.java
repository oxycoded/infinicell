package com.oxycoded.infinicell.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Item extends InfinicellEntity {
  @Column(nullable = false, unique = true)
  private String name;

  @Column(nullable = false)
  private long value;

  @Column(nullable = false)
  private long maxStacks = Long.MAX_VALUE;

  @Column(nullable = false)
  private Rarity rarity = Rarity.COMMON;

  @Column(nullable = false)
  private Tier tier = Tier.TIER_1;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "item")
  private List<ItemInstance> itemInstances = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "item")
  private List<RandomizedItemDrop> randomizedItemDrops = new ArrayList<>();

  @Column(nullable = false)
  private String iconSpriteUrl;

  @Column(nullable = false)
  private String detailedSpriteUrl;

  @Transient
  public boolean isStackable() {
    return getMaxStacks() > 1;
  }
}
