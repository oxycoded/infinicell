package com.oxycoded.infinicell.domain.model;

public enum CombatEventType {
  ATTACK,
  TAKE_DAMAGE
}
