package com.oxycoded.infinicell.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class PcCreatureInstance extends CreatureInstance {

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "account_id", nullable = false)
  private Account account;

  @OneToOne(mappedBy = "selectedPcCreatureInstance", cascade = CascadeType.ALL)
  @PrimaryKeyJoinColumn
  private Account selectedByAccount;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "pcCreatureInstance")
  private List<PcCreatureInstanceCell> pcCreatureInstanceCells = new ArrayList<>();

  // TODO refactor this to just retrieve the exact cell from the db; Looping though all of them gets cumbersome
  @Transient
  public PcCreatureInstanceCell getCurrentPcCreatureInstanceCell() {
    final Optional<PcCreatureInstanceCell> currentCellOptional = getPcCreatureInstanceCells().stream()
        .filter(PcCreatureInstanceCell::isCurrent)
        .findFirst();
    if (currentCellOptional.isEmpty()) {
      throw new RuntimeException("Creature instance is not currently in a cell??");
    }
    return currentCellOptional.get();
  }

  @Transient
  public Cell getCurrentCell() {
    return getCurrentPcCreatureInstanceCell().getCell();
  }

}
