package com.oxycoded.infinicell.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Combat extends EventReferenceEnabledEntity {
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "pc_creature_instance_cell_id", nullable = false)
  private PcCreatureInstanceCell pcCreatureInstanceCell;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "combat")
  private List<Combatant> combatants = new ArrayList<>();

  @Column(nullable = false)
  private CombatStatus combatStatus;

  public boolean isRoundReady() {
    for (Combatant combatant : getCombatants()) {
      if (!combatant.hasSubmittedCombatAction()) {
        return false;
      }
    }
    return true;
  }

  public void addCombatant(final Combatant combatant) {
    getCombatants().add(combatant);
  }

  public CombatRoundResult performRound() {
    final CombatRoundResult combatRoundResult = new CombatRoundResult();
    for( final Combatant combatant : getCombatants()) {
      final CombatAction submittedCombatAction = combatant.getSubmittedCombatAction();
      if (submittedCombatAction.getCombatActionType() == CombatActionType.ATTACK) {
        combatRoundResult.addCombatEvent(new CombatEvent(CombatEventType.ATTACK, submittedCombatAction.getTargets().get(0).getTarget(), null, 0));
        combatRoundResult.addCombatEvent(new CombatEvent(CombatEventType.ATTACK, submittedCombatAction.getTargets().get(0).getTarget(), null, 0));
      }
    }
    return combatRoundResult;
  }
}
