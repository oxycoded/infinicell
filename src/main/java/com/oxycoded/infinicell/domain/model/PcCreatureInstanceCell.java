package com.oxycoded.infinicell.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class PcCreatureInstanceCell extends SpawnTableEnabledEntity {

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "cell_id", nullable = false)
  private Cell cell;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "pc_creature_instance_id", nullable = false)
  private PcCreatureInstance pcCreatureInstance;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "pcCreatureInstanceCell")
  private List<Combat> combats = new ArrayList<>();

  @Column(nullable = false)
  private boolean cleared;

  @Column(nullable = false)
  private boolean searched;

  @Column(nullable = false)
  private boolean scouted;

  @Column(nullable = false)
  private boolean current;

  @Column(nullable = false)
  private boolean claimed;

  @Column(nullable = false)
  private boolean discovered;

  @Column(nullable = false)
  private long timesVisited;

  @Transient
  public long getX() {
    return getCell().getX();
  }

  @Transient
  public long getY() {
    return getCell().getY();
  }

  @Transient
  public long getZ() {
    return getCell().getZ();
  }

  @Transient
  public long getExplorationExperienceValue() {
    return getCell().getExplorationExperienceValue();
  }
}
