package com.oxycoded.infinicell.domain.model;

import java.util.ArrayList;
import java.util.List;

public class CombatRoundResult {
  private final List<CombatEvent> combatEvents = new ArrayList<>();

  public void addCombatEvent(final CombatEvent combatEvent) {
    getCombatEvents().add(combatEvent);
  }

  public List<CombatEvent> getCombatEvents() {
    return combatEvents;
  }
}
