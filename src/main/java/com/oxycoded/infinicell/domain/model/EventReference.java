package com.oxycoded.infinicell.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class EventReference extends InfinicellEntity {

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "event_id", nullable = false)
  private Event event;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "event_reference_enabled_entity_id", nullable = false)
  private EventReferenceEnabledEntity eventReferenceEnabledEntity;

  @Column(nullable = false)
  private EventReferenceType eventReferenceType;
}
