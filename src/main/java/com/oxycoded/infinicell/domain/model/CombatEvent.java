package com.oxycoded.infinicell.domain.model;

public class CombatEvent {
  private final CombatEventType combatEventType;
  private final Combatant target;
  private final Combatant damagedCombatant;
  private final int damageAmount;

  public CombatEvent(final CombatEventType combatEventType, final Combatant target, final Combatant damagedCombatant, final int damageAmount) {
    this.combatEventType = combatEventType;
    this.target = target;
    this.damagedCombatant = damagedCombatant;
    this.damageAmount = damageAmount;
  }

  public CombatEventType getCombatEventType() {
    return combatEventType;
  }

  public Combatant getTarget() {
    return target;
  }

  public Combatant getDamagedCombatant() {
    return damagedCombatant;
  }

  public int getDamageAmount() {
    return damageAmount;
  }
}
