package com.oxycoded.infinicell.domain.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter(AccessLevel.PROTECTED)
@Setter(AccessLevel.PRIVATE)
@Entity
public class Account extends EventReferenceEnabledEntity {

  @Column(nullable = false, unique = true)
  private String username;

  @Column(unique = true)
  private String email;

  @Column
  private String passwordHash;

  @Column(nullable = false, unique = true)
  private String logInCode;

  @Column(nullable = false, unique = true)
  private String accountIdentifier;

  @Column(nullable = false)
  private AccountActivityType accountActivityType = AccountActivityType.OFFLINE;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
  private List<PcCreatureInstance> pcCreatureInstances = new ArrayList<>();

  @OneToOne
  @JoinColumn(name = "selected_pc_creature_instance_id")
  private PcCreatureInstance selectedPcCreatureInstance;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
  private List<Friend> friends = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "accountOfFriend")
  private List<Friend> friendOfs = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "requestingAccount")
  private List<FriendRequest> outgoingFriendRequests = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "recipientAccount")
  private List<FriendRequest> incomingFriendRequests = new ArrayList<>();

  public static Account createAnonymousAccount(final String username) {
    return new Account(username, null, null);
  }

  public static Account createAccount(final String username, final String email, final String password, final String confirmPassword) {
    validatePassword(password, confirmPassword);
    return new Account(username, email, password);
  }

  private static void validatePassword(final String password, final String confirmPassword) {
    if (StringUtils.isAnyBlank(password, confirmPassword) || !StringUtils.equals(password, confirmPassword)) {
      throw new RuntimeException("password and confirmPassword are required, and they must be equivalent");
    }
  }

  private Account (final String username, final String email, final String passwordHash) {
    this.username = username;
    this.email = email;
    this.passwordHash = passwordHash;
  }

  @Transient
  @PrePersist
  protected void onCreate() {
    setLogInCode(UUID.randomUUID().toString());
    setAccountIdentifier(UUID.randomUUID().toString());
  }

  @Transient
  public void updateAccountActivity(final AccountActivityType accountActivityType) {
    setAccountActivityType(accountActivityType);
  }

  @Transient
  public void selectPcCreatureInstance(final PcCreatureInstance pcCreatureInstance) {
    setSelectedPcCreatureInstance(pcCreatureInstance);
  }

  @Transient
  public boolean isOnline() {
    return AccountActivityType.ONLINE.equals(getAccountActivityType());
  }

  @Transient
  public boolean isOffline() {
    return AccountActivityType.OFFLINE.equals(getAccountActivityType());
  }




  //TODO Refactor to remove below methods that only exist to get legacy code to compile

  public PcCreatureInstance getSelectedPcCreatureInstance() {
    return selectedPcCreatureInstance;
  }

  public List<FriendRequest> getOutgoingFriendRequests() {
    return outgoingFriendRequests;
  }

  public List<FriendRequest> getIncomingFriendRequests() {
    return incomingFriendRequests;
  }

  public List<Friend> getFriends() {
    return friends;
  }

  public AccountActivityType getAccountActivityType() {
    return accountActivityType;
  }

  public void setAccountActivityType(final AccountActivityType accountActivityType) {
    this.accountActivityType = accountActivityType;
  }

  public String getLogInCode() {
    return logInCode;
  }


}
