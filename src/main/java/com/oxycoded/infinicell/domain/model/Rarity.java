package com.oxycoded.infinicell.domain.model;

public enum Rarity {
  COMMON,
  UNCOMMON,
  RARE,
  SUPER_RARE,
  HYPER_RARE,
  LEGENDARY,
  MYTHIC,
  FABLED
}
