package com.oxycoded.infinicell.domain.model;

public enum FriendRequestStatus {
  OPEN,
  ACCEPTED,
  DENIED,
  IGNORED,
  CANCELED;

  public boolean isCancelable() {
    return OPEN.equals(this) || IGNORED.equals(this);
  }
}
