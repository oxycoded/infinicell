package com.oxycoded.infinicell.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class DropTable extends InfinicellEntity {
  @OneToOne
  @JoinColumn(name = "drop_table_enabled_entity_id")
  private DropTableEnabledEntity dropTableEnabledEntity;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "dropTable")
  private List<ConsistentItemDrop> consistentItemDrops = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "dropTable")
  private List<RandomizedItemDrop> randomizedItemDrops = new ArrayList<>();
}
