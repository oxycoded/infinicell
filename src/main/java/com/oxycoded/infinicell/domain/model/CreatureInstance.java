package com.oxycoded.infinicell.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class CreatureInstance extends DropTableEnabledEntity {
  public static final CreatureInstance NOT_SPAWNED = new CreatureInstance();

  @Column(nullable = false)
  private String name = "Creature";

  @Column(nullable = false)
  private String description = "Creature-like";

  @Column(nullable = false)
  private long experience = 0L;

  @Column(nullable = false)
  private String iconSpriteUrl = "unimplemented-icon.png";

  @Column(nullable = false)
  private String detailedSpriteUrl = "unimplemented-detailed.png";

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "creature_id", nullable = false)
  private Creature creature;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "creatureInstance")
  private List<Combatant> combatants = new ArrayList<>();

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "roster_id")
  private Roster roster;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "creatureInstanceToCopy")
  private List<ConsistentCreatureSpawn> consistentCreatureSpawns = new ArrayList<>();

  @Column(nullable = false)
  private int currentHealth = 10;

  @Column(nullable = false)
  private int maxHealth = 10;

  public void takeDamage(int damage) {
    final int healthAfterDamage = getCurrentHealth() - damage;
    final int healthFlooredAtZero = Math.max(healthAfterDamage, 0);
    setCurrentHealth(healthFlooredAtZero);
  }

  public void heal(int healAmount) {
    final int healthAfterHeal = getCurrentHealth() + healAmount;
    final int healthCappedAtMax = Math.min(healthAfterHeal, getMaxHealth());
    setCurrentHealth(healthCappedAtMax);
  }
}
