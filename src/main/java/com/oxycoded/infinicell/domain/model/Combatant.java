package com.oxycoded.infinicell.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Combatant extends EventReferenceEnabledEntity {
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "creature_instance_id", nullable = false)
  private CreatureInstance creatureInstance;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "combat_id", nullable = false)
  private Combat combat;

  @Column(nullable = false)
  private CombatTeam combatTeam;

  @Column(nullable = false)
  private CombatPosition combatPosition;

  @Column(nullable = false)
  private CombatantStatus combatantStatus;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "combatant")
  private List<CombatAction> combatActions = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "target")
  private List<CombatActionTarget> targetedBy = new ArrayList<>();

  public void submitCombatAction(final CombatAction combatAction) {
    if(hasSubmittedCombatAction()) {
      getCombatActions().stream().filter(CombatAction::isSubmitted).forEach(CombatAction::markAsOverridden);
    }
    combatAction.markAsSubmitted();
    getCombatActions().add(combatAction);
  }

  public CombatAction getSubmittedCombatAction() {
    return getCombatActions().stream().filter(CombatAction::isSubmitted).findAny().orElse(null);
  }

  public boolean hasSubmittedCombatAction() {
    return getCombatActions().stream().anyMatch(CombatAction::isSubmitted);
  }
}
