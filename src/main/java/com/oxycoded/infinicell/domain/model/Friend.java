package com.oxycoded.infinicell.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"account_id", "account_id_of_friend"}))
public class Friend extends EventReferenceEnabledEntity {
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "account_id", nullable = false)
  private Account account;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "account_id_of_friend", nullable = false)
  private Account accountOfFriend;

  @Column
  private String notes;

  @Transient
  public boolean isOffline() {
    return getAccountOfFriend().isOffline();
  }
}
