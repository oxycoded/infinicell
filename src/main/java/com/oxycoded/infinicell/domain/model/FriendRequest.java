package com.oxycoded.infinicell.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class FriendRequest extends EventReferenceEnabledEntity {
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "requesting_account_id", nullable = false)
  private Account requestingAccount;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "recipient_account_id", nullable = false)
  private Account recipientAccount;

  @Column(nullable = false)
  private FriendRequestStatus friendRequestStatus;

  @PrePersist
  protected void onCreate() {
    setFriendRequestStatus(FriendRequestStatus.OPEN);
  }
}
