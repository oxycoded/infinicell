package com.oxycoded.infinicell.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class SpawnTableEnabledEntity extends RosterEnabledEntity {
  @OneToOne(mappedBy = "spawnTableEnabledEntity", cascade = CascadeType.ALL)
  @PrimaryKeyJoinColumn
  private SpawnTable spawnTable;
}
