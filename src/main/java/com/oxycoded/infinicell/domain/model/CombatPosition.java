package com.oxycoded.infinicell.domain.model;

import lombok.Getter;

@Getter
public enum CombatPosition {
  FIRST(1),
  SECOND(2),
  THIRD(3);

  private final int intValue;

  CombatPosition(final int intValue) {
    this.intValue = intValue;
  }

  public static CombatPosition getByIntValue(int intValue) {
    for(final CombatPosition combatPosition : CombatPosition.values()) {
      if(combatPosition.getIntValue() == intValue) {
        return combatPosition;
      }
    }
    throw new IllegalArgumentException("intValue is not valid");
  }
}
