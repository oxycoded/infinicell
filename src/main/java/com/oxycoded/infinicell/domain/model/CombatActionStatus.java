package com.oxycoded.infinicell.domain.model;

public enum CombatActionStatus {
  SUBMITTED,
  RESCINDED,
  OVERWRITTEN,
  EXECUTED,
}
