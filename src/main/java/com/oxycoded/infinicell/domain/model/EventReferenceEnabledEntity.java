package com.oxycoded.infinicell.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class EventReferenceEnabledEntity extends InfinicellEntity {
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "eventReferenceEnabledEntity")
  private List<EventReference> eventReferences = new ArrayList<>();

}
