package com.oxycoded.infinicell.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class CombatAction extends InfinicellEntity {

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "combatant_id", nullable = false)
  private Combatant combatant;

  @Column(nullable = false)
  private CombatActionStatus combatActionStatus;

  @Column(nullable = false)
  private CombatActionType combatActionType;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "combatAction")
  private List<CombatActionTarget> targets = new ArrayList<>();

  public void addTarget(final CombatActionTarget combatActionTarget) {
    getTargets().add(combatActionTarget);
  }

  public boolean isSubmitted() {
    return CombatActionStatus.SUBMITTED.equals(getCombatActionStatus());
  }

  public void markAsOverridden() {
    setCombatActionStatus(CombatActionStatus.OVERWRITTEN);
  }

  public void markAsSubmitted() {
    setCombatActionStatus(CombatActionStatus.SUBMITTED);
  }
}
