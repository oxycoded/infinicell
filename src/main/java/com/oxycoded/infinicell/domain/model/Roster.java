package com.oxycoded.infinicell.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Roster extends InfinicellEntity {
  @OneToOne
  @JoinColumn(name = "roster_enabled_entity_id")
  private RosterEnabledEntity rosterEnabledEntity;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "roster")
  private List<CreatureInstance> creatureInstances = new ArrayList<>();
}
