package com.oxycoded.infinicell.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Inventory extends InfinicellEntity {
  @OneToOne
  @JoinColumn(name = "inventory_enabled_entity_id")
  private InventoryEnabledEntity inventoryEnabledEntity;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "inventory")
  private List<ItemInstance> itemInstances = new ArrayList<>();
}
