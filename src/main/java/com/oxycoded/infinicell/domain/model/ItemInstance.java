package com.oxycoded.infinicell.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ItemInstance extends InfinicellEntity {
  public static final ItemInstance NOT_DROPPED = new ItemInstance();

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private long value;

  @Column(nullable = false)
  private long currentStacks;

  @Column(nullable = false)
  private Rarity rarity;

  @Column(nullable = false)
  private Tier tier;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "item_id", nullable = false)
  private Item item;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "inventory_id")
  private Inventory inventory;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "itemInstanceToCopy")
  private List<ConsistentItemDrop> consistentItemDrops = new ArrayList<>();

  @Column(nullable = false)
  private String iconSpriteUrl;

  @Column(nullable = false)
  private String detailedSpriteUrl;

  /**
   * Postfixed with "Lod" for Law of Demeter; Named this way because without the "Lod" it conflicts with Spring's derived query in the following file:
   * com.oxycoded.infinicell.repository.ItemInstanceRepository.findAllByItemId
   */
  @Transient
  public String getItemIdLod() {
    return getItem().getId();
  }

  @Transient
  public String getItemName() {
    return getItem().getName();
  }

  @Transient
  public boolean isStackable() {
    return getItem().isStackable();
  }

  @Transient
  public long getMaxStacks() {
    return getItem().getMaxStacks();
  }
}
