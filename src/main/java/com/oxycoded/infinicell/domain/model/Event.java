package com.oxycoded.infinicell.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Event extends InfinicellEntity {

  @Column(nullable = false, updatable = false)
  private String timestamp;

  @Column(nullable = false)
  private EventType eventType;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "event")
  private List<EventReference> eventReferences = new ArrayList<>();

  @PrePersist
  private void setCreationTime() {
    final long epochMillis = Instant.now().toEpochMilli();
    final String epochMillisString = Long.toString(epochMillis);
    setTimestamp(epochMillisString);
  }
}
