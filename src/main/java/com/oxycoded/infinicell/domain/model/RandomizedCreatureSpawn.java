package com.oxycoded.infinicell.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class RandomizedCreatureSpawn extends InfinicellEntity {

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "spawn_table_id", nullable = false)
  private SpawnTable spawnTable;

  @Column(nullable = false)
  private long spawnChance;

  @Column(nullable = false)
  private long minSpawns;

  @Column(nullable = false)
  private long maxSpawns;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "creature_id", nullable = false)
  private Creature creature;
}
