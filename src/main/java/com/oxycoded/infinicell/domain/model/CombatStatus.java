package com.oxycoded.infinicell.domain.model;

public enum CombatStatus {
  NOT_STARTED,
  IN_PROGRESS,
  VICTORY,
  DEFEAT
}
