package com.oxycoded.infinicell.domain.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class NpcCreatureInstance extends CreatureInstance {
  public static final NpcCreatureInstance NOT_SPAWNED = new NpcCreatureInstance();
}
