package com.oxycoded.infinicell.domain.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@Entity
public class ExperienceGainedEvent extends Event {

  public ExperienceGainedEvent() {
    setEventType(EventType.EXPERIENCE_GAINED);
  }

  @Column(nullable = false)
  private long experienceGained;
}
