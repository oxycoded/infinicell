package com.oxycoded.infinicell.domain.model;

public enum CombatTeam {
  BLUE,
  RED,
  YELLOW,
  PURPLE,
  ORANGE,
  GREEN
}
