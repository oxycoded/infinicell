package com.oxycoded.infinicell.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"x", "y", "z"}))
public class Cell extends SpawnTableEnabledEntity {

  @Column(nullable = false)
  private long x;

  @Column(nullable = false)
  private long y;

  @Column(nullable = false)
  private long z;

  @Column(nullable = false)
  private long explorationExperienceValue;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "cell")
  private List<PcCreatureInstanceCell> pcCreatureInstanceCells = new ArrayList<>();

  @Transient
  public boolean isClaimed() {
    return getPcCreatureInstanceCells().stream().anyMatch(PcCreatureInstanceCell::isClaimed);
  }

  @Transient
  public CreatureInstance getDiscoveredBy() {
    final Optional<PcCreatureInstanceCell> discovererOptional = getPcCreatureInstanceCells().stream()
        .filter(PcCreatureInstanceCell::isDiscovered)
        .findFirst();
    if (discovererOptional.isEmpty()) {
      throw new RuntimeException("Cell was not discovered by anyone or anything??");
    }
    return discovererOptional.get().getPcCreatureInstance();
  }
}
