package com.oxycoded.infinicell.domain.model;

import lombok.Getter;

@Getter
public enum Direction {
  NORTH(0L, 1L, 0L),
  EAST(1L, 0L, 0L),
  SOUTH(0L, -1L, 0L),
  WEST(-1L, 0L, 0L),
  UP(0L, 0L, 1L),
  DOWN(0L, 0L, -1L);

  private final long xChange;
  private final long yChange;
  private final long zChange;

  Direction(final long xChange, final long yChange, final long zChange) {
    this.xChange = xChange;
    this.yChange = yChange;
    this.zChange = zChange;
  }
}
