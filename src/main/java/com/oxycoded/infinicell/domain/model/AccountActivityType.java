package com.oxycoded.infinicell.domain.model;

public enum AccountActivityType {
  ONLINE,
  OFFLINE,
  AWAY,
  BUSY,
  INVISIBLE
}
