package com.oxycoded.infinicell.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class CombatActionTarget extends InfinicellEntity {

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "target_combatant_id", nullable = false)
  private Combatant target;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "combat_action_id", nullable = false)
  private CombatAction combatAction;
}
