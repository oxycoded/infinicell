package com.oxycoded.infinicell.domain.model;

public enum CombatantStatus {
  OK,
  INCAPACITATED,
  DEFEATED,
  VICTORIOUS
}
