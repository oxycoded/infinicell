package com.oxycoded.infinicell.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class RosterEnabledEntity extends DropTableEnabledEntity {
  @OneToOne(mappedBy = "rosterEnabledEntity", cascade = CascadeType.ALL)
  @PrimaryKeyJoinColumn
  private Roster roster;
}
