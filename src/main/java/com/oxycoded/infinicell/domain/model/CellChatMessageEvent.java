package com.oxycoded.infinicell.domain.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@Entity
public class CellChatMessageEvent extends Event {

  public CellChatMessageEvent() {
    setEventType(EventType.CELL_CHAT_MESSAGE);
  }

  @Column(nullable = false)
  private String message;
}
