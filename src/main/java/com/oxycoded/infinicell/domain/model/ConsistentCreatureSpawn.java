package com.oxycoded.infinicell.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ConsistentCreatureSpawn extends InfinicellEntity {

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "spawn_table_id", nullable = false)
  private SpawnTable spawnTable;

  @Column(nullable = false)
  private long spawnChance;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "creature_instance_id", nullable = false)
  private CreatureInstance creatureInstanceToCopy;
}
