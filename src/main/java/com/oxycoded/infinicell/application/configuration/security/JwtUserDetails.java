package com.oxycoded.infinicell.application.configuration.security;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;

@Getter
@Setter
public class JwtUserDetails implements UserDetails {

  private String accountId;
  private Collection<InfinicellGrantedAuthority> infinicellGrantedAuthorities;

  public void addAsInfinicellGrantedAuthority(final String infinicellGrantedAuthorityString) {
    if(infinicellGrantedAuthorities == null) {
      setInfinicellGrantedAuthorities(new HashSet<>());
    }
    getInfinicellGrantedAuthorities().add(new InfinicellGrantedAuthority(infinicellGrantedAuthorityString));
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return getInfinicellGrantedAuthorities();
  }

  @Override
  public String getPassword() {
    return null;
  }

  @Override
  public String getUsername() {
    return null;
  }

  @Override
  public boolean isAccountNonExpired() {
    return false;
  }

  @Override
  public boolean isAccountNonLocked() {
    return false;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return false;
  }

  @Override
  public boolean isEnabled() {
    return false;
  }
}
