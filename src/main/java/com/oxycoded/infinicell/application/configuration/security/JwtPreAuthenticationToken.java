package com.oxycoded.infinicell.application.configuration.security;

import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

public class JwtPreAuthenticationToken extends PreAuthenticatedAuthenticationToken {

  public JwtPreAuthenticationToken(final JwtUserDetails jwtUserDetails, final WebAuthenticationDetails webAuthenticationDetails) {
    super(jwtUserDetails, null, jwtUserDetails.getAuthorities());
    super.setDetails(webAuthenticationDetails);
  }

  @Override
  public Object getCredentials() {
    return null;
  }
}
