package com.oxycoded.infinicell.application.configuration.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.oxycoded.infinicell.domain.model.Account;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class JwtTokenService {

  @Value("${jwt.issuer}")
  private String jwtIssuer;

  private final JWTVerifier jwtVerifier;
  private final Algorithm jwtAlgorithm;

  public String createToken(final Account account) {
    final Instant now = Instant.now();
    final Instant expiration = Instant.now().plus(Duration.ofHours(4));
    return JWT.create()
        .withIssuer(jwtIssuer)
        .withIssuedAt(Date.from(now))
        .withExpiresAt(Date.from(expiration))
        .withSubject(account.getId())
        .sign(jwtAlgorithm);
  }

  public Optional<DecodedJWT> getDecodedToken(final String token) {
    try {
      final DecodedJWT decodedJWT = getJwtVerifier().verify(token);
      return Optional.of(decodedJWT);
    } catch (JWTVerificationException jve) {
      return Optional.empty();
    }
  }
}
