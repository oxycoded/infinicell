package com.oxycoded.infinicell.application.configuration.security;

import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.oxycoded.infinicell.domain.model.Account;
import com.oxycoded.infinicell.exception.LogInCodeNotFoundException;
import com.oxycoded.infinicell.service.AccountService;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class InfinicellUserDetailsService implements UserDetailsService {

  @Value("${jwt.issuer}")
  private String jwtIssuer;

  private final JWTVerifier jwtVerifier;
  private final AccountService accountService;
  private final Algorithm jwtAlgorithm;
  private final JwtTokenService jwtTokenService;

  @Override
  public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
    return null;
  }

  public JwtUserDetails createJwtUserDetailsFromToken(final String token) {
    final Optional<DecodedJWT> decodedJWTOptional = getJwtTokenService().getDecodedToken(token);
    if (decodedJWTOptional.isEmpty()) {
      throw new LogInCodeNotFoundException();
    }
    final DecodedJWT decodedJWT = decodedJWTOptional.get();
    final String subject = decodedJWT.getSubject();
    final Account account = getAccountService().findById(subject);
    return createJwtUserDetailsFromAccount(account);
  }

  private JwtUserDetails createJwtUserDetailsFromAccount(final Account account) {
    final JwtUserDetails jwtUserDetails = new JwtUserDetails();
    jwtUserDetails.setAccountId(account.getId());
    jwtUserDetails.addAsInfinicellGrantedAuthority("TRAVELER");
    return jwtUserDetails;
  }
}
