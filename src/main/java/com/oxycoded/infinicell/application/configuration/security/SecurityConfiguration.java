package com.oxycoded.infinicell.application.configuration.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfiguration {

  @Value("${jwt.algorithm.secret}")
  private String jwtAlgorithmSecret;

  @Value("${jwt.issuer}")
  private String jwtIssuer;

  @Bean
  public Algorithm jwtAlgorithm() {
    return Algorithm.HMAC512(jwtAlgorithmSecret);
  }

  @Bean
  public JWTVerifier jwtVerifier(final Algorithm algorithm) {
    return JWT.require(algorithm)
        .withIssuer(jwtIssuer)
        .build();
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public AuthenticationProvider authenticationProvider(final InfinicellUserDetailsService infinicellUserDetailsService,
      final PasswordEncoder passwordEncoder) {
    final DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
    daoAuthenticationProvider.setUserDetailsService(infinicellUserDetailsService);
    daoAuthenticationProvider.setPasswordEncoder(passwordEncoder);
    return daoAuthenticationProvider;
  }
}
