package com.oxycoded.infinicell.application.configuration;

import com.oxycoded.infinicell.domain.model.CellChatMessageEvent;
import com.oxycoded.infinicell.domain.model.CreatureInstance;
import com.oxycoded.infinicell.domain.model.ExperienceGainedEvent;
import com.oxycoded.infinicell.domain.model.NpcCreatureInstance;
import graphql.kickstart.tools.SchemaParserDictionary;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class ApplicationConfiguration {

  @Value("${graphql.url:/graphql}")
  private String graphqlUrl;

  @Bean
  public FilterRegistrationBean<CorsFilter> corsFilter() {
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    CorsConfiguration config = new CorsConfiguration();
    config.setAllowCredentials(false);
    config.addAllowedOrigin("*");
    config.addAllowedHeader("*");
    config.addAllowedMethod("*");
    source.registerCorsConfiguration(graphqlUrl, config);
    FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(source));
    bean.setOrder(0);
    return bean;
  }

  @Bean
  public SchemaParserDictionary schemaParserDictionary() {
    return new SchemaParserDictionary()
        .add(NpcCreatureInstance.class)
        .add(CellChatMessageEvent.class)
        .add(ExperienceGainedEvent.class);
  }
}
