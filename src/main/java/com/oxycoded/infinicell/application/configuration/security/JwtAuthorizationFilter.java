package com.oxycoded.infinicell.application.configuration.security;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@RequiredArgsConstructor
@Getter(AccessLevel.PRIVATE)
public class JwtAuthorizationFilter extends OncePerRequestFilter {

  private static final String AUTHORIZATION_HEADER = "Authorization";
  private static final Pattern BEARER_PATTERN = Pattern.compile("^Bearer (.+?)$");

  private final InfinicellUserDetailsService infinicellUserDetailsService;

  @Override
  protected void doFilterInternal(@NotNull final HttpServletRequest httpServletRequest, @NotNull HttpServletResponse httpServletResponse,
      @NotNull FilterChain filterChain) throws ServletException, IOException {
    final Optional<String> tokenOptional = getToken(httpServletRequest);
    if(tokenOptional.isEmpty()) {
      filterChain.doFilter(httpServletRequest, httpServletResponse);
      return;
    }
    final String token = tokenOptional.get();
    final JwtUserDetails jwtUserDetails = getInfinicellUserDetailsService().createJwtUserDetailsFromToken(token);
    final WebAuthenticationDetails webAuthenticationDetails = new WebAuthenticationDetailsSource().buildDetails(httpServletRequest);
    final JwtPreAuthenticationToken jwtPreAuthenticationToken = new JwtPreAuthenticationToken(jwtUserDetails, webAuthenticationDetails);
    SecurityContextHolder.getContext().setAuthentication(jwtPreAuthenticationToken);
    filterChain.doFilter(httpServletRequest, httpServletResponse);
  }

  private Optional<String> getToken(final HttpServletRequest httpServletRequest) {
    return Optional
        .ofNullable(httpServletRequest.getHeader(AUTHORIZATION_HEADER))
        .filter(StringUtils::isNotBlank)
        .map(BEARER_PATTERN::matcher)
        .filter(Matcher::find)
        .map(matcher -> matcher.group(1));
  }
}
