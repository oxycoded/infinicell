package com.oxycoded.infinicell.application.configuration.security;

import org.springframework.security.core.GrantedAuthority;

public class InfinicellGrantedAuthority implements GrantedAuthority {
  private final String authority;

  public InfinicellGrantedAuthority(final String authority) {
    this.authority = authority.toUpperCase();
  }

  @Override
  public String getAuthority() {
    return authority;
  }
}
