enum Direction {
    NORTH
    EAST
    SOUTH
    WEST
    UP
    DOWN
}

interface InfinicellEntity {
    id: ID!
}

interface EventReferenceEnabledEntity implements InfinicellEntity {
    id: ID!
    #eventReferences: [EventReference]
    eventReferences(first: Int, last: Int, before: String, after: String): EventReferenceConnection! @connection(for: "EventReference")
}

interface InventoryEnabledEntity implements InfinicellEntity, EventReferenceEnabledEntity {
    id: ID!
    eventReferences(first: Int, last: Int, before: String, after: String): EventReferenceConnection! @connection(for: "EventReference")
    inventory: Inventory!
}

interface DropTableEnabledEntity implements InfinicellEntity, EventReferenceEnabledEntity, InventoryEnabledEntity {
    id: ID!
    eventReferences(first: Int, last: Int, before: String, after: String): EventReferenceConnection! @connection(for: "EventReference")
    inventory: Inventory!
    dropTable: DropTable!
}

interface RosterEnabledEntity implements InfinicellEntity, EventReferenceEnabledEntity, InventoryEnabledEntity, DropTableEnabledEntity {
    id: ID!
    eventReferences(first: Int, last: Int, before: String, after: String): EventReferenceConnection! @connection(for: "EventReference")
    inventory: Inventory!
    dropTable: DropTable!
    roster: Roster!
}

interface SpawnTableEnabledEntity implements InfinicellEntity, EventReferenceEnabledEntity, InventoryEnabledEntity, DropTableEnabledEntity, RosterEnabledEntity {
    id: ID!
    eventReferences(first: Int, last: Int, before: String, after: String): EventReferenceConnection! @connection(for: "EventReference")
    inventory: Inventory!
    dropTable: DropTable!
    roster: Roster!
    spawnTable: SpawnTable!
}

type Account implements InfinicellEntity, EventReferenceEnabledEntity {
    id: ID!
    eventReferences(first: Int, last: Int, before: String, after: String): EventReferenceConnection! @connection(for: "EventReference")
    username: String
    email: String
#    passwordHash: String
#    logInCode: String
    accountIdentifier: String!
    pcCreatureInstances: [PcCreatureInstance]!
    selectedPcCreatureInstance: PcCreatureInstance
    friends: [Friend]!
    friendOfs: [Friend]!

}

type AnonymousAccount {
    account: Account!
    logInCode: String!
}

type LogInAccount {
    account: Account!
    token: String!
}

type Friend implements InfinicellEntity, EventReferenceEnabledEntity {
    id: ID!
    eventReferences(first: Int, last: Int, before: String, after: String): EventReferenceConnection! @connection(for: "EventReference")
    account: Account!
    accountOfFriend: Account!
    notes: String
}

type FriendRequest implements InfinicellEntity, EventReferenceEnabledEntity {
    id: ID!
    eventReferences(first: Int, last: Int, before: String, after: String): EventReferenceConnection! @connection(for: "EventReference")
    requestingAccount: Account!
    recipientAccount: Account!
    friendRequestStatus: String!
}

type Creature implements InfinicellEntity, EventReferenceEnabledEntity, InventoryEnabledEntity, DropTableEnabledEntity {
    id: ID!
    eventReferences(first: Int, last: Int, before: String, after: String): EventReferenceConnection! @connection(for: "EventReference")
    inventory: Inventory!
    dropTable: DropTable!
    name: String!
    iconSpriteUrl: String!
    detailedSpriteUrl: String!
    creatureInstances: [CreatureInstanceUnion]!
    randomizedCreatureSpawns: [RandomizedCreatureSpawn]!
}

interface CreatureInstance implements InfinicellEntity, EventReferenceEnabledEntity, InventoryEnabledEntity, DropTableEnabledEntity {
    id: ID!
    eventReferences(first: Int, last: Int, before: String, after: String): EventReferenceConnection! @connection(for: "EventReference")
    inventory: Inventory!
    dropTable: DropTable!
    name: String!
    description: String!
    experience: Int!
    iconSpriteUrl: String!
    detailedSpriteUrl: String!
    creature: Creature!
    currentCombatant: Combatant
    roster: Roster
    consistentCreatureSpawns: [ConsistentCreatureSpawn]!
}

type PcCreatureInstance implements InfinicellEntity, EventReferenceEnabledEntity, InventoryEnabledEntity, DropTableEnabledEntity, CreatureInstance {
    id: ID!
    eventReferences(first: Int, last: Int, before: String, after: String): EventReferenceConnection! @connection(for: "EventReference")
    inventory: Inventory!
    dropTable: DropTable!
    name: String!
    description: String!
    experience: Int!
    iconSpriteUrl: String!
    detailedSpriteUrl: String!
    creature: Creature!
    currentCombatant: Combatant
    roster: Roster
    consistentCreatureSpawns: [ConsistentCreatureSpawn]!
    pcCreatureInstanceCells(onlyClaimed: Boolean = false, onlyDiscoverd: Boolean = false, onlyCurrent: Boolean = true): [PcCreatureInstanceCell]!
    currentPcCreatureInstanceCell: PcCreatureInstanceCell!
    currentCell: Cell!
    account: Account!
}

type NpcCreatureInstance implements InfinicellEntity, EventReferenceEnabledEntity, InventoryEnabledEntity, DropTableEnabledEntity, CreatureInstance {
    id: ID!
    eventReferences(first: Int, last: Int, before: String, after: String): EventReferenceConnection! @connection(for: "EventReference")
    inventory: Inventory!
    dropTable: DropTable!
    name: String!
    description: String!
    experience: Int!
    iconSpriteUrl: String!
    detailedSpriteUrl: String!
    creature: Creature!
    currentCombatant: Combatant
    roster: Roster!
    consistentCreatureSpawns: [ConsistentCreatureSpawn]!
}

type EventReference implements InfinicellEntity {
    id: ID!
    event: EventUnion!
    eventReferenceEnabledEntity: EventReferenceEnabledEntity!
    eventReferenceType: String!
}

type Event implements InfinicellEntity {
    id: ID!
    timestamp: String!
    eventType: String!
    eventReferences: [EventReference]!
}

type CellChatMessageEvent implements InfinicellEntity {
    id: ID!
    timestamp: String!
    eventType: String!
    eventReferences: [EventReference]!
    message: String!
}

type ExperienceGainedEvent implements InfinicellEntity {
    id: ID!
    timestamp: String!
    eventType: String!
    eventReferences: [EventReference]!
    experienceGained: Int!
}

type Inventory implements InfinicellEntity {
    id: ID!
    inventoryEnabledEntity: InventoryEnabledEntity!
    itemInstances: [ItemInstance]!
}

type DropTable implements InfinicellEntity {
    id: ID!
    dropTableEnabledEntity: DropTableEnabledEntity!
    consistentItemDrops: [ConsistentItemDrop]!
    randomizedItemDrops: [RandomizedItemDrop]!
}

type ConsistentItemDrop implements InfinicellEntity {
    id: ID!
    dropTable: DropTable!
    dropChance: Int!
    itemInstanceToCopy: ItemInstance!
}

type RandomizedItemDrop implements InfinicellEntity {
    id: ID!
    dropTable: DropTable!
    dropChance: Int!
    minStacks: Int!
    maxStacks: Int!
    item: Item!
}

type Roster implements InfinicellEntity {
    id: ID!
    rosterEnabledEntity: RosterEnabledEntity!
    creatureInstances: [CreatureInstanceUnion]!
}

type SpawnTable implements InfinicellEntity {
    id: ID!
    spawnTableEnabledEntity: SpawnTableEnabledEntity!
    consistentCreatureSpawns: [ConsistentCreatureSpawn]!
    randomizedCreatureSpawns: [RandomizedCreatureSpawn]!
}

type ConsistentCreatureSpawn implements InfinicellEntity {
    id: ID!
    spawnTable: SpawnTable!
    spawnChance: Int!
    creatureInstanceToCopy: CreatureInstanceUnion!
}

type RandomizedCreatureSpawn implements InfinicellEntity {
    id: ID!
    spawnTable: SpawnTable!
    spawnChance: Int!
    minSpawns: Int!
    maxSpawns: Int!
    creature: Creature!
}

type Item implements InfinicellEntity {
    id: ID!
    name: String!
    value: Int!
    maxStacks: String!
    rarity: String!
    tier: String!
    itemInstances: [ItemInstance]!
    randomizedItemDrops: [RandomizedItemDrop]!
    iconSpriteUrl: String!
    detailedSpriteUrl: String!
}

type ItemInstance implements InfinicellEntity {
    id: ID!
    name: String!
    value: Int!
    currentStacks: Int!
    rarity: String!
    tier: String!
    item: Item!
    consistentItemDrops: [ConsistentItemDrop]!
    inventory: Inventory!
    iconSpriteUrl: String!
    detailedSpriteUrl: String!
}

type Cell implements InfinicellEntity, EventReferenceEnabledEntity, InventoryEnabledEntity, DropTableEnabledEntity, RosterEnabledEntity, SpawnTableEnabledEntity {
    id: ID!
    eventReferences(first: Int, last: Int, before: String, after: String): EventReferenceConnection! @connection(for: "EventReference")
    inventory: Inventory!
    dropTable: DropTable!
    roster: Roster!
    spawnTable: SpawnTable!
    x: Int!
    y: Int!
    z: Int!
    explorationExperienceValue: Int!
    creatureInstanceCells(onlyClaimed: Boolean = false, onlyDiscoverd: Boolean = false, onlyCurrent: Boolean = true): [PcCreatureInstanceCell]!
    #creatureInstanceCells(first: Int, last: Int, before: String, after: String): CreatureInstanceCellConnection! @connection(for: "CreatureInstanceCell")
    discoveredBy: PcCreatureInstance!
}

type PcCreatureInstanceCell implements InfinicellEntity, EventReferenceEnabledEntity, InventoryEnabledEntity, DropTableEnabledEntity, RosterEnabledEntity, SpawnTableEnabledEntity {
    id: ID!
    eventReferences(first: Int, last: Int, before: String, after: String): EventReferenceConnection! @connection(for: "EventReference")
    inventory: Inventory!
    dropTable: DropTable!
    roster: Roster!
    spawnTable: SpawnTable!
    cell: Cell!
    pcCreatureInstance: PcCreatureInstance!
    cleared: Boolean!
    searched: Boolean!
    scouted: Boolean!
    current: Boolean!
    claimed: Boolean!
    discovered: Boolean!
    timesVisited: Int!
}

type Combatant implements InfinicellEntity, EventReferenceEnabledEntity {
    id: ID!
    eventReferences(first: Int, last: Int, before: String, after: String): EventReferenceConnection! @connection(for: "EventReference")
    creatureInstance: CreatureInstanceUnion!
    combat: Combat!
    combatTeam: String!
    combatPosition: String!
    combatantStatus: String!
}

type Combat implements InfinicellEntity, EventReferenceEnabledEntity {
    id: ID!
    eventReferences(first: Int, last: Int, before: String, after: String): EventReferenceConnection! @connection(for: "EventReference")
    pcCreatureInstanceCell: PcCreatureInstanceCell!
    combatants: [Combatant]!
    combatStatus: String!
}

union CreatureInstanceUnion = NpcCreatureInstance | PcCreatureInstance
union EventUnion = Event | CellChatMessageEvent | ExperienceGainedEvent

type Query {
    pcCreatureInstances: [PcCreatureInstance]!
    pcCreatureInstance(id: ID!): PcCreatureInstance
    itemInstance(id: ID!): ItemInstance
    creatureInstance(id: ID!): CreatureInstanceUnion
}

type Mutation {
    createAnonymousAccount(username: String): AnonymousAccount!
    accountLogInWithCode(logInCode: String): LogInAccount!
    accountLogInWithUsernamePassword(username: String, password: String): LogInAccount!

    createOutgoingFriendRequest(recipientAccountId: String): FriendRequest!
    cancelOutgoingFriendRequest(friendRequestId: String): FriendRequest!
    acceptIncomingFriendRequest(friendRequestId: String): FriendRequest!
    denyIncomingFriendRequest(friendRequestId: String): FriendRequest!
    ignoreIncomingFriendRequest(friendRequestId: String): FriendRequest!

    createPcCreatureInstance(name: String, description: String): PcCreatureInstance!
    loadPcCreatureInstance(pcCreatureInstanceId: String): PcCreatureInstance!

    step(direction: Direction): PcCreatureInstance!

    searchCurrentCell: PcCreatureInstance!

    takeItemInstances(itemInstanceIds: [String]): PcCreatureInstance!
    shareItemInstances(itemInstanceIds: [String]): PcCreatureInstance!

    scoutCurrentCell: PcCreatureInstance!

    startCombat(creatureInstanceIds: [String]): PcCreatureInstance!

    sendLocalChat(pcCreatureInstanceId: String, message: String): Event!
    claimCurrentCell(pcCreatureInstanceId: String): PcCreatureInstance!
}

type Subscription {
    cellEvents(cellId: String): Event!
}