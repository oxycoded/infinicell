package com.oxycoded.infinicell.domain.model;


import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CombatActionTypeTest {

  @ParameterizedTest
  @ValueSource(strings = {"ATTACK"})
  public void combatActionType(final String combatActionStringParameter) {
    assertNotNull(CombatActionType.valueOf(combatActionStringParameter));
  }

}
