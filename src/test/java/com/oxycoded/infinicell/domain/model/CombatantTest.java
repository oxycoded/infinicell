package com.oxycoded.infinicell.domain.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CombatantTest {

  @Test
  public void submitCombatAction_shouldSetSubmittedCombatAction() {
    final Combatant combatant = new Combatant();
    final CombatAction combatAction = new CombatAction();
    combatant.submitCombatAction(combatAction);
    assertEquals(combatAction, combatant.getSubmittedCombatAction());
  }

  @Test
  public void submitCombatAction_shouldOverrideCurrentSubmittedCombatAction() {
    final Combatant combatant = new Combatant();
    final CombatAction combatActionToOverride = new CombatAction();
    combatant.submitCombatAction(combatActionToOverride);
    final CombatAction combatAction = new CombatAction();
    combatant.submitCombatAction(combatAction);
    assertEquals(combatAction, combatant.getSubmittedCombatAction());
  }

  @Test
  public void hasSubmittedCombatAction_withNoSubmittedCombatAction_shouldReturnFalse() {
    final Combatant combatant = new Combatant();
    assertFalse(combatant.hasSubmittedCombatAction());
  }

  @Test
  public void hasSubmittedCombatAction_withSubmittedCombatAction_shouldReturnFalse() {
    final Combatant combatant = new Combatant();
    final CombatAction combatAction = new CombatAction();
    combatant.submitCombatAction(combatAction);
    assertTrue(combatant.hasSubmittedCombatAction());
  }

}
