package com.oxycoded.infinicell.domain.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

public class AccountTest {

  @Test
  public void accountDefaults() {
    final Account account = new Account();
    assertAll(
        () -> assertNull(account.getUsername()),
        () -> assertNull(account.getEmail()),
        () -> assertNull(account.getPasswordHash()),
        () -> assertNull(account.getLogInCode()),
        () -> assertNull(account.getAccountIdentifier()),
        () -> assertEquals(AccountActivityType.OFFLINE, account.getAccountActivityType()),
        () -> assertNotNull(account.getPcCreatureInstances()),
        () -> assertTrue(account.getPcCreatureInstances().isEmpty()),
        () -> assertNull(account.getSelectedPcCreatureInstance()),
        () -> assertNotNull(account.getFriends()),
        () -> assertTrue(account.getFriends().isEmpty()),
        () -> assertNotNull(account.getFriendOfs()),
        () -> assertTrue(account.getFriendOfs().isEmpty()),
        () -> assertNotNull(account.getOutgoingFriendRequests()),
        () -> assertTrue(account.getOutgoingFriendRequests().isEmpty()),
        () -> assertNotNull(account.getIncomingFriendRequests()),
        () -> assertTrue(account.getIncomingFriendRequests().isEmpty())
    );
  }

  @Test
  public void createAnonymousAccount_defaults() {
    final Account account = createDefaultAnonymousAccount();
    assertAll(
        () -> assertEquals("username", account.getUsername()),
        () -> assertNull(account.getEmail()),
        () -> assertNull(account.getPasswordHash()),
        () -> assertNull(account.getLogInCode()),
        () -> assertNull(account.getAccountIdentifier()),
        () -> assertEquals(AccountActivityType.OFFLINE, account.getAccountActivityType()),
        () -> assertNotNull(account.getPcCreatureInstances()),
        () -> assertTrue(account.getPcCreatureInstances().isEmpty()),
        () -> assertNull(account.getSelectedPcCreatureInstance()),
        () -> assertNotNull(account.getFriends()),
        () -> assertTrue(account.getFriends().isEmpty()),
        () -> assertNotNull(account.getFriendOfs()),
        () -> assertTrue(account.getFriendOfs().isEmpty()),
        () -> assertNotNull(account.getOutgoingFriendRequests()),
        () -> assertTrue(account.getOutgoingFriendRequests().isEmpty()),
        () -> assertNotNull(account.getIncomingFriendRequests()),
        () -> assertTrue(account.getIncomingFriendRequests().isEmpty())
    );
  }

  @Test
  public void createAccount_defaults() {
    final Account account = createDefaultAccount();
    assertAll(
        () -> assertEquals("username", account.getUsername()),
        () -> assertEquals("email", account.getEmail()),
        () -> assertEquals("password", account.getPasswordHash()),
        () -> assertNull(account.getLogInCode()),
        () -> assertNull(account.getAccountIdentifier()),
        () -> assertEquals(AccountActivityType.OFFLINE, account.getAccountActivityType()),
        () -> assertNotNull(account.getPcCreatureInstances()),
        () -> assertTrue(account.getPcCreatureInstances().isEmpty()),
        () -> assertNull(account.getSelectedPcCreatureInstance()),
        () -> assertNotNull(account.getFriends()),
        () -> assertTrue(account.getFriends().isEmpty()),
        () -> assertNotNull(account.getFriendOfs()),
        () -> assertTrue(account.getFriendOfs().isEmpty()),
        () -> assertNotNull(account.getOutgoingFriendRequests()),
        () -> assertTrue(account.getOutgoingFriendRequests().isEmpty()),
        () -> assertNotNull(account.getIncomingFriendRequests()),
        () -> assertTrue(account.getIncomingFriendRequests().isEmpty())
    );
  }

  @ParameterizedTest
  @NullAndEmptySource
  public void createAccount_withInvalidPassword_shouldThrowException(final String passwordParameter) {
    assertThrows(RuntimeException.class, () -> createAccount(passwordParameter, "password"));
  }

  @ParameterizedTest
  @NullAndEmptySource
  public void createAccount_withInvalidConfirmPassword_shouldThrowException(final String confirmPasswordParameter) {
    assertThrows(RuntimeException.class, () -> createAccount("password", confirmPasswordParameter));
  }

  @Test
  public void createAccount_withNonMatchingPasswords_shouldThrowException() {
    assertThrows(RuntimeException.class, () -> createAccount("password", "otherPassword"));
  }

  @Test
  public void onCreate_shouldGenerateLogInCodeAndAccountIdentifier() {
    final Account account = createDefaultAnonymousAccount();
    account.onCreate();
    assertAll(
        () -> assertNotNull(account.getLogInCode()),
        () -> assertNotNull(account.getAccountIdentifier())
    );
  }
  
  @Test
  public void updateAccountActivity_shouldUpdateAccountActivity() {
    final Account account = createDefaultAnonymousAccount();
    account.updateAccountActivity(AccountActivityType.ONLINE);
    assertEquals(AccountActivityType.ONLINE, account.getAccountActivityType());
  }

  @Test
  public void isOnline_whenAccountIsOnline_shouldReturnTrue() {
    final Account account = createDefaultAnonymousAccount();
    account.updateAccountActivity(AccountActivityType.ONLINE);
    assertTrue(account.isOnline());
  }

  @Test
  public void isOnline_whenAccountIsNotOnline_shouldReturnFalse() {
    final Account account = createDefaultAnonymousAccount();
    account.updateAccountActivity(AccountActivityType.OFFLINE);
    assertFalse(account.isOnline());
  }

  @Test
  public void isOnline_whenAccountActivityIsNull_shouldReturnFalse() {
    final Account account = createDefaultAnonymousAccount();
    account.updateAccountActivity(null);
    assertFalse(account.isOnline());
  }

  @Test
  public void isOffline_whenAccountIsOffline_shouldReturnTrue() {
    final Account account = createDefaultAnonymousAccount();
    account.updateAccountActivity(AccountActivityType.OFFLINE);
    assertTrue(account.isOffline());
  }

  @Test
  public void isOffline_whenAccountIsNotOffline_shouldReturnFalse() {
    final Account account = createDefaultAnonymousAccount();
    account.updateAccountActivity(AccountActivityType.ONLINE);
    assertFalse(account.isOffline());
  }

  @Test
  public void isOffline_whenAccountActivityIsNull_shouldReturnFalse() {
    final Account account = createDefaultAnonymousAccount();
    account.updateAccountActivity(null);
    assertFalse(account.isOffline());
  }

  @Test
  public void selectPcCreatureInstance() {
    final Account account = createDefaultAnonymousAccount();
    final PcCreatureInstance pcCreatureInstance = new PcCreatureInstance();
    account.selectPcCreatureInstance(pcCreatureInstance);
    assertEquals(pcCreatureInstance, account.getSelectedPcCreatureInstance());
  }

  private Account createDefaultAnonymousAccount() {
    return Account.createAnonymousAccount("username");
  }

  private Account createDefaultAccount() {
    return createAccount("password", "password");
  }

  private Account createAccount(final String password, final String confirmPassword) {
    return Account.createAccount("username", "email", password, confirmPassword);
  }
}
