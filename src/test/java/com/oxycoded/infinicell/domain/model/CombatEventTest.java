package com.oxycoded.infinicell.domain.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CombatEventTest {

  @Test
  public void combatEventType() {
    final Combatant target = new Combatant();
    final Combatant damagedCombatant = new Combatant();
    final int damageAmount = 1;
    final CombatEvent combatEvent = new CombatEvent(CombatEventType.ATTACK, target, damagedCombatant, damageAmount);

    assertAll(
        () -> assertEquals(CombatEventType.ATTACK, combatEvent.getCombatEventType()),
        () -> assertEquals(target, combatEvent.getTarget()),
        () -> assertEquals(damagedCombatant, combatEvent.getDamagedCombatant()),
        () -> assertEquals(damageAmount, combatEvent.getDamageAmount())
    );
  }

}
