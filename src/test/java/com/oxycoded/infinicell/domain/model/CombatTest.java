package com.oxycoded.infinicell.domain.model;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CombatTest {

  @Test
  public void isRoundReady_whenNoCombatantsHaveSubmittedCombatActions_shouldReturnFalse() {
    final Combat combat = createCombat();
    combat.addCombatant(createCombatant());
    combat.addCombatant(createCombatant());
    assertFalse(combat.isRoundReady());
  }

  @Test
  public void isRoundReady_whenAllCombatantsHaveSubmittedCombatActions_shouldReturnTrue() {
    final Combat combat = createCombat();
    combat.addCombatant(createCombatantWithCombatAction());
    combat.addCombatant(createCombatantWithCombatAction());
    assertTrue(combat.isRoundReady());
  }

  @Test
  public void isRoundReady_whenSomeCombatantsHaveSubmittedCombatActions_shouldReturnFalse() {
    final Combat combat = createCombat();
    combat.addCombatant(createCombatantWithCombatAction());
    combat.addCombatant(createCombatant());
    assertFalse(combat.isRoundReady());
  }

  @Test
  @Disabled
  public void performRound_shouldReturnCombatRoundResult() {
    final Combat combat = createCombat();
    final Combatant combatantA = createCombatant();
    final Combatant combatantB = createCombatant();
    combat.addCombatant(combatantA);
    combat.addCombatant(combatantB);
    combatantA.submitCombatAction(createAttackCombatAction(combatantB));
    combatantB.submitCombatAction(createAttackCombatAction(combatantA));

    final CombatRoundResult combatRoundResult = combat.performRound();

    assertAll(
        () -> assertNotNull(combatRoundResult),
        () -> assertEquals(4, combatRoundResult.getCombatEvents().size()),

        () -> assertEquals(CombatEventType.ATTACK, combatRoundResult.getCombatEvents().get(0).getCombatEventType()),
        () -> assertEquals(combatantB, combatRoundResult.getCombatEvents().get(0).getTarget()),

        () -> assertEquals(CombatEventType.TAKE_DAMAGE, combatRoundResult.getCombatEvents().get(1).getCombatEventType()),
        () -> assertEquals(combatantB, combatRoundResult.getCombatEvents().get(1).getDamagedCombatant()),
        () -> assertEquals(1, combatRoundResult.getCombatEvents().get(1).getDamageAmount()),
        //() -> assertEquals(DamageType.PHYSICAL, combatRoundResult.getCombatEvents().get(1).getDamageType()),

        () -> assertEquals(CombatEventType.ATTACK, combatRoundResult.getCombatEvents().get(2).getCombatEventType()),
        () -> assertEquals(combatantA, combatRoundResult.getCombatEvents().get(0).getTarget())
    );
  }

  private Combat createCombat() {
    return new Combat();
  }

  private Combatant createCombatant() {
    return new Combatant();
  }

  private Combatant createCombatantWithCombatAction() {
    final Combatant combatant = createCombatant();
    combatant.submitCombatAction(createAttackCombatAction(null));
    return combatant;
  }

  private CombatAction createAttackCombatAction(final Combatant target) {
    final CombatAction combatAction = new CombatAction();
    combatAction.setCombatActionType(CombatActionType.ATTACK);
    combatAction.addTarget(createCombatActionTarget(combatAction, target));
    return combatAction;
  }

  private CombatActionTarget createCombatActionTarget(final CombatAction combatAction, final Combatant target) {
    final CombatActionTarget combatActionTarget = new CombatActionTarget();
    combatActionTarget.setTarget(target);
    combatActionTarget.setCombatAction(combatAction);
    return combatActionTarget;
  }

}
