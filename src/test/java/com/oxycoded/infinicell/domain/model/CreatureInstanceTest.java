package com.oxycoded.infinicell.domain.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreatureInstanceTest {

  @Test
  public void defaultCurrentHealth_shouldBe10() {
    final CreatureInstance creatureInstance = new CreatureInstance();
    assertEquals(10, creatureInstance.getCurrentHealth());
  }

  @Test
  public void takingDamage_shouldReduceCurrentHealthByDamageAmount() {
    final CreatureInstance creatureInstance = new CreatureInstance();
    creatureInstance.takeDamage(3);
    assertEquals(7, creatureInstance.getCurrentHealth());
  }

  @Test
  public void takingMoreDamageThanCurrentHealth_shouldReduceCurrentHealthToZero() {
    final CreatureInstance creatureInstance = new CreatureInstance();
    creatureInstance.takeDamage(1000);
    assertEquals(0, creatureInstance.getCurrentHealth());
  }


  @Test
  public void heal_shouldIncreaseCurrentHealthByHealAmount() {
    final CreatureInstance creatureInstance = new CreatureInstance();
    creatureInstance.takeDamage(3);
    creatureInstance.heal(3);
    assertEquals(10, creatureInstance.getCurrentHealth());
  }

  @Test
  public void heal_shouldNotIncreaseCurrentHealthBeyondMaxHealth() {
    final CreatureInstance creatureInstance = new CreatureInstance();
    creatureInstance.takeDamage(3);
    creatureInstance.heal(1000);
    assertEquals(10, creatureInstance.getCurrentHealth());
  }

  @Test
  public void defaultMaxHealth_shouldBe10() {
    final CreatureInstance creatureInstance = new CreatureInstance();
    assertEquals(10, creatureInstance.getMaxHealth());
  }

  @Test
  public void takingDamage_shouldNotReduceMaxHealth() {
    final CreatureInstance creatureInstance = new CreatureInstance();
    creatureInstance.takeDamage(3);
    assertEquals(10, creatureInstance.getMaxHealth());
  }
}
