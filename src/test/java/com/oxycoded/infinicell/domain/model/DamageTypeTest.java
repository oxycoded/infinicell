package com.oxycoded.infinicell.domain.model;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DamageTypeTest {
  @ParameterizedTest
  @ValueSource(strings = {"PHYSICAL"})
  public void damageTypes_shouldExist(final String damageTypeStringParameter) {
    assertNotNull(DamageType.valueOf(damageTypeStringParameter));
  }
}
