package com.oxycoded.infinicell.domain.model;


import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AccountActivityTypeTest {

  @ParameterizedTest
  @ValueSource(strings = {"ONLINE", "OFFLINE", "AWAY", "BUSY", "INVISIBLE"})
  public void accountActivityType(final String accountActivityTypeStringParameter) {
    assertNotNull(AccountActivityType.valueOf(accountActivityTypeStringParameter));
  }

}
