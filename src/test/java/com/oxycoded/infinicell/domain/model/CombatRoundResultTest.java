package com.oxycoded.infinicell.domain.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class CombatRoundResultTest {

  @Test
  public void addCombatEvent_shouldAddCombatEvent() {
    final CombatRoundResult combatRoundResult = new CombatRoundResult();
    final Combatant target = new Combatant();
    final CombatEvent combatEvent = new CombatEvent(CombatEventType.ATTACK, target, null, 0);
    combatRoundResult.addCombatEvent(combatEvent);

    assertAll(
        () -> assertEquals(1, combatRoundResult.getCombatEvents().size()),
        () -> assertEquals(combatEvent, combatRoundResult.getCombatEvents().get(0))
    );
  }

  @Test
  public void getNumberOfCombatEvents_withEmptyCombatEventList_shouldReturnZero() {
    final CombatRoundResult combatRoundResult = new CombatRoundResult();
    assertEquals(0, combatRoundResult.getCombatEvents().size());
  }

}
